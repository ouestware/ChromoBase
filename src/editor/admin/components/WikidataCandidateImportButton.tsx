import { LazyQueryExecFunction, OperationVariables } from "@keystone-6/core/admin-ui/apollo";
import { useList } from "@keystone-6/core/admin-ui/context";
import { Link } from "@keystone-6/core/admin-ui/router";
import { Button } from "@keystone-ui/button";
import { fromPairs, isString, keys, omit, pick, toPairs } from "lodash";
import React, { FC, useEffect } from "react";
import { useCreateIfNew } from "../hooks/useCreateIfNew";
import { PreviewObjectType } from "./WikidataCandidatePreview";

// we should reuse List definition from schema to control that list is accurate...
const IMPORTABLE_LIST = ["Person", "Occupation", "Organisation", "Glossary", "GlossaryType", "Event"] as const;
export type ImportableList = (typeof IMPORTABLE_LIST)[number];

export const WikidataCandidateCreationButton: FC<{ listName: ImportableList; object: PreviewObjectType }> = ({
  listName,
  object,
}) => {
  const { queryExistingData, createIfNew, existingData, loading, createdData, creationLoading } =
    useCreateIfNew(listName);

  useEffect(() => {
    if (queryExistingData)
      queryExistingData({
        variables: {
          where: { wikidataId: object.wikidataId },
        },
        fetchPolicy: "no-cache",
      }).then((d) => console.log(d.data));
  }, [queryExistingData]);

  const list = useList(listName);
  const createsIfNew: Record<
    ImportableList,
    {
      queryExistingData: LazyQueryExecFunction<unknown, OperationVariables>;
      createIfNew: (object: PreviewObjectType) => Promise<unknown>;
      existingData: unknown;
      loading: boolean;
      createdData: unknown;
      creationLoading: boolean;
    }
  > = {
    Occupation: useCreateIfNew("Occupation"),
    Glossary: useCreateIfNew("Glossary"),
    Organisation: useCreateIfNew("Organisation"),
    Person: useCreateIfNew("Person"),
    Event: useCreateIfNew("Event"),
    GlossaryType: useCreateIfNew("GlossaryType"),
  };

  return (
    <div>
      {existingData && existingData.item && (
        <div>
          Already imported as <Link href={`/${list.path}/${existingData.item.id}`}>{existingData.item.label}</Link>
        </div>
      )}
      {!loading && existingData && existingData.item === null && (
        <Button
          isLoading={loading || creationLoading}
          onClick={async () => {
            const extraKeysNotInModel = keys(object).filter((f) => list.fields[f] === undefined);
            const payload: Record<string, unknown> = fromPairs(
              await Promise.all(
                // remove extra key
                toPairs(omit(object, extraKeysNotInModel)).map(async ([key, value]) => {
                  const field = list.fields[key];
                  if (field) {
                    const fieldMeta = field.fieldMeta as {
                      refListKey?: ImportableList;
                      many?: boolean;
                      documentFeatures?: Record<string, unknown>;
                    };
                    console.log(fieldMeta);
                    // detect from list that occupations is a FK
                    if (fieldMeta.refListKey && value != undefined && createsIfNew[fieldMeta.refListKey]) {
                      const { createIfNew: createSubIfNew } = createsIfNew[fieldMeta.refListKey];
                      const _v = Array.isArray(value) ? value : [value];

                      const os = await Promise.all(
                        _v.filter((o): o is Record<string, string> => !isString(o)).map((o) => createSubIfNew(o)),
                      );
                      const connect = os.map((o) => pick(o, ["id", "wikidataId"]));
                      return [key, { connect: fieldMeta.many ? connect : connect[0] }];
                    }
                  }
                  return [key, value];
                }),
              ),
            );
            await createIfNew(payload as PreviewObjectType);
            //TODO: notify
          }}
        >
          import as {listName}
        </Button>
      )}
      {createdData && <Link href={`/${list.path}/${createdData.item.id}`}>{createdData.item.label}</Link>}
    </div>
  );
};
