import { LoadingDots } from "@keystone-ui/loading";
import axios from "axios";
import React, { FC, useState } from "react";
import { Item } from "wikibase-sdk";
import { WikidataCandidatePreview } from "./WikidataCandidatePreview";

export interface WikidataCandidateType {
  id: string;
  label: string;
  description: string;
}

export const WikidataImportCandidate: FC<{ candidate: WikidataCandidateType }> = ({ candidate }) => {
  const [candidateEntity, setCandidateEntity] = useState<Item | null>(null);

  return (
    <details
      key={candidate.id}
      onToggle={async () => {
        if (candidateEntity === null) {
          try {
            const response = await axios.get<{ entities: Record<string, Item> }>(
              `https://www.wikidata.org/wiki/Special:EntityData/${candidate.id}.json`,
            );
            if (response.data) {
              setCandidateEntity(response.data.entities[candidate.id]);
            }
          } catch {
            //todo notify
            console.error("can't retrieve candidate entity");
          }
        }
      }}
    >
      <summary style={{ cursor: "pointer" }}>
        {candidate.label} <i>{candidate.description}</i>{" "}
        <a href={`https://wikidata.org/wiki/${candidate.id}`}>{candidate.id}</a>
      </summary>
      {candidateEntity ? (
        <WikidataCandidatePreview candidate={candidateEntity} />
      ) : (
        <LoadingDots label={`searching...`} tone="active" size="medium" />
      )}
    </details>
  );
};
