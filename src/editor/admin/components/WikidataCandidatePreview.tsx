import axios from "axios";
import { isObject, isString, pick, toPairs } from "lodash";
import React, { FC, useEffect, useState } from "react";
import {
  Item,
  PropertyId,
  SimplifiedPropertyClaims,
  isItemId,
  simplifyDescriptions,
  simplifyEntity,
  simplifyLabels,
} from "wikibase-sdk";

import { Stack } from "@keystone-ui/core";
import { LoadingDots } from "@keystone-ui/loading";

import { getEntityModel } from "../hooks/entityTypeIsSubClassOf";
import { ImportableList, WikidataCandidateCreationButton } from "./WikidataCandidateImportButton";

const processClaims = async (
  properties: SimplifiedPropertyClaims,
  processSubEntity?: (item: Item) => string | Record<string, string> | null,
): Promise<(string | Record<string, string>)[]> => {
  if (properties === undefined) return [];
  return (
    await Promise.all(
      properties.map(async (p) => {
        if (isString(p) && isItemId(p)) {
          const r = await axios.get<{ entities: Record<string, Item> }>(
            `https://www.wikidata.org/wiki/Special:EntityData/${p}.json`,
          );
          const subEntity = r.data.entities[p];
          if (processSubEntity) {
            return processSubEntity(subEntity);
          }
          // default
          if (subEntity.labels) {
            const labels = simplifyLabels(subEntity.labels);
            return labels.en || labels.fr || "";
          }
          return subEntity.title || "";
        }
        return p + "";
      }),
    )
  ).filter((p): p is string | Record<string, string> => p !== null);
};

export type PreviewObjectType = Record<
  string,
  string | undefined | (string | Record<string, string>)[] | Record<string, string>
>;

export const WikidataCandidatePreview: FC<{ candidate: Item }> = ({ candidate }) => {
  const [previewObject, setPreviewObject] = useState<PreviewObjectType | null>(null);
  const [loadingPreview, setLoadingPreview] = useState<boolean>(false);
  const [model, setModel] = useState<ImportableList | null>(null);

  useEffect(() => {
    const simplifiedCandidate = simplifyEntity(candidate, { timeConverter: "simple-day" });
    // helpers
    const getSubEntityLabel = async (property: PropertyId) => {
      if (simplifiedCandidate.claims && simplifiedCandidate.claims[property])
        return await processClaims(simplifiedCandidate.claims[property], (item: Item) => {
          if (item.labels) {
            const labels = simplifyLabels(item.labels);
            return labels.en || labels.fr || "";
          }
          return null;
        });
      return null;
    };
    const getPropertyUniqueLiteral = (property: PropertyId) =>
      simplifiedCandidate.claims &&
      simplifiedCandidate.claims[property] &&
      simplifiedCandidate.claims[property][0] + "";

    const getPreview = async (): Promise<PreviewObjectType | null> => {
      setLoadingPreview(true);

      /**
       *
       * People : Q5
       * organisation: Q43229
       * occupation: Q12737077
       * event: Q1656682
       * objects: Q4406616
       *
       */

      // People
      if (simplifiedCandidate.claims?.P31?.includes("Q5")) {
        const person: PreviewObjectType = {};
        person.wikidataId = simplifiedCandidate.id;
        // first name P735
        person.firstName = (await processClaims(simplifiedCandidate.claims?.P735)).join(" ");
        // last name P734
        person.lastName = (await processClaims(simplifiedCandidate.claims?.P734)).join(" ");
        // fullname
        person.fullName = candidate.labels && simplifyLabels(candidate.labels).en;

        // date birth P569
        person.dateOfBirth = getPropertyUniqueLiteral("P569");
        // date death P570
        person.dateOfDeath = getPropertyUniqueLiteral("P570");

        // occupations P106
        person.occupations = await processClaims(simplifiedCandidate.claims?.P106, (item: Item) => {
          // get wikidata id + label
          if (item.labels) {
            const labels = simplifyLabels(item.labels);
            return {
              wikidataId: item.id.toString(),
              label: labels.en || labels.fr || "",

              description: (item.descriptions && simplifyDescriptions(item.descriptions).en) || "",
            };
          }
          return null;
        });
        // NOT AVAILABLE
        // biography

        // IDEAS
        // fields of work P101
        // image P18
        // identifier BNF P268
        // identifier idREF P269
        setModel("Person");
        return person;
      }
      // Find model from sparql query
      const entityModel = await getEntityModel(candidate.id);

      let type: Record<string, string> | undefined = undefined;
      const metadata: PreviewObjectType = {};
      // ORGANISATION
      if (entityModel?.model === "Organisation") {
        setModel("Organisation");
        metadata.dateOfInception = getPropertyUniqueLiteral("P571");
      }
      if (entityModel?.model === "Glossary") {
        setModel("Glossary");
        type = pick(entityModel, ["wikidataId", "label", "description"]);
        metadata.chemicalFormula = getPropertyUniqueLiteral("P274");
        metadata.dateOfDiscoveryInvention = getPropertyUniqueLiteral("P575");
      }
      if (entityModel?.model === "Event") {
        setModel("Event");
        metadata.startDate = getPropertyUniqueLiteral("P580") || getPropertyUniqueLiteral("P1619");
        metadata.endDate = getPropertyUniqueLiteral("P582") || getPropertyUniqueLiteral("P3999");
        const city = await getSubEntityLabel("P131");
        const country = await getSubEntityLabel("P17");
        metadata.place = [city, country].filter((i) => i !== null).join(", ");
      }

      return {
        wikidataId: simplifiedCandidate.id,
        label: candidate.labels && simplifyLabels(candidate.labels).en,
        type,
        description: candidate.descriptions && simplifyDescriptions(candidate.descriptions).en,

        // URL ?
        ...metadata,
      };
    };
    getPreview().then((o) => {
      setLoadingPreview(false);
      setPreviewObject(o);
    });
  }, [candidate]);

  return (
    <>
      {loadingPreview && <LoadingDots label={`loading preview...`} tone="active" size="medium" />}
      {!loadingPreview && previewObject === null && <span>Entity Type not supported yet</span>}
      {!loadingPreview && previewObject && (
        <div>
          {/* TODO : replace this draft preview in HTML by more robust version */}
          {toPairs(previewObject).map(([k, v]) => {
            if (v)
              return (
                <div key={k}>
                  {k}:{" "}
                  {Array.isArray(v) ? v.map((e) => (isObject(e) ? e.label : e)).join(", ") : isObject(v) ? v.label : v}
                </div>
              );
            return null;
          })}
          {model !== null && <WikidataCandidateCreationButton listName={model} object={previewObject} />}
          {model === null && (
            <Stack across gap="medium">
              <WikidataCandidateCreationButton listName="Person" object={previewObject} />
              <WikidataCandidateCreationButton listName="Event" object={previewObject} />
              <WikidataCandidateCreationButton listName="Organisation" object={previewObject} />
              <WikidataCandidateCreationButton listName="Glossary" object={previewObject} />
            </Stack>
          )}
        </div>
      )}
    </>
  );
};
