import { useEffect, useState } from "react";

import { gql, useQuery } from "@keystone-6/core/admin-ui/apollo";
import type { NavigationProps } from "@keystone-6/core/admin-ui/components";
import { ListNavItems, NavItem, NavigationContainer } from "@keystone-6/core/admin-ui/components";

export function CustomNavigation({ lists, authenticatedItem }: NavigationProps) {
  const [userRole, setUserRole] = useState<string | null>(null);
  const roleQuery = useQuery<{ authenticatedItem: { role: string } }>(
    gql(`query AuthenticatedItem {
    authenticatedItem {
      ... on User {
        role
      }
    }
  }`),
  );
  useEffect(() => {
    if (roleQuery && roleQuery.data) setUserRole(roleQuery.data?.authenticatedItem.role);
  }, [roleQuery]);

  return (
    <NavigationContainer authenticatedItem={authenticatedItem}>
      <NavItem href="/">Dashboard</NavItem>
      <ListNavItems lists={lists} />
      {userRole && userRole !== "reader" && <NavItem href="/wikidata-import">WikiData Import</NavItem>}
      {process.env.NEXT_PUBLIC_WEBSITE_PREVIEW_URL && (
        <NavItem href={process.env.NEXT_PUBLIC_PREVIEW_BASEPATH}>ChromoBase Preview</NavItem>
      )}
      {userRole && userRole === "admin" && <NavItem href="/publish">Publish</NavItem>}
    </NavigationContainer>
  );
}
