import axios from "axios";
import React, { useCallback, useState } from "react";

import { PageContainer } from "@keystone-6/core/admin-ui/components";

import { Button } from "@keystone-ui/button";
import { Box, Stack } from "@keystone-ui/core";
import { TextInput } from "@keystone-ui/fields";
import { WikidataCandidateType, WikidataImportCandidate } from "../components/WikidataImportCandidate";

const WIKIDATA_API_URL = new URL("/wikidata_api/", process.env.NEXT_PUBLIC_WIKIDATA_API_URL).toString();

export default function WikidataImport() {
  const [query, setQuery] = useState<string>("");

  const [candidates, setCandidates] = useState<WikidataCandidateType[]>([]);
  const [loadingCandidates, setLoadingCandidates] = useState<boolean>(false);

  const search = useCallback(() => {
    if (query != "") {
      const options = {
        method: "GET",
        url: WIKIDATA_API_URL,
        params: {
          action: "wbsearchentities",
          search: query,
          format: "json",
          errorformat: "plaintext",
          language: "en",
          uselang: "en",
          type: "item",
          limit: 20,
        },
      };
      setCandidates([]);
      setLoadingCandidates(true);
      axios
        .request<{ search: { id: string; label: string; description: string }[] }>(options)
        .then(function (response) {
          console.log(response.data);
          setCandidates(response.data.search);
          setLoadingCandidates(false);
        })
        .catch(function (error) {
          setLoadingCandidates(false);
          console.error(error);
        });
    }
  }, [query]);

  return (
    <PageContainer header="Import from Wikidata">
      <form
        onSubmit={(e) => {
          e.preventDefault();
          search();
        }}
      >
        <div style={{ display: "flex" }}>
          <TextInput type="text" value={query} onChange={(e) => setQuery(e.target.value)} />
          <Button type="submit" isLoading={loadingCandidates}>
            search
          </Button>
        </div>
      </form>
      <Box margin="medium" padding="medium">
        <Stack gap="medium">
          {candidates.map((p) => (
            <WikidataImportCandidate candidate={p} />
          ))}
        </Stack>
      </Box>
    </PageContainer>
  );
}
