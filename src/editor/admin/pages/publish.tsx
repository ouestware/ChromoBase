import axios from "axios";
import React, { useCallback, useEffect, useState } from "react";

import { PageContainer } from "@keystone-6/core/admin-ui/components";

import { Button } from "@keystone-ui/button";
import { Box, Stack } from "@keystone-ui/core";

export default function BuildStatic() {
  const [buildResult, setBuildResult] = useState<{ status: number; log: string } | null>(null);
  const [loading, setLoading] = useState<NodeJS.Timeout | null>(null);

  const getLog = useCallback(async () => {
    const logContent = await axios
      .get<string>("/rest/publish.log")
      .catch(() => setBuildResult({ status: 404, log: "" }));
    if (logContent) setBuildResult({ status: logContent.status, log: logContent.data });
  }, [setBuildResult]);
  useEffect(() => {
    getLog();
  }, []);

  return (
    <PageContainer header="Publish">
      <div style={{ display: "flex" }}>
        <Button
          isLoading={!!loading}
          onClick={async () => {
            setLoading(setInterval(getLog, 2000));
            try {
              await axios.get<string>("/rest/build");
              //setBuildResult({ status: r.status, log: r.data });
              getLog();
            } catch (error) {
              console.error(error);
              setBuildResult({ status: 500, log: error.toString() });
              alert(`Publication went wrong: ${error}`);
            } finally {
              if (loading) clearInterval(loading);
              setLoading(null);
            }
          }}
        >
          publish in production
        </Button>
      </div>

      <Box margin="medium" padding="medium">
        <Stack gap="medium">
          <h2>Last publication log:</h2>
          {buildResult && (
            <textarea
              key={buildResult.log.length}
              style={{ border: "1px solid black", width: "100%", height: "60vh" }}
              disabled
              value={buildResult.log.replace(/[\b\0\v\f]/g, "")}
            />
          )}
        </Stack>
      </Box>
    </PageContainer>
  );
}
