import { gql, useLazyQuery, useMutation } from "@keystone-6/core/admin-ui/apollo";
import { useList } from "@keystone-6/core/admin-ui/context";
import { mapValues } from "lodash";
import { useCallback } from "react";
import { ImportableList } from "../components/WikidataCandidateImportButton";
import { PreviewObjectType } from "../components/WikidataCandidatePreview";

export const useCreateIfNew = (listName: ImportableList) => {
  const list = useList(listName);

  const [queryExistingData, { data: existingData, loading }] = useLazyQuery(
    gql`query($where: ${list.gqlNames.whereUniqueInputName}!) {
    item:${list.gqlNames.itemQueryName}(where: $where) {
      id 
      label: ${list.labelField}
    }
  }`,
  );

  //TODO: handle error in Toast
  const [createItem, { data: createdData, loading: creationLoading }] = useMutation(
    gql`mutation($data: ${list.gqlNames.createInputName}!) {
      item: ${list.gqlNames.createMutationName}(data: $data) {
        id
        label: ${list.labelField}
    }
  }`,
  );

  const createIfNew = useCallback(
    async (object: PreviewObjectType) => {
      const r = await queryExistingData({
        variables: {
          where: { wikidataId: object.wikidataId },
        },
        fetchPolicy: "no-cache",
      });
      if (r && r.data?.item) {
        // already exist return id and label
        return r.data.item;
      } else {
        const payload: Record<string, unknown> = mapValues(object, (value, key) => {
          const field = list.fields[key];
          const fieldMeta = field.fieldMeta as { refListKey?: string; documentFeatures?: Record<string, unknown> };

          // document field: wrap value into document structure as first paragraph
          if (fieldMeta.documentFeatures !== undefined && value)
            return [
              {
                type: "paragraph",
                children: [{ text: value }],
              },
            ];
          return value;
        });

        // create
        const d = await createItem({ variables: { data: payload } });

        return d.data.item;
      }
    },
    [queryExistingData, createItem],
  );

  return { queryExistingData, createIfNew, existingData, loading, createdData, creationLoading };
};
