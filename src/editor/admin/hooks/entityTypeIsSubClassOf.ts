import axios from "axios";
import { range, sortBy, toPairs } from "lodash";
import { SparqlResults, SparqlValueType, simplifySparqlResults } from "wikibase-sdk";

export interface ConceptIdenfication {
  label: string;
  model: "Glossary" | "Organisation" | "Event";
  wikidataId: string;
  description: string;
  identificationProperty: keyof typeof IDENTIFICATION_METHOD;
}

export const GLOSSARY_TYPES: ConceptIdenfication[] = [
  {
    label: "event",
    model: "Event",
    wikidataId: "Q1656682",
    description: "Temporary and scheduled happening, like a conference, festival, competition or similar.",
    identificationProperty: "P31+",
  },
  {
    label: "organisation",
    model: "Organisation",
    wikidataId: "Q43229",
    description: "Social entity established to meet needs or pursue goals",
    identificationProperty: "P31|P279+",
  },
  {
    label: "chemical",
    model: "Glossary",
    wikidataId: "Q11173",
    description: "Pure chemical substance consisting of two or more different chemical elements",
    identificationProperty: "P279+",
  },
  {
    label: "natural material",

    model: "Glossary",
    wikidataId: "Q3405827",
    description: "Product or physical matter that arises without the use of technology",
    identificationProperty: "P279+",
  },

  {
    label: "tool",

    model: "Glossary",
    wikidataId: "Q39546",
    description: "Physical item that can be used to achieve a goal.",
    identificationProperty: "P279+",
  },
  {
    label: "artifact",
    model: "Glossary",
    wikidataId: "Q8205328",
    description: "Physical object made or shaped by humans",
    identificationProperty: "P279+",
  },
  {
    label: "art genre",
    model: "Glossary",
    wikidataId: "Q1792379",
    description: "form of art in terms of a medium, format or theme",
    identificationProperty: "P31|P279+",
  },
  {
    label: "art genre",
    model: "Glossary",
    wikidataId: "Q1792379",
    description: "form of art in terms of a medium, format or theme",
    identificationProperty: "P279+",
  },
  {
    label: "cultural movement",
    model: "Glossary",
    wikidataId: "Q2198855",
    description: "period and movement in cultural history",
    identificationProperty: "P31+",
  },
  {
    label: "process",

    model: "Glossary",
    wikidataId: "Q3249551",
    description: "Series of events which occur over an extended period of time",
    identificationProperty: "P279+",
  },
];

const IDENTIFICATION_METHOD = {
  "P31|P279+": (entityId: string, concept: ConceptIdenfication, order: number) => `OPTIONAL { 
    wd:${entityId} wdt:P31 $t
    BIND(EXISTS{$t wdt:P279+ wd:${concept.wikidataId}} as ?${order})}`,
  "P31+": (entityId: string, concept: ConceptIdenfication, order: number) =>
    `BIND(EXISTS{wd:${entityId} wdt:P31+ wd:${concept.wikidataId}} as ?${order})`,
  "P279+": (entityId: string, concept: ConceptIdenfication, order: number) =>
    `BIND(EXISTS{wd:${entityId} wdt:P279+ wd:${concept.wikidataId}} as ?${order})`,
};

export const getEntityModel = async (entityId: string): Promise<ConceptIdenfication | undefined> => {
  const selectFields = range(0, GLOSSARY_TYPES.length)
    .map((order) => `?${order}`)
    .join(" ");

  const sparqlQuery = `SELECT ${selectFields} 
    WHERE {
      ${GLOSSARY_TYPES.map((concept, order) =>
        IDENTIFICATION_METHOD[concept.identificationProperty](entityId, concept, order),
      ).join("\n")}    }
    LIMIT 1`;

  const r = await axios.get<SparqlResults>("https://query.wikidata.org/sparql", { params: { query: sparqlQuery } });

  const results = simplifySparqlResults(r.data)[0] as Record<string, SparqlValueType>;
  const firstMatchingConceptOrder = sortBy(toPairs(results), ([order]) => order).find(([, value]) => value === true);
  if (firstMatchingConceptOrder) return GLOSSARY_TYPES[firstMatchingConceptOrder[0]];
  return undefined;
};
