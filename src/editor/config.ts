// LOAD ENV VARS FROM ROOT .ENV FILE
import { config as dotenv } from "dotenv";
import dotenvExpand from "dotenv-expand";

dotenvExpand.expand(dotenv());

const config = {
  database: {
    url: `${process.env.POSTGRES_DOMAIN || "localhost"}:${process.env.POSTGRES_PORT || 5432}`,
    user: process.env.POSTGRES_USER || "chromobase",
    password: process.env.POSTGRES_PASSWORD || "changeme",
    database: process.env.POSTGRES_DATABASE || "chromobase",
  },
} as const;

export default config;
