import { build as buildIiif } from "biiif";
import { copyFile, mkdir, rm } from "fs/promises";
import path from "path";
import { Node } from "slate";

import type { Lists } from ".keystone/types";
import { graphql, list } from "@keystone-6/core";
import { allOperations, denyAll } from "@keystone-6/core/access";
import { checkbox, image, integer, password, relationship, select, text, virtual } from "@keystone-6/core/fields";
import { DocumentRendererProps } from "@keystone-6/document-renderer";
import { document } from "@keystone-6/fields-document";

import { ParagraphNode, RelationshipNode } from "@chromobase/website/src/components/narrative/documentRenderer/types";
import { fromPairs, groupBy, intersection, keys, mapValues } from "lodash";
import { adminOnly, allEditors, allReaders } from "./auth";
import { isoDate } from "./fields/IsoDateField";
import { color } from "./fields/color";
import { hsvToText } from "./fields/color/HsvColor";
import {
  NarrativeCommonFields,
  extractRelationshipsFromParagraph,
  narrativeRelationships,
  narrativeSlugHook,
  narrativeTranslationSlugHook,
} from "./schema/Narrative";
import { objectList } from "./schema/Object";
import { personLabel } from "./schema/Person";
import { logoList, staticContentList } from "./schema/StaticContent";
import { WikidataCommonFields } from "./schema/Wikidata";
import { slugHookFactory, slugify, timestampsFields } from "./schema/utils";

export const lists: Lists = {
  User: list({
    access: {
      operation: {
        query: allReaders,
        delete: adminOnly,
        create: adminOnly,
        update: allEditors,
      },
      filter: {
        update: ({ session }) => {
          return session.data?.role === "admin" ? true : { id: { equals: session.data.id } };
        },
      },
    },

    // this is the fields for our User list
    fields: {
      name: text({ validation: { isRequired: true }, isIndexed: "unique" }),

      email: text({
        isIndexed: "unique",
        db: { isNullable: true },
      }),
      isAdmin: checkbox(),
      role: select({
        validation: { isRequired: true },
        options: [
          { value: "admin", label: "admin" },
          { value: "editor", label: "editor" },
          { value: "reader", label: "reader" },
        ],
        access: {
          update: adminOnly,
        },
      }),
      password: password({ validation: { isRequired: true } }),

      author: relationship({ ref: "Author.user" }),

      ...timestampsFields,
    },
  }),
  Author: list({
    access: {
      operation: {
        ...allOperations(allEditors),
        query: allReaders,
      },
    },
    fields: {
      name: text({ validation: { isRequired: true } }),
      presentation: text(),
      personalPageURL: text(),
      narratives: relationship({ ref: "Narrative.authors", many: true }),
      user: relationship({ ref: "User.author" }),
      ...timestampsFields,
    },
  }),
  /**
   * Narrative: text which unfold the storis of aniline dyes inventions on one particular issue.
   * Central editorial pieces of the chromobase.
   * Notices are linked directly from the text.
   */
  Narrative: list({
    access: {
      operation: {
        ...allOperations(allEditors),
        query: allReaders,
      },
    },

    fields: {
      // Title, Document and language: main fields duplicated in translatedNarrative
      ...NarrativeCommonFields,
      cover: relationship({
        ref: "Media",
        ui: {
          displayMode: "cards",
          cardFields: ["title", "image"],
          linkToItem: true,
          inlineConnect: true,
        },
      }),
      authors: relationship({
        ref: "Author.narratives",
        ui: {
          displayMode: "cards",
          cardFields: ["name", "presentation"],
          inlineEdit: { fields: ["name"] },
          linkToItem: true,
          inlineConnect: true,
        },
        many: true,
      }),
      date: isoDate({ validation: { isRequired: true } }),
      category: relationship({
        ref: "NarrativeCategory.narratives",
        ui: {
          displayMode: "select",
          hideCreate: true,
        },
      }),
      extraReferences: relationship({
        ref: "Reference.narrativesExtra",
        many: true,
        ui: {
          displayMode: "cards",
          cardFields: ["citation"],
          inlineEdit: { fields: ["note", "citation"] },
          linkToItem: false,
          inlineConnect: true,
          inlineCreate: { fields: ["note", "citation"] },
        },
      }),
      people: relationship({
        ref: "Person.narratives",
        many: true,
        ui: {
          displayMode: "count",
          hideCreate: true,
        },
      }),
      objects: relationship({
        ref: "Object.narratives",
        many: true,
        ui: {
          displayMode: "count",
          hideCreate: true,
        },
      }),
      colours: relationship({
        ref: "Colour.narratives",
        many: true,
        ui: {
          displayMode: "count",
          hideCreate: true,
        },
      }),
      organisations: relationship({
        ref: "Organisation.narratives",
        many: true,
        ui: {
          displayMode: "count",
          hideCreate: true,
        },
      }),
      glossary: relationship({
        ref: "Glossary.narratives",
        many: true,
        ui: {
          displayMode: "count",
          hideCreate: true,
        },
      }),
      events: relationship({
        ref: "Event.narratives",
        many: true,
        ui: {
          displayMode: "count",
          hideCreate: true,
        },
      }),
      references: relationship({
        ref: "Reference.narratives",
        many: true,
        ui: {
          displayMode: "count",
          hideCreate: true,
        },
      }),
      translations: relationship({
        ref: "NarrativeTranslation.translatedNarrative",
        many: true,
      }),
      ...timestampsFields,
    },
    hooks: {
      resolveInput: async (props) => {
        // apply slug hook
        const resolvedData = await narrativeSlugHook(props);

        const { item } = props;
        if (resolvedData.content) {
          // content was modified

          // retrieve existing relationships from API if it's not a creation
          let existingRelationships: Record<keyof typeof narrativeRelationships, { id: string }[]> = mapValues(
            narrativeRelationships,
            () => [],
          );
          if (item) {
            //sadly item does not include relationships field
            //let's retrieve them from GQL
            const gqlResult = (await props.context.graphql.run({
              query: `query Narrative($where: NarrativeWhereUniqueInput!) {
            narrative(where: $where) {
              ${keys(narrativeRelationships).map((k) => ` ${k} { id } `)}
            }
          }`,
              variables: { where: { id: item.id } },
            })) as { narrative: Record<keyof typeof narrativeRelationships, { id: string }[]> };

            existingRelationships = {
              ...existingRelationships,
              ...gqlResult.narrative,
            };
          }

          //extract relationships from document
          const document = resolvedData.content as DocumentRendererProps["document"];
          const relationships: RelationshipNode[] = [];
          for (const paragraph of document) {
            if ("type" in paragraph && paragraph.type === "paragraph") {
              const extracted = extractRelationshipsFromParagraph(paragraph as unknown as ParagraphNode);
              for (const r of extracted) {
                //filter out broken relationships
                const listKey = narrativeRelationships[r.relationship].listKey;
                if (r.data && r.data.id && props.context.db[listKey]) {
                  // check if target exists in db
                  const exist = await props.context.db[listKey].findOne({ where: { id: r.data.id } });
                  if (exist === null) {
                    //todo: we could remove the broken relationship from content. A method removeRelationshipsFromDocument to do that has been left unfinished in ./schema/Narrative.ts
                    console.warn(`ignoring a broken link in narrative ${item?.id}: ${r.relationship} ${r.data.id}`);
                  } else relationships.push(r);
                }
              }
            }
          }

          //update relationship at object level
          if (relationships.length > 0) {
            // group actual state by type
            const currentRelationshipsByType = groupBy(relationships, (r) => r.relationship);
            // compare actual and previous state to compute connect and disconnect params
            const relationshipsConnectByType = fromPairs(
              // use all possible keys to make sure we spot disconnection
              keys(narrativeRelationships)
                .map((relationshipType) => {
                  //compute ids lists
                  const newConnectionsIds = (currentRelationshipsByType[relationshipType] || [])
                    .map((rel) => rel.data?.id)
                    .filter((e): e is string => e !== undefined);
                  const oldConnectionsIds: string[] =
                    existingRelationships && existingRelationships[relationshipType]
                      ? existingRelationships[relationshipType].map((e) => e.id)
                      : [];

                  // spot connections and disconnections
                  const same = intersection(newConnectionsIds, oldConnectionsIds);
                  const disconnect = oldConnectionsIds.filter((o) => !same.includes(o));
                  const connect = newConnectionsIds.filter((n) => !same.includes(n));
                  // apply patch on object level relationship fields
                  if (connect.length > 0 || disconnect.length > 0)
                    return [
                      relationshipType,
                      {
                        disconnect: disconnect.map((d) => ({ id: d })),
                        connect: connect.map((d) => ({ id: d })),
                      },
                    ];
                  else return null;
                })
                // keep only non null patches
                .filter((r): r is [string, { disconnect: { id: string }[]; connect: { id: string }[] }] => r !== null),
            );
            return { ...resolvedData, ...relationshipsConnectByType };
          }
        }
        return resolvedData;
      },
    },
  }),
  NarrativeTranslation: list({
    access: {
      operation: {
        ...allOperations(allEditors),
        query: allReaders,
      },
    },
    fields: {
      ...NarrativeCommonFields,
      translatedNarrative: relationship({ ref: "Narrative.translations" }),
      ...timestampsFields,
    },
    hooks: {
      resolveInput: narrativeTranslationSlugHook,
    },
  }),
  // this last list is our Tag list, it only has a name field for now
  NarrativeCategory: list({
    access: {
      operation: {
        ...allOperations(adminOnly),
        query: allReaders,
      },
    },

    // this is the fields for our Tag list
    fields: {
      slug: text({ isIndexed: "unique" }),
      title: text(),
      description: document({
        links: true,
        formatting: {
          inlineMarks: {
            bold: true,
            italic: true,
            underline: true,
            strikethrough: true,
            superscript: true,
            subscript: true,
          },
        },
      }),
      // this can be helpful to find out all the Posts associated with a Tag
      narratives: relationship({ ref: "Narrative.category", many: true }),
      ...timestampsFields,
    },
  }),
  Person: list({
    access: {
      operation: {
        ...allOperations(allEditors),
        query: allReaders,
      },
    },
    fields: {
      slug: text({ isIndexed: "unique" }),
      label: virtual({
        field: graphql.field({
          type: graphql.String,
          resolve(item) {
            return personLabel({ item, resolvedData: {} }) as string;
          },
        }),
      }),
      firstName: text(),
      lastName: text(),
      fullName: text(),
      biography: document({
        links: true,
        formatting: {
          inlineMarks: {
            bold: true,
            italic: true,
            underline: true,
            strikethrough: true,
            superscript: true,
            subscript: true,
          },
        },
      }),
      dateOfBirth: isoDate(),
      dateOfDeath: isoDate(),
      occupations: relationship({
        ref: "Occupation",
        many: true,
      }),
      narratives: relationship({
        ref: "Narrative.people",
        many: true,
        ui: {
          displayMode: "count",
          hideCreate: true,
          createView: { fieldMode: "hidden" },
        },
      }),
      ...WikidataCommonFields,
      ...timestampsFields,
    },
    hooks: {
      resolveInput: slugHookFactory<Lists.Person.TypeInfo>((p) => slugify(personLabel(p) as string)),
    },
  }),
  Occupation: list({
    access: {
      operation: {
        ...allOperations(allEditors),
        query: allReaders,
      },
    },
    fields: {
      label: text(),
      description: document({
        links: true,
        formatting: {
          inlineMarks: {
            bold: true,
            italic: true,
            underline: true,
            strikethrough: true,
            superscript: true,
            subscript: true,
          },
        },
      }),
      ...WikidataCommonFields,
      ...timestampsFields,
    },
  }),
  Organisation: list({
    access: {
      operation: {
        ...allOperations(allEditors),
        query: allReaders,
      },
    },
    fields: {
      label: text({ validation: { isRequired: true }, isIndexed: "unique" }),
      slug: text({ isIndexed: "unique" }),
      type: relationship({
        ref: "OrganisationType",
        ui: {
          displayMode: "select",
        },
      }),
      dateOfInception: isoDate(),
      description: document({
        links: true,
        formatting: {
          inlineMarks: {
            bold: true,
            italic: true,
            underline: true,
            strikethrough: true,
            superscript: true,
            subscript: true,
          },
        },
      }),
      narratives: relationship({
        ref: "Narrative.organisations",
        many: true,
        ui: {
          displayMode: "count",
          hideCreate: true,
          createView: { fieldMode: "hidden" },
        },
      }),
      ...WikidataCommonFields,
      ...timestampsFields,
    },
    hooks: {
      resolveInput: slugHookFactory<Lists.Organisation.TypeInfo>((p) =>
        slugify((p.resolvedData.label as string) || p.item?.label || ""),
      ),
    },
  }),
  OrganisationType: list({
    access: {
      operation: {
        ...allOperations(allEditors),
        query: allReaders,
      },
    },
    fields: {
      label: text({ validation: { isRequired: true }, isIndexed: "unique" }),
      description: document({
        links: true,
        formatting: {
          inlineMarks: {
            bold: true,
            italic: true,
            underline: true,
            strikethrough: true,
            superscript: true,
            subscript: true,
          },
        },
      }),

      ...WikidataCommonFields,
      ...timestampsFields,
    },
    ui: {
      isHidden: true,
    },
  }),
  /**
   * Object: glossary of materials and other process used in the industry or research around aniline dyes
   * We might want to rename this list as Materials or Materiality or Glossary as it's a list of concepts and not poitner to precise objects that are preserved.
   */
  Glossary: list({
    access: {
      operation: {
        ...allOperations(allEditors),
        query: allReaders,
      },
    },
    fields: {
      label: text({ validation: { isRequired: true }, isIndexed: "unique" }),
      slug: text({ isIndexed: "unique" }),
      description: document({
        links: true,
        formatting: {
          inlineMarks: {
            bold: true,
            italic: true,
            underline: true,
            strikethrough: true,
            superscript: true,
            subscript: true,
          },
        },
      }),
      type: relationship({
        ref: "GlossaryType",
        ui: {
          displayMode: "select",
        },
      }),
      chemicalFormula: text(),
      dateOfDiscoveryInvention: isoDate(),
      medias: relationship({ ref: "Media", many: true }),
      narratives: relationship({
        ref: "Narrative.glossary",
        many: true,
        ui: {
          displayMode: "count",
          hideCreate: true,

          createView: { fieldMode: "hidden" },
        },
      }),
      ...WikidataCommonFields,
      ...timestampsFields,
    },
    hooks: {
      resolveInput: slugHookFactory<Lists.Glossary.TypeInfo>((p) =>
        slugify((p.resolvedData.label as string) || p.item?.label || ""),
      ),
    },
  }),
  GlossaryType: list({
    access: {
      operation: {
        ...allOperations(allEditors),
        query: allReaders,
      },
    },
    fields: {
      label: text({ validation: { isRequired: true }, isIndexed: "unique" }),
      category: select({
        options: [
          { value: "technique", label: "technique" },
          { value: "glossary", label: "glossary" },
        ],
        validation: { isRequired: true },
        defaultValue: "technique",
      }),
      description: document({
        links: true,
        formatting: {
          inlineMarks: {
            bold: true,
            italic: true,
            underline: true,
            strikethrough: true,
            superscript: true,
            subscript: true,
          },
        },
      }),
      ...WikidataCommonFields,
      ...timestampsFields,
    },
  }),
  /**
   * Object: object displayed to demonstrate a concept or show an example, e.g. in a museum
   * In ChromoBase Object from CNAM museum will be displayed in High definition pictures
   * For instance: a bottle of chemical compound, a sample of dyed fabric, a dress, a painting...
   * Object are not Object as they represent the virtual replication of one exact object which we can
   */
  Object: list(objectList),
  ObjectType: list({
    access: {
      operation: {
        ...allOperations(allEditors),
        query: allReaders,
      },
    },
    fields: {
      label: text({ validation: { isRequired: true }, isIndexed: "unique" }),
      ...timestampsFields,
    },
    ui: {
      isHidden: true,
    },
  }),
  /**
   * Reference: bibliographic metadata to source documents or archival material
   * If a picture from a source needs to be displayed than this picture has to be a Media embeded in an Object object.
   * Reference only stores bibliographic information.
   */
  Reference: list({
    access: {
      operation: {
        ...allOperations(allEditors),
        query: allReaders,
      },
    },
    fields: {
      label: text({
        access: { update: denyAll },
        ui: {
          createView: { fieldMode: "hidden" },
          itemView: { fieldMode: "hidden" },
          listView: { fieldMode: "hidden" },
        },
      }),
      citation: document({
        links: true,
        formatting: {
          inlineMarks: {
            bold: true,
            italic: true,
            underline: true,
            strikethrough: true,
            superscript: true,
            subscript: true,
          },
        },
      }),
      slug: text({ isIndexed: "unique" }),
      note: text({ isIndexed: "unique", validation: { isRequired: true } }),
      medias: relationship({
        ref: "Media",
        ui: {
          displayMode: "cards",
          cardFields: ["title", "image"],
          linkToItem: true,
          inlineConnect: true,
          inlineEdit: { fields: ["title", "order", "image", "type", "embed", "url"] },
          inlineCreate: { fields: ["title", "order", "image", "type", "embed", "url"] },
        },
        many: true,
      }),
      narratives: relationship({
        ref: "Narrative.references",
        many: true,
        ui: {
          displayMode: "count",
          hideCreate: true,

          createView: { fieldMode: "hidden" },
        },
      }),
      narrativesExtra: relationship({ ref: "Narrative.extraReferences", many: true }),
      ...timestampsFields,
    },
    ui: {
      searchFields: ["citation"],
    },
    hooks: {
      resolveInput: ({ resolvedData, item }) => {
        const serialize = (nodes) => {
          return nodes.map((n) => Node.string(n)).join("\n");
        };
        const citation = resolvedData.citation !== undefined ? resolvedData.citation : item?.citation;
        const note =
          resolvedData.note !== undefined
            ? (resolvedData.note as string)
            : item?.note || serialize(citation as unknown as Node[]).slice(0, 50);
        const slug = slugify(note);
        return { ...resolvedData, slug, note, label: note };
      },
    },
  }),
  /**
   * Media: technical information about a picture to be able to display it
   * Source and description to be added in an Object object.
   */
  Media: list({
    graphql: {
      plural: "medias",
    },
    access: {
      operation: {
        ...allOperations(allEditors),
        query: allReaders,
      },
    },
    fields: {
      title: text(),
      order: integer(),
      image: image({ storage: "local_images" }),
      url: text(),
      embed: text({ ui: { displayMode: "textarea" } }),
      type: select({
        validation: { isRequired: true },
        options: [
          { value: "image", label: "Image" },
          { value: "remote", label: "Remote image" },
          { value: "image_hd", label: "High Definition Image" },
          { value: "embed", label: "Embed content" },
        ],
      }),
      ...timestampsFields,
    },
    hooks: {
      afterOperation: async ({ item, originalItem, operation }) => {
        const absoluteRootPath = path.join(process.cwd(), "./public/images/");
        if (
          operation !== "delete" &&
          item?.type === "image_hd" &&
          (item.image_id !== originalItem?.image_id || originalItem?.type !== "image_hd")
        ) {
          const originalFilePath = path.join(absoluteRootPath, `${item.image_id}.${item.image_extension}`);
          const folderPath = path.join(absoluteRootPath, `${item.image_id}/`);

          switch (operation) {
            case "create":
            case "update":
              // create a IIIF Folder and then do as update
              try {
                await mkdir(folderPath);
              } catch (e) {
                // if dir already exist that's normal situation else throw
                if (e.code !== "EEXIST") throw e;
              }
              // copy
              await copyFile(originalFilePath, `${folderPath}image.${item.image_extension}`);
              // biiif
              console.log("building IIIF...");
              await buildIiif(
                folderPath,
                `${process.env.EDITOR_PUBLIC_URL}${process.env.EDITOR_BASEPATH}/images/${item.image_id}`,
              );

              break;
          }
        }
        if (operation === "delete" && originalItem?.type === "image_hd") {
          //wipe IIIF folder from filesystem
          const folderPath = path.join(absoluteRootPath, `_${originalItem.image_id}/`);
          await rm(folderPath, { recursive: true });
        }
      },
    },
  }),
  Event: list({
    access: {
      operation: {
        ...allOperations(allEditors),
        query: allReaders,
      },
    },

    fields: {
      label: text({ validation: { isRequired: true }, isIndexed: "unique" }),
      slug: text({ isIndexed: "unique" }),
      type: relationship({
        ref: "EventType",
        ui: {
          displayMode: "select",
        },
      }),
      startDate: isoDate(),
      endDate: isoDate(),
      place: text(),
      description: document({
        links: true,
        formatting: {
          inlineMarks: {
            bold: true,
            italic: true,
            underline: true,
            strikethrough: true,
            superscript: true,
            subscript: true,
          },
        },
      }),
      ...WikidataCommonFields,
      narratives: relationship({
        ref: "Narrative.events",
        many: true,
        ui: {
          displayMode: "count",
          hideCreate: true,
          createView: { fieldMode: "hidden" },
        },
      }),
      ...timestampsFields,
    },
    hooks: {
      resolveInput: slugHookFactory<Lists.Event.TypeInfo>((p) =>
        slugify((p.resolvedData.label as string) || p.item?.label || ""),
      ),
    },
  }),
  EventType: list({
    access: {
      operation: {
        ...allOperations(allEditors),
        query: allReaders,
      },
    },
    fields: {
      label: text({ validation: { isRequired: true }, isIndexed: "unique" }),
      description: document({
        links: true,
        formatting: {
          inlineMarks: {
            bold: true,
            italic: true,
            underline: true,
            strikethrough: true,
            superscript: true,
            subscript: true,
          },
        },
      }),

      ...WikidataCommonFields,
      ...timestampsFields,
    },
    ui: {
      isHidden: true,
    },
  }),
  Colour: list({
    access: {
      operation: {
        ...allOperations(allEditors),
        query: allReaders,
      },
    },
    fields: {
      label: text({
        ui: {
          createView: { fieldMode: "hidden" },
          itemView: { fieldMode: "hidden" },
          // listView: { fieldMode: "hidden" },
        },
      }),
      name: text(),
      color: color(),
      colorOrder: text({
        ui: {
          createView: { fieldMode: "hidden" },
          itemView: { fieldMode: "hidden" },
          //listView: { fieldMode: "hidden" },
        },
      }),
      narratives: relationship({
        ref: "Narrative.colours",
        many: true,
        ui: {
          displayMode: "count",
          hideCreate: true,
          createView: { fieldMode: "hidden" },
        },
        ...timestampsFields,
      }),
    },

    hooks: {
      resolveInput: ({ resolvedData, item }) => {
        const name = resolvedData.name !== undefined ? (resolvedData.name as string) : item?.name;
        const color =
          resolvedData.color !== undefined
            ? (resolvedData.color as { h: number; s: number; v: number })
            : item
              ? { h: item.color_h, s: item.color_s, v: item.color_v }
              : undefined;
        const colorOrder =
          color !== undefined
            ? `h:${("" + color.h).padStart(3, "0")} s:${("" + color.s).padStart(2, "0")} v:${("" + color.v).padStart(
                2,
                "0",
              )}`
            : "no color";
        const label = name ? name : color !== undefined ? hsvToText(color) : "no color";
        return { ...resolvedData, label, colorOrder };
      },
    },
  }),
  StaticContent: list(staticContentList),
  Logo: list(logoList),
};
