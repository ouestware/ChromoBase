-- CreateTable
CREATE TABLE "User" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL DEFAULT '',
    "email" TEXT NOT NULL DEFAULT '',
    "isAdmin" BOOLEAN NOT NULL DEFAULT false,
    "role" TEXT NOT NULL,
    "password" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "User_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Author" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL DEFAULT '',
    "presentation" TEXT NOT NULL DEFAULT '',
    "user" TEXT,

    CONSTRAINT "Author_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Story" (
    "id" TEXT NOT NULL,
    "title" TEXT NOT NULL DEFAULT '',
    "slug" TEXT NOT NULL DEFAULT '',
    "date" TEXT,
    "language" TEXT DEFAULT 'en',
    "content" JSONB NOT NULL DEFAULT '[{"type":"paragraph","children":[{"text":""}]}]',
    "category" TEXT,

    CONSTRAINT "Story_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "StoryTranslation" (
    "id" TEXT NOT NULL,
    "title" TEXT NOT NULL DEFAULT '',
    "slug" TEXT NOT NULL DEFAULT '',
    "date" TEXT,
    "language" TEXT DEFAULT 'en',
    "content" JSONB NOT NULL DEFAULT '[{"type":"paragraph","children":[{"text":""}]}]',
    "translatedStory" TEXT,

    CONSTRAINT "StoryTranslation_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "StoryCategory" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL DEFAULT '',
    "description" TEXT NOT NULL DEFAULT '',

    CONSTRAINT "StoryCategory_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Person" (
    "id" TEXT NOT NULL,
    "firstName" TEXT NOT NULL DEFAULT '',
    "lastName" TEXT NOT NULL DEFAULT '',
    "fullName" TEXT NOT NULL DEFAULT '',
    "biography" JSONB NOT NULL DEFAULT '[{"type":"paragraph","children":[{"text":""}]}]',
    "dateOfBirth" TEXT,
    "dateOfDeath" TEXT,
    "wikidataId" TEXT NOT NULL DEFAULT '',
    "wikidataItem" JSONB,

    CONSTRAINT "Person_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Occupation" (
    "id" TEXT NOT NULL,
    "label" TEXT NOT NULL DEFAULT '',
    "description" JSONB NOT NULL DEFAULT '[{"type":"paragraph","children":[{"text":""}]}]',
    "wikidataId" TEXT NOT NULL DEFAULT '',
    "wikidataItem" JSONB,

    CONSTRAINT "Occupation_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Organisation" (
    "id" TEXT NOT NULL,
    "label" TEXT NOT NULL DEFAULT '',
    "dateOfInception" TEXT,
    "description" JSONB NOT NULL DEFAULT '[{"type":"paragraph","children":[{"text":""}]}]',
    "wikidataId" TEXT NOT NULL DEFAULT '',
    "wikidataItem" JSONB,

    CONSTRAINT "Organisation_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Glossary" (
    "id" TEXT NOT NULL,
    "label" TEXT NOT NULL DEFAULT '',
    "description" JSONB NOT NULL DEFAULT '[{"type":"paragraph","children":[{"text":""}]}]',
    "type" TEXT,
    "chemicalFormula" TEXT NOT NULL DEFAULT '',
    "dateOfDiscoveryInvention" TEXT,
    "wikidataId" TEXT NOT NULL DEFAULT '',
    "wikidataItem" JSONB,

    CONSTRAINT "Glossary_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Exhibit" (
    "id" TEXT NOT NULL,
    "label" TEXT NOT NULL DEFAULT '',
    "description" JSONB NOT NULL DEFAULT '[{"type":"paragraph","children":[{"text":""}]}]',
    "date" TEXT,
    "reference" TEXT,

    CONSTRAINT "Exhibit_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Reference" (
    "id" TEXT NOT NULL,
    "citation" JSONB NOT NULL DEFAULT '[{"type":"paragraph","children":[{"text":""}]}]',
    "date" TEXT,

    CONSTRAINT "Reference_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Media" (
    "id" TEXT NOT NULL,
    "title" TEXT NOT NULL DEFAULT '',
    "image_filesize" INTEGER,
    "image_extension" TEXT,
    "image_width" INTEGER,
    "image_height" INTEGER,
    "image_id" TEXT,
    "url" TEXT NOT NULL DEFAULT '',
    "type" TEXT,

    CONSTRAINT "Media_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Event" (
    "id" TEXT NOT NULL,
    "label" TEXT NOT NULL DEFAULT '',
    "startDate" TEXT,
    "endDate" TEXT,
    "place" TEXT NOT NULL DEFAULT '',
    "description" JSONB NOT NULL DEFAULT '[{"type":"paragraph","children":[{"text":""}]}]',
    "wikidataId" TEXT NOT NULL DEFAULT '',
    "wikidataItem" JSONB,

    CONSTRAINT "Event_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Color" (
    "id" TEXT NOT NULL,
    "color_h" DOUBLE PRECISION NOT NULL,
    "color_s" DOUBLE PRECISION NOT NULL,
    "color_v" DOUBLE PRECISION NOT NULL,

    CONSTRAINT "Color_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "_Author_stories" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_Person_occupations" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_Glossary_medias" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_Exhibit_medias" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_Exhibit_colors" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "User_name_key" ON "User"("name");

-- CreateIndex
CREATE UNIQUE INDEX "User_email_key" ON "User"("email");

-- CreateIndex
CREATE UNIQUE INDEX "Author_user_key" ON "Author"("user");

-- CreateIndex
CREATE UNIQUE INDEX "Story_title_key" ON "Story"("title");

-- CreateIndex
CREATE UNIQUE INDEX "Story_slug_key" ON "Story"("slug");

-- CreateIndex
CREATE INDEX "Story_category_idx" ON "Story"("category");

-- CreateIndex
CREATE UNIQUE INDEX "StoryTranslation_title_key" ON "StoryTranslation"("title");

-- CreateIndex
CREATE UNIQUE INDEX "StoryTranslation_slug_key" ON "StoryTranslation"("slug");

-- CreateIndex
CREATE INDEX "StoryTranslation_translatedStory_idx" ON "StoryTranslation"("translatedStory");

-- CreateIndex
CREATE UNIQUE INDEX "Person_wikidataId_key" ON "Person"("wikidataId");

-- CreateIndex
CREATE UNIQUE INDEX "Occupation_wikidataId_key" ON "Occupation"("wikidataId");

-- CreateIndex
CREATE UNIQUE INDEX "Organisation_wikidataId_key" ON "Organisation"("wikidataId");

-- CreateIndex
CREATE UNIQUE INDEX "Glossary_wikidataId_key" ON "Glossary"("wikidataId");

-- CreateIndex
CREATE INDEX "Exhibit_reference_idx" ON "Exhibit"("reference");

-- CreateIndex
CREATE UNIQUE INDEX "Event_wikidataId_key" ON "Event"("wikidataId");

-- CreateIndex
CREATE UNIQUE INDEX "_Author_stories_AB_unique" ON "_Author_stories"("A", "B");

-- CreateIndex
CREATE INDEX "_Author_stories_B_index" ON "_Author_stories"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_Person_occupations_AB_unique" ON "_Person_occupations"("A", "B");

-- CreateIndex
CREATE INDEX "_Person_occupations_B_index" ON "_Person_occupations"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_Glossary_medias_AB_unique" ON "_Glossary_medias"("A", "B");

-- CreateIndex
CREATE INDEX "_Glossary_medias_B_index" ON "_Glossary_medias"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_Exhibit_medias_AB_unique" ON "_Exhibit_medias"("A", "B");

-- CreateIndex
CREATE INDEX "_Exhibit_medias_B_index" ON "_Exhibit_medias"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_Exhibit_colors_AB_unique" ON "_Exhibit_colors"("A", "B");

-- CreateIndex
CREATE INDEX "_Exhibit_colors_B_index" ON "_Exhibit_colors"("B");

-- AddForeignKey
ALTER TABLE "Author" ADD CONSTRAINT "Author_user_fkey" FOREIGN KEY ("user") REFERENCES "User"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Story" ADD CONSTRAINT "Story_category_fkey" FOREIGN KEY ("category") REFERENCES "StoryCategory"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "StoryTranslation" ADD CONSTRAINT "StoryTranslation_translatedStory_fkey" FOREIGN KEY ("translatedStory") REFERENCES "Story"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Exhibit" ADD CONSTRAINT "Exhibit_reference_fkey" FOREIGN KEY ("reference") REFERENCES "Reference"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Author_stories" ADD CONSTRAINT "_Author_stories_A_fkey" FOREIGN KEY ("A") REFERENCES "Author"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Author_stories" ADD CONSTRAINT "_Author_stories_B_fkey" FOREIGN KEY ("B") REFERENCES "Story"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Person_occupations" ADD CONSTRAINT "_Person_occupations_A_fkey" FOREIGN KEY ("A") REFERENCES "Occupation"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Person_occupations" ADD CONSTRAINT "_Person_occupations_B_fkey" FOREIGN KEY ("B") REFERENCES "Person"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Glossary_medias" ADD CONSTRAINT "_Glossary_medias_A_fkey" FOREIGN KEY ("A") REFERENCES "Glossary"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Glossary_medias" ADD CONSTRAINT "_Glossary_medias_B_fkey" FOREIGN KEY ("B") REFERENCES "Media"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Exhibit_medias" ADD CONSTRAINT "_Exhibit_medias_A_fkey" FOREIGN KEY ("A") REFERENCES "Exhibit"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Exhibit_medias" ADD CONSTRAINT "_Exhibit_medias_B_fkey" FOREIGN KEY ("B") REFERENCES "Media"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Exhibit_colors" ADD CONSTRAINT "_Exhibit_colors_A_fkey" FOREIGN KEY ("A") REFERENCES "Color"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Exhibit_colors" ADD CONSTRAINT "_Exhibit_colors_B_fkey" FOREIGN KEY ("B") REFERENCES "Exhibit"("id") ON DELETE CASCADE ON UPDATE CASCADE;
