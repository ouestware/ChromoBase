/*
  Warnings:

  - You are about to drop the column `date` on the `Reference` table. All the data in the column will be lost.
  - You are about to drop the `Color` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Exhibit` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Story` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `StoryCategory` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `StoryTranslation` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `_Author_stories` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `_Exhibit_colors` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `_Exhibit_medias` table. If the table is not empty, all the data it contains will be lost.
  - A unique constraint covering the columns `[label]` on the table `Event` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[slug]` on the table `Event` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[label]` on the table `Glossary` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[slug]` on the table `Glossary` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[label]` on the table `Organisation` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[slug]` on the table `Organisation` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[slug]` on the table `Person` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[slug]` on the table `Reference` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[note]` on the table `Reference` will be added. If there are existing duplicate values, this will fail.
  - Made the column `type` on table `Media` required. This step will fail if there are existing NULL values in that column.

*/
-- DropForeignKey
ALTER TABLE "Exhibit" DROP CONSTRAINT "Exhibit_reference_fkey";

-- DropForeignKey
ALTER TABLE "Story" DROP CONSTRAINT "Story_category_fkey";

-- DropForeignKey
ALTER TABLE "StoryTranslation" DROP CONSTRAINT "StoryTranslation_translatedStory_fkey";

-- DropForeignKey
ALTER TABLE "_Author_stories" DROP CONSTRAINT "_Author_stories_A_fkey";

-- DropForeignKey
ALTER TABLE "_Author_stories" DROP CONSTRAINT "_Author_stories_B_fkey";

-- DropForeignKey
ALTER TABLE "_Exhibit_colors" DROP CONSTRAINT "_Exhibit_colors_A_fkey";

-- DropForeignKey
ALTER TABLE "_Exhibit_colors" DROP CONSTRAINT "_Exhibit_colors_B_fkey";

-- DropForeignKey
ALTER TABLE "_Exhibit_medias" DROP CONSTRAINT "_Exhibit_medias_A_fkey";

-- DropForeignKey
ALTER TABLE "_Exhibit_medias" DROP CONSTRAINT "_Exhibit_medias_B_fkey";

-- AlterTable
ALTER TABLE "Event" ADD COLUMN     "slug" TEXT NOT NULL DEFAULT '',
ADD COLUMN     "type" TEXT;

-- AlterTable
ALTER TABLE "Glossary" ADD COLUMN     "slug" TEXT NOT NULL DEFAULT '';

-- AlterTable
ALTER TABLE "Media" ADD COLUMN     "embed" TEXT NOT NULL DEFAULT '',
ALTER COLUMN "type" SET NOT NULL;

-- AlterTable
ALTER TABLE "Organisation" ADD COLUMN     "slug" TEXT NOT NULL DEFAULT '',
ADD COLUMN     "type" TEXT;

-- AlterTable
ALTER TABLE "Person" ADD COLUMN     "slug" TEXT NOT NULL DEFAULT '';

-- AlterTable
ALTER TABLE "Reference" DROP COLUMN "date",
ADD COLUMN     "label" TEXT NOT NULL DEFAULT '',
ADD COLUMN     "note" TEXT NOT NULL DEFAULT '',
ADD COLUMN     "slug" TEXT NOT NULL DEFAULT '';

-- DropTable
DROP TABLE "Color";

-- DropTable
DROP TABLE "Exhibit";

-- DropTable
DROP TABLE "Story";

-- DropTable
DROP TABLE "StoryCategory";

-- DropTable
DROP TABLE "StoryTranslation";

-- DropTable
DROP TABLE "_Author_stories";

-- DropTable
DROP TABLE "_Exhibit_colors";

-- DropTable
DROP TABLE "_Exhibit_medias";

-- CreateTable
CREATE TABLE "Narrative" (
    "id" TEXT NOT NULL,
    "title" TEXT NOT NULL DEFAULT '',
    "slug" TEXT NOT NULL DEFAULT '',
    "selectForHome" BOOLEAN NOT NULL DEFAULT false,
    "status" TEXT DEFAULT 'private',
    "language" TEXT DEFAULT 'en',
    "content" JSONB NOT NULL DEFAULT '[{"type":"paragraph","children":[{"text":""}]}]',
    "cover" TEXT,
    "date" TEXT,
    "category" TEXT,

    CONSTRAINT "Narrative_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "NarrativeTranslation" (
    "id" TEXT NOT NULL,
    "title" TEXT NOT NULL DEFAULT '',
    "slug" TEXT NOT NULL DEFAULT '',
    "selectForHome" BOOLEAN NOT NULL DEFAULT false,
    "status" TEXT DEFAULT 'private',
    "language" TEXT DEFAULT 'en',
    "content" JSONB NOT NULL DEFAULT '[{"type":"paragraph","children":[{"text":""}]}]',
    "translatedNarrative" TEXT,

    CONSTRAINT "NarrativeTranslation_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "NarrativeCategory" (
    "id" TEXT NOT NULL,
    "slug" TEXT NOT NULL DEFAULT '',
    "title" TEXT NOT NULL DEFAULT '',
    "description" JSONB NOT NULL DEFAULT '[{"type":"paragraph","children":[{"text":""}]}]',

    CONSTRAINT "NarrativeCategory_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "OrganisationType" (
    "id" TEXT NOT NULL,
    "label" TEXT NOT NULL DEFAULT '',
    "description" JSONB NOT NULL DEFAULT '[{"type":"paragraph","children":[{"text":""}]}]',
    "wikidataId" TEXT NOT NULL DEFAULT '',
    "wikidataItem" JSONB,

    CONSTRAINT "OrganisationType_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "GlossaryType" (
    "id" TEXT NOT NULL,
    "label" TEXT NOT NULL DEFAULT '',
    "description" JSONB NOT NULL DEFAULT '[{"type":"paragraph","children":[{"text":""}]}]',
    "wikidataId" TEXT NOT NULL DEFAULT '',
    "wikidataItem" JSONB,

    CONSTRAINT "GlossaryType_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Object" (
    "id" TEXT NOT NULL,
    "label" TEXT NOT NULL DEFAULT '',
    "slug" TEXT NOT NULL DEFAULT '',
    "author" TEXT NOT NULL DEFAULT '',
    "location" TEXT NOT NULL DEFAULT '',
    "digitalizedByChromotope" BOOLEAN NOT NULL DEFAULT false,
    "description" JSONB NOT NULL DEFAULT '[{"type":"paragraph","children":[{"text":""}]}]',
    "date" TEXT,
    "cover" TEXT,
    "type" TEXT,
    "reference" TEXT,

    CONSTRAINT "Object_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "ObjectType" (
    "id" TEXT NOT NULL,
    "label" TEXT NOT NULL DEFAULT '',

    CONSTRAINT "ObjectType_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "EventType" (
    "id" TEXT NOT NULL,
    "label" TEXT NOT NULL DEFAULT '',
    "description" JSONB NOT NULL DEFAULT '[{"type":"paragraph","children":[{"text":""}]}]',
    "wikidataId" TEXT NOT NULL DEFAULT '',
    "wikidataItem" JSONB,

    CONSTRAINT "EventType_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Colour" (
    "id" TEXT NOT NULL,
    "label" TEXT NOT NULL DEFAULT '',
    "name" TEXT NOT NULL DEFAULT '',
    "color_h" DOUBLE PRECISION NOT NULL,
    "color_s" DOUBLE PRECISION NOT NULL,
    "color_v" DOUBLE PRECISION NOT NULL,
    "colorOrder" TEXT NOT NULL DEFAULT '',

    CONSTRAINT "Colour_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "_Author_narratives" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_Narrative_extraReferences" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_Narrative_people" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_Narrative_objects" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_Narrative_organisations" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_Narrative_references" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_Glossary_narratives" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_Object_medias" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_Reference_medias" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_Event_narratives" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_Colour_narratives" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_Object_colours" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "Narrative_title_key" ON "Narrative"("title");

-- CreateIndex
CREATE UNIQUE INDEX "Narrative_slug_key" ON "Narrative"("slug");

-- CreateIndex
CREATE INDEX "Narrative_cover_idx" ON "Narrative"("cover");

-- CreateIndex
CREATE INDEX "Narrative_category_idx" ON "Narrative"("category");

-- CreateIndex
CREATE UNIQUE INDEX "NarrativeTranslation_title_key" ON "NarrativeTranslation"("title");

-- CreateIndex
CREATE UNIQUE INDEX "NarrativeTranslation_slug_key" ON "NarrativeTranslation"("slug");

-- CreateIndex
CREATE INDEX "NarrativeTranslation_translatedNarrative_idx" ON "NarrativeTranslation"("translatedNarrative");

-- CreateIndex
CREATE UNIQUE INDEX "NarrativeCategory_slug_key" ON "NarrativeCategory"("slug");

-- CreateIndex
CREATE UNIQUE INDEX "OrganisationType_label_key" ON "OrganisationType"("label");

-- CreateIndex
CREATE UNIQUE INDEX "OrganisationType_wikidataId_key" ON "OrganisationType"("wikidataId");

-- CreateIndex
CREATE UNIQUE INDEX "GlossaryType_label_key" ON "GlossaryType"("label");

-- CreateIndex
CREATE UNIQUE INDEX "GlossaryType_wikidataId_key" ON "GlossaryType"("wikidataId");

-- CreateIndex
CREATE UNIQUE INDEX "Object_label_key" ON "Object"("label");

-- CreateIndex
CREATE UNIQUE INDEX "Object_slug_key" ON "Object"("slug");

-- CreateIndex
CREATE INDEX "Object_cover_idx" ON "Object"("cover");

-- CreateIndex
CREATE INDEX "Object_type_idx" ON "Object"("type");

-- CreateIndex
CREATE INDEX "Object_reference_idx" ON "Object"("reference");

-- CreateIndex
CREATE UNIQUE INDEX "ObjectType_label_key" ON "ObjectType"("label");

-- CreateIndex
CREATE UNIQUE INDEX "EventType_label_key" ON "EventType"("label");

-- CreateIndex
CREATE UNIQUE INDEX "EventType_wikidataId_key" ON "EventType"("wikidataId");

-- CreateIndex
CREATE UNIQUE INDEX "_Author_narratives_AB_unique" ON "_Author_narratives"("A", "B");

-- CreateIndex
CREATE INDEX "_Author_narratives_B_index" ON "_Author_narratives"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_Narrative_extraReferences_AB_unique" ON "_Narrative_extraReferences"("A", "B");

-- CreateIndex
CREATE INDEX "_Narrative_extraReferences_B_index" ON "_Narrative_extraReferences"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_Narrative_people_AB_unique" ON "_Narrative_people"("A", "B");

-- CreateIndex
CREATE INDEX "_Narrative_people_B_index" ON "_Narrative_people"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_Narrative_objects_AB_unique" ON "_Narrative_objects"("A", "B");

-- CreateIndex
CREATE INDEX "_Narrative_objects_B_index" ON "_Narrative_objects"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_Narrative_organisations_AB_unique" ON "_Narrative_organisations"("A", "B");

-- CreateIndex
CREATE INDEX "_Narrative_organisations_B_index" ON "_Narrative_organisations"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_Narrative_references_AB_unique" ON "_Narrative_references"("A", "B");

-- CreateIndex
CREATE INDEX "_Narrative_references_B_index" ON "_Narrative_references"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_Glossary_narratives_AB_unique" ON "_Glossary_narratives"("A", "B");

-- CreateIndex
CREATE INDEX "_Glossary_narratives_B_index" ON "_Glossary_narratives"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_Object_medias_AB_unique" ON "_Object_medias"("A", "B");

-- CreateIndex
CREATE INDEX "_Object_medias_B_index" ON "_Object_medias"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_Reference_medias_AB_unique" ON "_Reference_medias"("A", "B");

-- CreateIndex
CREATE INDEX "_Reference_medias_B_index" ON "_Reference_medias"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_Event_narratives_AB_unique" ON "_Event_narratives"("A", "B");

-- CreateIndex
CREATE INDEX "_Event_narratives_B_index" ON "_Event_narratives"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_Colour_narratives_AB_unique" ON "_Colour_narratives"("A", "B");

-- CreateIndex
CREATE INDEX "_Colour_narratives_B_index" ON "_Colour_narratives"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_Object_colours_AB_unique" ON "_Object_colours"("A", "B");

-- CreateIndex
CREATE INDEX "_Object_colours_B_index" ON "_Object_colours"("B");

-- CreateIndex
CREATE UNIQUE INDEX "Event_label_key" ON "Event"("label");

-- CreateIndex
CREATE UNIQUE INDEX "Event_slug_key" ON "Event"("slug");

-- CreateIndex
CREATE INDEX "Event_type_idx" ON "Event"("type");

-- CreateIndex
CREATE UNIQUE INDEX "Glossary_label_key" ON "Glossary"("label");

-- CreateIndex
CREATE UNIQUE INDEX "Glossary_slug_key" ON "Glossary"("slug");

-- CreateIndex
CREATE INDEX "Glossary_type_idx" ON "Glossary"("type");

-- CreateIndex
CREATE UNIQUE INDEX "Organisation_label_key" ON "Organisation"("label");

-- CreateIndex
CREATE UNIQUE INDEX "Organisation_slug_key" ON "Organisation"("slug");

-- CreateIndex
CREATE INDEX "Organisation_type_idx" ON "Organisation"("type");

-- CreateIndex
CREATE UNIQUE INDEX "Person_slug_key" ON "Person"("slug");

-- CreateIndex
CREATE UNIQUE INDEX "Reference_slug_key" ON "Reference"("slug");

-- CreateIndex
CREATE UNIQUE INDEX "Reference_note_key" ON "Reference"("note");

-- AddForeignKey
ALTER TABLE "Narrative" ADD CONSTRAINT "Narrative_cover_fkey" FOREIGN KEY ("cover") REFERENCES "Media"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Narrative" ADD CONSTRAINT "Narrative_category_fkey" FOREIGN KEY ("category") REFERENCES "NarrativeCategory"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "NarrativeTranslation" ADD CONSTRAINT "NarrativeTranslation_translatedNarrative_fkey" FOREIGN KEY ("translatedNarrative") REFERENCES "Narrative"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Organisation" ADD CONSTRAINT "Organisation_type_fkey" FOREIGN KEY ("type") REFERENCES "OrganisationType"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Glossary" ADD CONSTRAINT "Glossary_type_fkey" FOREIGN KEY ("type") REFERENCES "GlossaryType"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Object" ADD CONSTRAINT "Object_cover_fkey" FOREIGN KEY ("cover") REFERENCES "Media"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Object" ADD CONSTRAINT "Object_type_fkey" FOREIGN KEY ("type") REFERENCES "ObjectType"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Object" ADD CONSTRAINT "Object_reference_fkey" FOREIGN KEY ("reference") REFERENCES "Reference"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Event" ADD CONSTRAINT "Event_type_fkey" FOREIGN KEY ("type") REFERENCES "EventType"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Author_narratives" ADD CONSTRAINT "_Author_narratives_A_fkey" FOREIGN KEY ("A") REFERENCES "Author"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Author_narratives" ADD CONSTRAINT "_Author_narratives_B_fkey" FOREIGN KEY ("B") REFERENCES "Narrative"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Narrative_extraReferences" ADD CONSTRAINT "_Narrative_extraReferences_A_fkey" FOREIGN KEY ("A") REFERENCES "Narrative"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Narrative_extraReferences" ADD CONSTRAINT "_Narrative_extraReferences_B_fkey" FOREIGN KEY ("B") REFERENCES "Reference"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Narrative_people" ADD CONSTRAINT "_Narrative_people_A_fkey" FOREIGN KEY ("A") REFERENCES "Narrative"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Narrative_people" ADD CONSTRAINT "_Narrative_people_B_fkey" FOREIGN KEY ("B") REFERENCES "Person"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Narrative_objects" ADD CONSTRAINT "_Narrative_objects_A_fkey" FOREIGN KEY ("A") REFERENCES "Narrative"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Narrative_objects" ADD CONSTRAINT "_Narrative_objects_B_fkey" FOREIGN KEY ("B") REFERENCES "Object"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Narrative_organisations" ADD CONSTRAINT "_Narrative_organisations_A_fkey" FOREIGN KEY ("A") REFERENCES "Narrative"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Narrative_organisations" ADD CONSTRAINT "_Narrative_organisations_B_fkey" FOREIGN KEY ("B") REFERENCES "Organisation"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Narrative_references" ADD CONSTRAINT "_Narrative_references_A_fkey" FOREIGN KEY ("A") REFERENCES "Narrative"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Narrative_references" ADD CONSTRAINT "_Narrative_references_B_fkey" FOREIGN KEY ("B") REFERENCES "Reference"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Glossary_narratives" ADD CONSTRAINT "_Glossary_narratives_A_fkey" FOREIGN KEY ("A") REFERENCES "Glossary"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Glossary_narratives" ADD CONSTRAINT "_Glossary_narratives_B_fkey" FOREIGN KEY ("B") REFERENCES "Narrative"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Object_medias" ADD CONSTRAINT "_Object_medias_A_fkey" FOREIGN KEY ("A") REFERENCES "Media"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Object_medias" ADD CONSTRAINT "_Object_medias_B_fkey" FOREIGN KEY ("B") REFERENCES "Object"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Reference_medias" ADD CONSTRAINT "_Reference_medias_A_fkey" FOREIGN KEY ("A") REFERENCES "Media"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Reference_medias" ADD CONSTRAINT "_Reference_medias_B_fkey" FOREIGN KEY ("B") REFERENCES "Reference"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Event_narratives" ADD CONSTRAINT "_Event_narratives_A_fkey" FOREIGN KEY ("A") REFERENCES "Event"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Event_narratives" ADD CONSTRAINT "_Event_narratives_B_fkey" FOREIGN KEY ("B") REFERENCES "Narrative"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Colour_narratives" ADD CONSTRAINT "_Colour_narratives_A_fkey" FOREIGN KEY ("A") REFERENCES "Colour"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Colour_narratives" ADD CONSTRAINT "_Colour_narratives_B_fkey" FOREIGN KEY ("B") REFERENCES "Narrative"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Object_colours" ADD CONSTRAINT "_Object_colours_A_fkey" FOREIGN KEY ("A") REFERENCES "Colour"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Object_colours" ADD CONSTRAINT "_Object_colours_B_fkey" FOREIGN KEY ("B") REFERENCES "Object"("id") ON DELETE CASCADE ON UPDATE CASCADE;
