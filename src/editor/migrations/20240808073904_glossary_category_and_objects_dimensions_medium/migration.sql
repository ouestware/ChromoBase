-- AlterTable
ALTER TABLE "GlossaryType" ADD COLUMN     "category" TEXT NOT NULL DEFAULT 'technique';

-- AlterTable
ALTER TABLE "Object" ADD COLUMN     "dimensions" TEXT NOT NULL DEFAULT '',
ADD COLUMN     "medium" TEXT NOT NULL DEFAULT '';
