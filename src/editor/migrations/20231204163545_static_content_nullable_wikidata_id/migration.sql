-- AlterTable
ALTER TABLE "Event" ALTER COLUMN "wikidataId" DROP NOT NULL,
ALTER COLUMN "wikidataId" DROP DEFAULT;

-- AlterTable
ALTER TABLE "EventType" ALTER COLUMN "wikidataId" DROP NOT NULL,
ALTER COLUMN "wikidataId" DROP DEFAULT;

-- AlterTable
ALTER TABLE "Glossary" ALTER COLUMN "wikidataId" DROP NOT NULL,
ALTER COLUMN "wikidataId" DROP DEFAULT;

-- AlterTable
ALTER TABLE "GlossaryType" ALTER COLUMN "wikidataId" DROP NOT NULL,
ALTER COLUMN "wikidataId" DROP DEFAULT;

-- AlterTable
ALTER TABLE "Occupation" ALTER COLUMN "wikidataId" DROP NOT NULL,
ALTER COLUMN "wikidataId" DROP DEFAULT;

-- AlterTable
ALTER TABLE "Organisation" ALTER COLUMN "wikidataId" DROP NOT NULL,
ALTER COLUMN "wikidataId" DROP DEFAULT;

-- AlterTable
ALTER TABLE "OrganisationType" ALTER COLUMN "wikidataId" DROP NOT NULL,
ALTER COLUMN "wikidataId" DROP DEFAULT;

-- AlterTable
ALTER TABLE "Person" ALTER COLUMN "wikidataId" DROP NOT NULL,
ALTER COLUMN "wikidataId" DROP DEFAULT;

-- AlterTable
ALTER TABLE "User" ALTER COLUMN "email" DROP NOT NULL,
ALTER COLUMN "email" DROP DEFAULT;

-- CreateTable
CREATE TABLE "StaticContent" (
    "id" INTEGER NOT NULL,
    "baseline" TEXT NOT NULL DEFAULT '',
    "colourWheelIntroduction" JSONB NOT NULL DEFAULT '[{"type":"paragraph","children":[{"text":""}]}]',
    "about" JSONB NOT NULL DEFAULT '[{"type":"paragraph","children":[{"text":""}]}]',
    "credits" JSONB NOT NULL DEFAULT '[{"type":"paragraph","children":[{"text":""}]}]',
    "funding" JSONB NOT NULL DEFAULT '[{"type":"paragraph","children":[{"text":""}]}]',
    "legalNotice" JSONB NOT NULL DEFAULT '[{"type":"paragraph","children":[{"text":""}]}]',

    CONSTRAINT "StaticContent_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Logo" (
    "id" TEXT NOT NULL,
    "image_filesize" INTEGER,
    "image_extension" TEXT,
    "image_width" INTEGER,
    "image_height" INTEGER,
    "image_id" TEXT,
    "institutionName" TEXT NOT NULL DEFAULT '',
    "url" TEXT NOT NULL DEFAULT '',
    "order" INTEGER,

    CONSTRAINT "Logo_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "_StaticContent_partnersLogos" (
    "A" TEXT NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateTable
CREATE TABLE "_StaticContent_fundingLogos" (
    "A" TEXT NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "_StaticContent_partnersLogos_AB_unique" ON "_StaticContent_partnersLogos"("A", "B");

-- CreateIndex
CREATE INDEX "_StaticContent_partnersLogos_B_index" ON "_StaticContent_partnersLogos"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_StaticContent_fundingLogos_AB_unique" ON "_StaticContent_fundingLogos"("A", "B");

-- CreateIndex
CREATE INDEX "_StaticContent_fundingLogos_B_index" ON "_StaticContent_fundingLogos"("B");

-- AddForeignKey
ALTER TABLE "_StaticContent_partnersLogos" ADD CONSTRAINT "_StaticContent_partnersLogos_A_fkey" FOREIGN KEY ("A") REFERENCES "Logo"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_StaticContent_partnersLogos" ADD CONSTRAINT "_StaticContent_partnersLogos_B_fkey" FOREIGN KEY ("B") REFERENCES "StaticContent"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_StaticContent_fundingLogos" ADD CONSTRAINT "_StaticContent_fundingLogos_A_fkey" FOREIGN KEY ("A") REFERENCES "Logo"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_StaticContent_fundingLogos" ADD CONSTRAINT "_StaticContent_fundingLogos_B_fkey" FOREIGN KEY ("B") REFERENCES "StaticContent"("id") ON DELETE CASCADE ON UPDATE CASCADE;
