import type { Context } from ".keystone/types";
import type { Request, Response } from "express";
import { exec } from "node:child_process";
import fs from "node:fs/promises";
import util from "node:util";

export async function publish(req: Request, res: Response) {
  // This was added by the context middleware in ../keystone.ts
  const { context } = req as typeof req & { context: Context };

  if (context.session && context.session.data.role === "admin") {
    // start publish script
    try {
      console.log("starting a publish process...");
      //const { stdout } =
      await util.promisify(exec)("cd ../website/; bash ./deploy.static.sh > ../website/publish.log");
      //await fs.writeFile("../website/publish.log", stdout);
      //res.send(stdout);
      console.log("publish process succeeded");
      res.sendStatus(200);
    } catch (error) {
      console.log("publish process failed with error", error);
      res.sendStatus(500);
      res.end(error.toString());
    }
  } else {
    res.sendStatus(403);
  }
}
export async function publishLog(req: Request, res: Response) {
  // This was added by the context middleware in ../keystone.ts
  const { context } = req as typeof req & { context: Context };

  if (context.session && context.session.data.role === "admin") {
    // start publish script
    const logFilePath = "../website/publish.log";
    try {
      await fs.access(logFilePath);
      const logContent = await fs.readFile(logFilePath, { encoding: "utf8" });
      res.send(logContent);

      res.end();
    } catch (error) {
      res.statusMessage = error.toString();
      res.sendStatus(404);
    }
  } else {
    res.sendStatus(401);
  }
}
