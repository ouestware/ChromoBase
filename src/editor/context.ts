import type { Context } from ".keystone/types";
import { getContext } from "@keystone-6/core/context";
import * as PrismaModule from "@prisma/client";
import config from "./keystone";

// Making sure multiple prisma clients are not created during hot reloading
// eslint-disable-next-line  @typescript-eslint/no-explicit-any
export const keystoneContext: Context = (globalThis as any).keystoneContext || getContext(config, PrismaModule);

if (process.env.NODE_ENV !== "production") {
  // eslint-disable-next-line  @typescript-eslint/no-explicit-any
  (globalThis as any).keystoneContext = keystoneContext;
}
