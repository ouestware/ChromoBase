// Welcome to Keystone!
//
// This file is what Keystone uses as the entry-point to your headless backend
//
// Keystone imports the default export of this file, expecting a Keystone configuration object
//   you can find out more at https://keystonejs.com/docs/apis/config
import type { Context } from ".keystone/types";
import { config } from "@keystone-6/core";

import proxy from "express-http-proxy";
import localConfig from "./config";

// to keep this file tidy, we define our schema in a different file
import { lists } from "./schema";

// authentication is configured separately here too, but you might move this elsewhere
// when you write your list-level access control functions, as they typically rely on session data
import { trimEnd } from "lodash";
import { session, withAuth } from "./auth";
import { publish, publishLog } from "./rest/publish";

export default withAuth(
  config({
    db: {
      // we're using sqlite for the fastest startup experience
      //   for more information on what database might be appropriate for you
      //   see https://keystonejs.com/docs/guides/choosing-a-database#title
      provider: "postgresql",
      url: `postgres://${localConfig.database.user}:${localConfig.database.password}@${localConfig.database.url}/${localConfig.database.database}`,
    },
    ui: {
      basePath: process.env.EDITOR_BASEPATH,
    },
    lists,
    session,
    storage: {
      local_images: {
        kind: "local",
        type: "image",
        generateUrl: (path) =>
          `${process.env.EDITOR_PUBLIC_URL}${trimEnd(process.env.EDITOR_BASEPATH, "/")}/images${path}`,
        serverRoute: {
          path: `${trimEnd(process.env.EDITOR_BASEPATH, "/")}/images`,
        },
        storagePath: "./public/images",
      },
    },
    server: {
      /*
        This is the main part of this example. Here we include a function that
        takes the express app Keystone created, and does two things:
        - Adds a middleware function that will run on requests matching our REST
          API routes, to get a keystone context on `req`. This means we don't
          need to put our route handlers in a closure and repeat it for each.
        - Adds a GET handler for tasks, which will query for tasks in the
          Keystone schema and return the results as JSON
      */
      // cors: temporary hack for IIIF external viewer
      cors: { origin: "*" },
      extendHttpServer: (server) => {
        const was = server.timeout;
        server.setTimeout(process.env.NODE_ENV !== "production" ? 0 : 10 * 60 * 1000); //disable timeout which default to 120,000 in prod
        console.log(`HTTP Server Timeout was ${was}, now set to ${server.timeout}`);
      },
      extendExpressApp: (app, commonContext) => {
        app.use("/rest", async (req, res, next) => {
          /*
            WARNING: normally if you're adding custom properties to an
            express request type, you might extend the global Express namespace...
            ... we're not doing that here because we're in a Typescript monorepo
            so we're casting the request instead :)
          */
          (req as typeof req & { context: Context }).context = await commonContext.withRequest(req, res);
          next();
        });

        app.get("/rest/build", publish);
        app.get("/rest/publish.log", publishLog);

        // Proxy to preview server
        app.use(`${process.env.EDITOR_BASEPATH}${process.env.PREVIEW_BASEPATH}`, async (req, res, next) => {
          const context = await commonContext.withRequest(req, res);
          if (process.env.WEBSITE_PREVIEW_URL) {
            if (context.session)
              proxy(
                process.env.WEBSITE_PREVIEW_URL,

                {
                  proxyReqPathResolver: async function (request) {
                    const u = new URL(request.url, `http://${request.headers.host}`);
                    u.pathname = `${process.env.EDITOR_BASEPATH}${process.env.PREVIEW_BASEPATH}${u.pathname}`;
                    return u.pathname + u.search + u.hash;
                  },
                },
              )(req, res, next);
            else res.status(403).redirect("/signin");
          } else res.status(404).end();
        });
      },
    },
  }),
);
