declare module "bifff" {
  type build = (dir: string, url: string, virtualName?: string) => Promise<void>;
}
