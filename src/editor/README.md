# ChromoBase Editor

## Data model

### Narrative

Text which unfolds aniline dyes inventions history on one particular issue.
Central editorial pieces of the chromobase, narratives are written by an identified author who is/are credited on the public website.
Notices from the chromobase are linked directly from the text.

### Notices

Notices are piece of data which defines the actors, events, concepts, documents which were part of the history of aniline dyes inventions.

Notices can be seen as encyclopedia entries stating definition and facts. Notices are often linked to wikidata entities. It's not an exhaustive actor-network database depicting all relations between those entities. It's more a glossary of actors (including non-humans' ones) encountered in narratives. Notice should not exist if nor citing in one narrative/Object.

Notices are:

- persons
- objects (might change the name to materials, materiality, technical glossary...)
  - chemical compound: fuchsine
  - natural material: wool, cotton...
  - tool, artifact (?): horizontal loom, vertical loom...
  - process : weaving, dyeing...
- events
- organisations
- occupations

### Objects

Objects preserved in some collection which have been digitized in various form to be displayed in the chromobase.
Objects are the second editorial pinpoint of the chromobase revealing the materiality behind the narratives written by the team. Their descriptions tend to be larger than notices' one to better describe their origin and materiality. Those description will cite notices the same way narratives do.

For instance: a bottle of chemical compound, a sample of dyed fabric, a dress, an illustration in a journal, a painting...

### References

Bibliographic metadata to source documents or archival material.
If a picture from a source needs to be displayed than this picture has to be a Media embedded in an Object object.
Reference only stores bibliographic information.

### Media

Technical information about a picture to be able to display it.
Source and description to be added in an Object object.
