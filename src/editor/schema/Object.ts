import type { Lists } from ".keystone/types";
import { ListConfig } from "@keystone-6/core";
import { allOperations } from "@keystone-6/core/access";
import { checkbox, relationship, text } from "@keystone-6/core/fields";
import { document } from "@keystone-6/fields-document";
import { allEditors, allReaders } from "../auth";
import { isoDate } from "../fields/IsoDateField";
import { slugHookFactory, slugify, timestampsFields } from "./utils";

const objectSlug = slugHookFactory<Lists.Object.TypeInfo>(({ resolvedData, item }) =>
  slugify((resolvedData.label as string) || item?.label || ""),
);

export const objectList: ListConfig<Lists.Object.TypeInfo> = {
  access: {
    operation: {
      ...allOperations(allEditors),
      query: allReaders,
    },
  },
  fields: {
    label: text({ validation: { isRequired: true }, isIndexed: "unique" }),
    slug: text({ isIndexed: "unique" }),
    author: text(),
    location: text(),
    digitalizedByChromotope: checkbox(),
    medium: text(),
    dimensions: text(),
    description: document({
      links: true,
      formatting: {
        inlineMarks: {
          bold: true,
          italic: true,
          underline: true,
          strikethrough: true,
          superscript: true,
          subscript: true,
        },
      },
    }),
    date: isoDate(),
    cover: relationship({
      ref: "Media",
      ui: {
        displayMode: "cards",
        cardFields: ["title", "image"],
        linkToItem: true,
        inlineConnect: true,
        inlineEdit: { fields: ["title", "image", "type", "embed", "url"] },
        inlineCreate: { fields: ["title", "image", "type", "embed", "url"] },
      },
    }),
    type: relationship({
      ref: "ObjectType",
      ui: {
        displayMode: "select",
      },
    }),
    medias: relationship({
      ref: "Media",
      ui: {
        displayMode: "cards",
        cardFields: ["title", "image"],
        linkToItem: true,
        inlineConnect: true,
        inlineEdit: { fields: ["title", "order", "image", "type", "embed", "url"] },
        inlineCreate: { fields: ["title", "order", "image", "type", "embed", "url"] },
      },
      many: true,
    }),
    reference: relationship({
      ref: "Reference",
      ui: {
        displayMode: "cards",
        cardFields: ["citation"],
        inlineEdit: { fields: ["note", "citation"] },
        linkToItem: false,
        inlineConnect: true,
        inlineCreate: { fields: ["note", "citation"] },
      },
    }),
    colours: relationship({
      ref: "Colour",
      ui: {
        displayMode: "cards",
        cardFields: ["color"],
        inlineEdit: { fields: ["color"] },
        linkToItem: false,
        inlineConnect: false,
        inlineCreate: { fields: ["color"] },
      },
      many: true,
    }),
    narratives: relationship({
      ref: "Narrative.objects",
      many: true,
      ui: {
        displayMode: "count",
        hideCreate: true,
        createView: { fieldMode: "hidden" },
      },
    }),
    ...timestampsFields,
  },
  hooks: {
    resolveInput: objectSlug,
  },
};
