import { timestamp } from "@keystone-6/core/fields";
import { BaseFields, BaseListTypeInfo, ListHooks } from "@keystone-6/core/types";
import { trim } from "lodash";

export function slugify(value: string): string {
  return value
    .toLowerCase()
    .normalize("NFD")
    .replace(/[\u0300-\u036f]/g, "")
    .trim()
    .replace(/[\s]/g, "-")
    .replace(/[!"#$%&'()*+,./:;<=>?@[\]^_`{|}~]/g, "");
}

export function slugHookFactory<T extends BaseListTypeInfo>(
  makeSlug: (props: { item?: T["item"]; resolvedData: T["prisma"]["update"] | T["prisma"]["create"] }) => string,
) {
  const slugResolveHook: ListHooks<T>["resolveInput"] = async (props) => {
    const { slug } = props.resolvedData;
    // update slug if title changed and slug has not been manually edited
    if (slug === undefined || trim(slug as string) === "") {
      const newSlug = makeSlug({ resolvedData: props.resolvedData, item: props.item });

      return {
        ...props.resolvedData,
        slug: newSlug,
      };
    }
    return props.resolvedData;
  };

  return slugResolveHook;
}

export const timestampsFields: BaseFields<BaseListTypeInfo> = {
  createdAt: timestamp({
    // this sets the timestamp to Date.now() when the user is first created
    defaultValue: { kind: "now" },
    ui: { createView: { fieldMode: "hidden" }, itemView: { fieldMode: "read" } },
  }),
  updatedAt: timestamp({
    // this sets the timestamp to Date.now() when the user is first created
    defaultValue: { kind: "now" },
    ui: { createView: { fieldMode: "hidden" }, itemView: { fieldMode: "read" } },
    db: { updatedAt: true },
  }),
};
