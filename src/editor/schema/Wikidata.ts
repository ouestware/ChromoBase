import { BaseFields } from "@keystone-6/core";
import { text } from "@keystone-6/core/fields";
import { BaseListTypeInfo } from "@keystone-6/core/types";

export const WikidataCommonFields: BaseFields<BaseListTypeInfo> = {
  wikidataId: text({ isIndexed: "unique", db: { isNullable: true } }),
};
