import { flatMap } from "lodash";

import type { Lists } from ".keystone/types";
import type {
  InlineNode,
  ListItemNode,
  RelationshipNode,
  TopLevelNode,
} from "@chromobase/website/src/components/narrative/documentRenderer/types";
import { BaseFields } from "@keystone-6/core";
import { checkbox, select, text } from "@keystone-6/core/fields";
import { BaseListTypeInfo } from "@keystone-6/core/types";
import { document } from "@keystone-6/fields-document";
import { slugHookFactory, slugify } from "./utils";

export const narrativeRelationships = {
  people: {
    listKey: "Person",
    label: "Person",
    selection: "label slug narrativesCount",
  },
  objects: {
    listKey: "Object",
    label: "Object",
    selection: "label slug cover { id type image {url width height} title url embed}     narrativesCount",
  },
  glossary: {
    listKey: "Glossary",
    label: "Glossary",
    selection: "label slug  type { label category}  narrativesCount",
  },
  organisations: {
    listKey: "Organisation",
    label: "Organisation",
    selection: "label slug narrativesCount",
  },
  colours: {
    listKey: "Colour",
    label: "Colour",
    selection: "label name id  color { h s v}  narrativesCount",
  },
  events: {
    listKey: "Event",
    label: "Event",
    selection: "label slug     narrativesCount",
  },
  references: {
    listKey: "Reference",
    label: "Reference",
    selection: "note label slug narrativesCount",
  },
};

export const NarrativeCommonFields: BaseFields<BaseListTypeInfo> = {
  title: text({ isIndexed: "unique", validation: { isRequired: true } }),
  slug: text({ isIndexed: "unique" }),
  selectForHome: checkbox(),
  status: select({
    options: [
      { label: "private", value: "private" },
      { label: "public", value: "public" },
    ],
    defaultValue: "private",
  }),
  language: select({
    defaultValue: "en",
    options: [
      {
        value: "en",
        label: "English",
      },
      { value: "fr", label: "Français" },
    ],
  }),
  // the document field can be used for making rich editable content
  //   you can find out more at https://keystonejs.com/docs/guides/document-fields
  content: document({
    links: true,
    formatting: {
      inlineMarks: {
        bold: true,
        italic: true,
        underline: true,
        strikethrough: true,
        superscript: true,
        subscript: true,
      },
      listTypes: { unordered: true, ordered: true },
    },
    relationships: narrativeRelationships,
  }),
};

export const extractRelationshipsFromParagraph = (
  node: TopLevelNode | InlineNode | ListItemNode,
  ignoreEmptyData?: boolean,
): RelationshipNode[] => {
  const relationship =
    "type" in node && node.type === "relationship" && node.data !== undefined && (!ignoreEmptyData || node.data?.data)
      ? [node as RelationshipNode]
      : [];

  return [
    ...relationship,
    ...("children" in node
      ? flatMap(node.children, (c: InlineNode | ListItemNode) => extractRelationshipsFromParagraph(c))
      : []),
  ];
};

function _narrativeSlugHook<T extends Lists.Narrative.TypeInfo | Lists.NarrativeTranslation.TypeInfo>() {
  return slugHookFactory<T>((props) => {
    const title = (props.resolvedData.title as string) || props.item?.title;
    return title ? slugify(title) : "";
  });
}
export const narrativeSlugHook = _narrativeSlugHook<Lists.Narrative.TypeInfo>();
export const narrativeTranslationSlugHook = _narrativeSlugHook<Lists.NarrativeTranslation.TypeInfo>();
