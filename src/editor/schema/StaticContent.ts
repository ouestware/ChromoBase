import type { Lists } from ".keystone/types";
import { ListConfig } from "@keystone-6/core";
import { allOperations } from "@keystone-6/core/access";
import { document, DocumentFieldConfig } from "@keystone-6/fields-document";

import { image, integer, relationship, text } from "@keystone-6/core/fields";
import { allEditors, allReaders } from "../auth";

const staticDocumentFormatting: DocumentFieldConfig<Lists.StaticContent.TypeInfo> = {
  links: true,
  formatting: {
    headingLevels: [3],
    inlineMarks: {
      bold: true,
      italic: true,
      underline: true,
      strikethrough: true,
      superscript: true,
      subscript: true,
    },
    listTypes: { unordered: true, ordered: true },
  },
};

export const staticContentList: ListConfig<Lists.StaticContent.TypeInfo> = {
  isSingleton: true,
  access: {
    operation: {
      ...allOperations(allEditors),
      query: allReaders,
    },
  },
  fields: {
    baseline: text({ ui: { displayMode: "textarea" } }),
    colourWheelIntroduction: document(staticDocumentFormatting),
    about: document(staticDocumentFormatting),
    credits: document(staticDocumentFormatting),
    partnersLogos: relationship({ ref: "Logo", many: true }),
    funding: document(staticDocumentFormatting),
    fundingLogos: relationship({ ref: "Logo", many: true }),
    legalNotice: document(staticDocumentFormatting),
  },
};

export const logoList: ListConfig<Lists.Logo.TypeInfo> = {
  access: {
    operation: {
      ...allOperations(allEditors),
      query: allReaders,
    },
  },
  ui: { isHidden: true, labelField: "institutionName" },

  fields: {
    image: image({ storage: "local_images" }),
    institutionName: text(),
    url: text(),
    order: integer(),
  },
};
