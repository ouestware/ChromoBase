import type { Lists } from ".keystone/types";

export const personLabel = (props: {
  item?: Lists.Person.TypeInfo["item"];
  resolvedData: Lists.Person.TypeInfo["prisma"]["update"] | Lists.Person.TypeInfo["prisma"]["create"];
}) => {
  const { resolvedData, item } = props;
  const fullName = resolvedData.fullName !== undefined ? resolvedData.fullName : item?.fullName;
  const firstName = resolvedData.firstName !== undefined ? resolvedData.firstName : item?.firstName;
  const lastName = resolvedData.lastName !== undefined ? resolvedData.lastName : item?.lastName;
  return fullName || [firstName || "", lastName || ""].join(" ");
};
