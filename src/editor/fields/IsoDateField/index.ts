import { text } from "@keystone-6/core/fields";

export const isoDate: typeof text = (props) =>
  text({
    ...props,
    db: { isNullable: true },
    ui: { views: "@chromobase/editor/fields/IsoDateField/views" },
    validation: {
      ...props?.validation,
      match: {
        regex: /^([0-9]{4})(-(1[0-2]|0[1-9])(-(3[01]|[012][0-9]))?)?$/,
        explanation: "YYYY-MM-DD or YYYY-MM or YYYY",
      },
    },
  });
