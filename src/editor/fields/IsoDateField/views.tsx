import isValid from "date-fns/isValid";
import { useCallback, useEffect, useState } from "react";
import Calendar from "react-calendar";
import "react-calendar/dist/Calendar.css";

import { CellContainer, CellLink } from "@keystone-6/core/admin-ui/components";
import { Field as TextField } from "@keystone-6/core/fields/types/text/views";
import { CellComponent } from "@keystone-6/core/types";
import { Button } from "@keystone-ui/button";
import { Inline, Stack, Text } from "@keystone-ui/core";
import { FieldContainer, FieldDescription, FieldLabel, TextInput } from "@keystone-ui/fields";
import { CalendarIcon } from "@keystone-ui/icons";
import { PopoverDialog, useControlledPopover } from "@keystone-ui/popover";

const formatIso = (date: Date, precision: "year" | "month" | "day"): string => {
  const year = "" + date.getFullYear();
  const month = ("" + (date.getMonth() + 1)).padStart(2, "0");
  const day = ("" + date.getDate()).padStart(2, "0");

  switch (precision) {
    case "year":
      return year;
    case "month":
      return `${year}-${month}`;
    case "day":
      return `${year}-${month}-${day}`;
  }
};

const validDate = (date: string | null): { valid: boolean; message?: string } => {
  if (!date) return { valid: true };

  try {
    if (!/^([0-9]{4})(-(1[0-2]|0[1-9])(-(3[01]|[012][0-9]))?)?$/.test(date)) throw new Error();
    const d = new Date(date);
    if (isValid(d)) return { valid: true };
    else return { valid: false, message: "Date incorrect: does not exist in the calendar" };
  } catch {
    return { valid: false, message: "Date must be expressed as YYYY-MM-DD or YYYY-MM or YYYY" };
  }
};

export const Field: typeof TextField = ({ field, value: fieldValue, onChange }) => {
  const value = fieldValue.inner.kind === "null" ? "" : fieldValue.inner.value;

  const [invalid, setInvalid] = useState<string | undefined>(undefined);
  const [dateInput, setDateInput] = useState<string | null>(value);
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const { dialog, trigger, arrow } = useControlledPopover(
    {
      isOpen,
      onClose: useCallback(() => {
        setIsOpen(false);
      }, [setIsOpen]),
    },
    {
      placement: "bottom-start",
      modifiers: [
        {
          name: "offset",
          options: {
            offset: [0, 8],
          },
        },
      ],
    },
  );

  useEffect(() => {
    if (dateInput !== value) {
      setDateInput(value);
    }
  }, [value]);

  return (
    <FieldContainer>
      <Stack>
        <FieldLabel>{field.label}</FieldLabel>
        <FieldDescription id={`${field.path}-description`}>{field.description}</FieldDescription>
        {onChange ? (
          <Inline gap="small">
            <Stack>
              <Stack across gap="small">
                <TextInput
                  ref={trigger.ref}
                  {...trigger.props}
                  width="medium"
                  type="text"
                  value={dateInput || ""}
                  invalid={!!invalid}
                  onChange={(e) => {
                    setDateInput(e.target.value);
                    const { valid, message } = validDate(e.target.value);
                    if (valid) {
                      setInvalid(undefined);
                      if (field.isNullable)
                        if (e.target.value !== "")
                          // update value
                          onChange({ ...fieldValue, inner: { kind: "value", value: e.target.value } });
                        // set to null
                        else onChange({ ...fieldValue, inner: { kind: "null", prev: value } });
                      else onChange({ ...fieldValue, inner: { kind: "value", value: e.target.value } });
                    } else {
                      setInvalid(message);
                    }
                  }}
                />{" "}
                <Button
                  tone={"active"}
                  onClick={() => {
                    setIsOpen(!isOpen);
                  }}
                >
                  <CalendarIcon />
                </Button>
                {isOpen && (
                  <PopoverDialog arrow={arrow} isVisible ref={dialog.ref} {...dialog.props}>
                    <Calendar
                      onClickDay={(date) => {
                        setInvalid(undefined);
                        onChange({
                          ...fieldValue,
                          inner: { kind: "value", value: formatIso(date, "day") },
                        });
                      }}
                      onClickMonth={(date) => {
                        setInvalid(undefined);
                        onChange({
                          ...fieldValue,
                          inner: { kind: "value", value: formatIso(date, "month") },
                        });
                      }}
                      onClickYear={(date) => {
                        setInvalid(undefined);
                        onChange({ ...fieldValue, inner: { kind: "value", value: formatIso(date, "year") } });
                      }}
                      value={value}
                    />
                  </PopoverDialog>
                )}
              </Stack>
              {invalid && (
                <Text color="red600" size="small">
                  {invalid}
                </Text>
              )}
            </Stack>
          </Inline>
        ) : (
          value !== null && <Text>{value}</Text>
        )}
      </Stack>
    </FieldContainer>
  );
};

export const Cell: CellComponent = ({ item, field, linkTo }) => {
  const value = item[field.path] || "";
  return linkTo ? <CellLink {...linkTo}>{value}</CellLink> : <CellContainer>{value}</CellContainer>;
};
