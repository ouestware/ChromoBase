import {
  CardValueComponent,
  CellComponent,
  FieldController,
  FieldControllerConfig,
  FieldProps,
} from "@keystone-6/core/types";
import { Stack } from "@keystone-ui/core";
import { FieldContainer, FieldLabel, TextInput } from "@keystone-ui/fields";
import { useEffect, useState } from "react";

import { HsvColor } from "./HsvColor";

import { CellContainer, CellLink } from "@keystone-6/core/admin-ui/components";

import { HsvaColor } from "@uiw/color-convert";
import ShadeSlider from "@uiw/react-color-shade-slider";
import Wheel from "@uiw/react-color-wheel";
import { omit, pick } from "lodash";
import { ColorValue } from ".";
import { HexInput } from "./RgbInput";
import { roundColorCanalsPrecision } from "./utils";

export const controller = (config: FieldControllerConfig): FieldController<ColorValue, Partial<ColorValue>> => {
  return {
    path: config.path,
    label: config.label,
    graphqlSelection: `${config.path} {
      h
      s
      v
    }`,
    defaultValue: { h: 0, s: 100, v: 50 },
    description: "Color in HSV space",
    deserialize: (data) => {
      const value = data[config.path];
      return typeof value === "object" ? value : null;
    },
    serialize: (value) => ({ [config.path]: value }),
  };
};

export const Field = ({ field, value, onChange }: FieldProps<typeof controller>) => {
  // this pick should not be here. Looks like value's type is errorneous as value contains the GQL __typename field which cause validation error when submitting.
  // we add a:1 here to create a HsvaColor type needed by the react-color component.
  const [color, setColor] = useState<HsvaColor>({ ...pick(value, ["h", "s", "v"]), a: 1 });

  useEffect(() => {
    // removing a:1 to send back a HsvColor
    if (onChange) onChange(omit(color, "a"));
  }, [color, onChange]);

  return (
    <FieldContainer>
      <FieldLabel htmlFor={field.path}>
        <div style={{ display: "flex", alignItems: "center" }}>
          <span style={{ marginRight: "0.5rem" }}>{field.label}:</span>
          <HsvColor color={color} noLabel />
        </div>
      </FieldLabel>
      {onChange && (
        <Stack across gap="medium">
          <Stack gap="small">
            {/* Graphical Hue and Saturation input */}
            <Wheel
              color={color}
              onChange={(newcolor) => {
                setColor({ ...color, ...newcolor.hsva });
              }}
            />
            {/* Graphical Value input */}
            <ShadeSlider
              hsva={color}
              onChange={(newcolor) => {
                setColor(roundColorCanalsPrecision({ ...color, ...newcolor }));
              }}
            />
          </Stack>
          {/* Input for precise color edits */}
          <Stack gap="medium">
            <FieldContainer as="fieldset">
              <FieldLabel>Hue</FieldLabel>
              <TextInput
                type="number"
                value={color.h}
                min={0}
                max={360}
                onChange={(e) => setColor({ ...color, h: +e.target.value })}
              />
              <FieldLabel>Saturation</FieldLabel>
              <TextInput
                type="number"
                value={color.s}
                min={0}
                max={100}
                onChange={(e) => setColor({ ...color, s: +e.target.value })}
              />
              <FieldLabel>Value</FieldLabel>
              <TextInput
                type="number"
                value={color.v}
                min={0}
                max={100}
                onChange={(e) => setColor({ ...color, v: +e.target.value })}
              />
            </FieldContainer>
            <HexInput color={color} onChange={(c) => setColor(roundColorCanalsPrecision(c))} />
          </Stack>
        </Stack>
      )}
    </FieldContainer>
  );
};

export const Cell: CellComponent<typeof controller> = ({ item, field, linkTo }) => {
  const hslColor = <HsvColor color={item[field.path]} />;

  return linkTo ? <CellLink {...linkTo}>{hslColor}</CellLink> : <CellContainer>{hslColor}</CellContainer>;
};
Cell.supportsLinkTo = true;

export const CardValue: CardValueComponent<typeof controller> = ({ item, field }) => {
  return (
    <FieldContainer>
      <FieldLabel>{field.label}</FieldLabel>
      <HsvColor color={item[field.path]} />
    </FieldContainer>
  );
};
