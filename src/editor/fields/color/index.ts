import { graphql } from "@keystone-6/core";
import { BaseListTypeInfo, CommonFieldConfig, FieldTypeFunc, fieldType } from "@keystone-6/core/types";
import { HsvColor } from "@uiw/color-convert";
import { mapValues, pick } from "lodash";

type ColorFieldConfig<ListTypeInfo extends BaseListTypeInfo> = CommonFieldConfig<ListTypeInfo>;

//TODO: to generalize we should allow RGB mode and activate alpha canal
export type ColorValue = HsvColor; //undefinded a to make the type compatible with HsvaColor

type Nullable<T> = { [P in keyof T]: T[P] | null };

const HSVInput = graphql.inputObject({
  name: "ColorHSVInput",
  fields: {
    h: graphql.arg({ type: graphql.nonNull(graphql.Float) }),
    s: graphql.arg({ type: graphql.nonNull(graphql.Float) }),
    v: graphql.arg({ type: graphql.nonNull(graphql.Float) }),
  },
});
const HSVInputPartial = graphql.inputObject({
  name: "ColorHSVInputPartial",
  fields: {
    h: graphql.arg({ type: graphql.Float }),
    s: graphql.arg({ type: graphql.Float }),
    v: graphql.arg({ type: graphql.Float }),
  },
});

const HSVOutput = graphql.object<ColorValue>()({
  name: "ColorHSVOutput",
  fields: {
    h: graphql.field({ type: graphql.nonNull(graphql.Float) }),
    s: graphql.field({ type: graphql.nonNull(graphql.Float) }),
    v: graphql.field({ type: graphql.nonNull(graphql.Float) }),
  },
});

const HSVInputArg = graphql.arg({
  type: HSVInput,
});
const HSVInputPartialArg = graphql.arg({
  type: HSVInputPartial,
});

const ColorHSVFilter = graphql.inputObject({
  name: "ColorHSVFilter",
  fields: {
    equals: HSVInputPartialArg,
    lt: HSVInputPartialArg,
    lte: HSVInputPartialArg,
    gt: HSVInputPartialArg,
    gte: HSVInputPartialArg,
  },
});

export const color =
  <ListTypeInfo extends BaseListTypeInfo>(config: ColorFieldConfig<ListTypeInfo> = {}): FieldTypeFunc<ListTypeInfo> =>
  () => {
    function resolveWhere(
      value: null | {
        equals: Partial<Nullable<ColorValue>> | null | undefined;
        lt: Partial<Nullable<ColorValue>> | null | undefined;
        lte: Partial<Nullable<ColorValue>> | null | undefined;
        gt: Partial<Nullable<ColorValue>> | null | undefined;
        gte: Partial<Nullable<ColorValue>> | null | undefined;
      },
    ) {
      if (value === null) {
        throw new Error("ColorHSVFilter cannot be null");
      }

      const { equals, lt, lte, gt, gte } = value;

      if (equals) {
        return mapValues(equals, (f) => ({ equals: f }));
      }
      if (lt) {
        return mapValues(lt, (f) => ({ lt: f }));
      }
      if (lte) {
        return mapValues(lte, (f) => ({ lte: f }));
      }
      if (gt) {
        return mapValues(gt, (f) => ({ gt: f }));
      }
      if (gte) {
        return mapValues(gte, (f) => ({ gte: f }));
      }

      return {};
    }

    return fieldType({
      kind: "multi",
      fields: {
        h: {
          kind: "scalar",
          mode: "required",
          scalar: "Float",
        },
        s: {
          kind: "scalar",
          mode: "required",
          scalar: "Float",
        },
        v: {
          kind: "scalar",
          mode: "required",
          scalar: "Float",
        },
      },
    })({
      ...config,
      // TODO: built-in validation of h s and v min max
      input: {
        where: {
          arg: graphql.arg({ type: ColorHSVFilter }),
          resolve(value) {
            return resolveWhere(value);
          },
        },
        create: {
          arg: HSVInputArg,
          resolve(value) {
            // TODO: default value should be here
            if (!value) return { h: 0, s: 100, v: 50 };
            return value;
          },
        },
        update: {
          arg: HSVInputArg,
          resolve(value) {
            // TODO: default value should be here

            if (!value) return { h: 0, s: 100, v: 50 };
            return pick(value, ["h", "s", "v"]);
          },
        },
        // orderBy: {
        //   arg: graphql.arg({ type: orderDirectionEnum }),
        //   resolve: (value) => ({ h: value, s: value, v: value }),
        // },
      },
      output: graphql.field({
        type: HSVOutput,
        resolve({ value }) {
          return pick(value, ["h", "s", "v"]);
        },
      }),

      views: "@chromobase/editor/fields/color/views",
      getAdminMeta() {
        return {};
      },
    });
  };
