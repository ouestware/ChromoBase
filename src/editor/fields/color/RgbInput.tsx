// import { Button } from "@keystone-ui/button";
import { FieldContainer, FieldLabel, TextInput } from "@keystone-ui/fields";
import { HsvaColor, equalHex, hexToHsva, hsvaToHex } from "@uiw/color-convert";
import { FC, useEffect, useState } from "react";
import { validHexNoAlpha } from "./utils";

export const HexInput: FC<{ color: HsvaColor; onChange: (color: HsvaColor) => void }> = ({ color, onChange }) => {
  const [hex, setHex] = useState<string>(hsvaToHex(color));
  const [debouncedHex, setDebouncedHex] = useState<NodeJS.Timeout | null>(null);

  // clear timeout on unmount
  useEffect(() => {
    return () => {
      if (debouncedHex) clearTimeout(debouncedHex);
    };
  }, [debouncedHex]);

  useEffect(() => {
    // don't change if hex value is actually the same #EEE === #EEEEEE
    if (!equalHex(hex, hsvaToHex(color))) setHex(hsvaToHex(color));
  }, [color]);

  return (
    <FieldContainer as="fieldset">
      <FieldLabel>RGB</FieldLabel>
      <TextInput
        type="text"
        value={hex}
        invalid={!validHexNoAlpha(hex)}
        onChange={(e) => {
          const newHex = e.target.value;

          // debounce change before calling the callback
          if (debouncedHex) clearTimeout(debouncedHex);
          setDebouncedHex(
            setTimeout(() => {
              if (validHexNoAlpha(newHex)) {
                // don't change if hex value is actually the same #EEE === #EEEEEE
                if (!equalHex(newHex, hsvaToHex(color))) {
                  const newColor = hexToHsva(newHex);
                  onChange(newColor);
                }
              }
            }, 500),
          );
        }}
      />
    </FieldContainer>
  );
};
