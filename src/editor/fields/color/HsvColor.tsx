import { Stack } from "@keystone-ui/core";
import { HsvColor as HsvColorType, HsvaColor, hsvaToHex } from "@uiw/color-convert";
import { CSSProperties, FC } from "react";

export const hsvToText = (color: HsvColorType) => `h:${color.h}° s:${color.s}% v:${color.v}%`;

export const HsvColor: FC<{ color: HsvaColor; noLabel?: boolean; style?: CSSProperties }> = ({
  color,
  noLabel,
  style,
}) => {
  const hsvValue = hsvToText(color);
  const hexValue = hsvaToHex(color);

  const colorSquare = <div style={{ width: "20px", height: "20px", backgroundColor: hexValue }} />;

  if (noLabel) return colorSquare;
  else
    return (
      <Stack across gap="medium" align="center" style={style}>
        {colorSquare}
        {!noLabel && (
          <Stack>
            <span>{hsvValue}</span>
            <span>{hexValue}</span>
          </Stack>
        )}
      </Stack>
    );
};
