import { HsvaColor } from "@uiw/color-convert";
import { mapValues } from "lodash";

export const validHexNoAlpha = (hex: string) => /^#?([A-Fa-f0-9]{3}|[A-Fa-f0-9]{6})$/.test(hex);
export const roundColorCanalsPrecision = (color: HsvaColor) => mapValues(color, (v) => Math.round(+v));
