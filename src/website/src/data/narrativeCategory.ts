import { gql } from "../__generated__/gql";
import { getClient } from "./gqlClient";

export const NARRATIVE_CATEGORY = gql(`
  query NarrativeCategory($slug: String!, $narrativesWhere: NarrativeWhereInput) {
    narrativeCategory(where: {slug:$slug}) {
      description {
        document
      }
      title
      narratives(where: $narrativesWhere) {
        ...NarrativeComplete
      }
    }
  }
`);

export const getNarrativeCategory = async (slug: string) => {
  const result = (await getClient()).query({
    query: NARRATIVE_CATEGORY,
    variables: {
      slug,
      narrativesWhere:
        import.meta.env.PUBLIC_BUILD_TARGET === "static"
          ? { status: { equals: "public" } }
          : undefined,
    },
    fetchPolicy: "no-cache",
  });
  const data = (await result).data;

  return data.narrativeCategory;
};
