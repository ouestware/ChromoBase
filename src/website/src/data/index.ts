import {
  EventCompleteFragment,
  GlossaryCompleteFragment,
  NarrativeCompleteFragment,
  ObjectCompleteFragment,
  OrganisationCompleteFragment,
  PersonCompleteFragment,
  ReferenceCompleteFragment,
} from "../__generated__/graphql";
import { getEvent, getEvents } from "./event";
import {
  getGlossaries,
  getGlossary,
  getTechnique,
  getTechniques,
} from "./glossary";
import { getNarrative, getNarratives } from "./narrative";
import { getObject, getObjects } from "./object";
import { getOrganisation, getOrganisations } from "./organisation";
import { getPeople, getPerson } from "./person";
import { getReference, getReferences } from "./reference";

type DataModel =
  | "Narrative"
  | "Person"
  | "Object"
  | "Glossary"
  | "Technique"
  | "Organisation"
  | "Event"
  | "Reference";
type ItemsTypes =
  | NarrativeCompleteFragment[]
  | PersonCompleteFragment[]
  | ObjectCompleteFragment[]
  | GlossaryCompleteFragment[]
  | OrganisationCompleteFragment[]
  | EventCompleteFragment[]
  | ReferenceCompleteFragment[];
type ItemType =
  | NarrativeCompleteFragment
  | PersonCompleteFragment
  | ObjectCompleteFragment
  | GlossaryCompleteFragment
  | OrganisationCompleteFragment
  | EventCompleteFragment
  | ReferenceCompleteFragment;

const dataGetter: Record<
  DataModel,
  {
    getAll: () => Promise<ItemsTypes>;
    getOne: (where: { slug?: string }) => Promise<ItemType>;
  }
> = {
  Narrative: {
    getAll: getNarratives,
    getOne: getNarrative,
  },
  Person: {
    getAll: getPeople,
    getOne: getPerson,
  },
  Object: {
    getAll: getObjects,
    getOne: getObject,
  },
  Glossary: {
    getAll: getGlossaries,
    getOne: getGlossary,
  },
  Technique: {
    getAll: getTechniques,
    getOne: getTechnique,
  },
  Organisation: {
    getAll: getOrganisations,
    getOne: getOrganisation,
  },
  Event: {
    getAll: getEvents,
    getOne: getEvent,
  },
  Reference: {
    getAll: getReferences,
    getOne: getReference,
  },
};

export function getStaticPathsFactory<T extends ItemType>(
  model: DataModel,
): () => Promise<
  { params: { slug?: string | null | undefined }; props: { item: T } }[]
> {
  return async () => {
    const items = await dataGetter[model].getAll();
    if (items)
      return items.map((item) => ({
        params: { slug: item.slug },
        props: { item: item as T },
      }));
    else return [];
  };
}

export async function getItem<T extends ItemType>(
  staticProp: T,
  model: DataModel,
  where: { slug?: string | null },
) {
  if (import.meta.env.SSR) {
    //SSR mode

    const item = await dataGetter[model].getOne({
      slug: where.slug || undefined,
    });
    if (item) return item as T;
  }
  // Static mode
  else return staticProp;
}
