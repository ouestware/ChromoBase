import { gql } from "../__generated__/gql";
import { ColourCompleteFragment } from "../__generated__/graphql";
import { getClient } from "./gqlClient";

import { whereNarrativesExist } from "./narrative";

export const colourFragment = gql(`fragment ColourComplete on Colour {

    label
    name
    color {
      h
      s
      v
    }
    narratives {
      id
      slug
      title
      category {
        slug
        title
      }
      date
      authors {
        id
        name
        presentation

      }
      cover {
        id
        title
        image {
          id
          url
          width
          height
          extension
          filesize
        }
        type
        url
        embed
      }
    }
  }
`);

export const COLOURS = gql(`
  query colours($where: ColourWhereInput) {
    colours(where:$where, orderBy: [{name: asc}]) {
      ...ColourComplete
    }
  }
`);

export const getNamedColours = async (): Promise<ColourCompleteFragment[]> => {
  const result = (await getClient()).query({
    query: COLOURS,
    variables: {
      where: { ...whereNarrativesExist, name: { not: { equals: "" } } },
    },
    fetchPolicy: "no-cache",
  });
  const data = (await result).data;

  return data ? (data.colours as ColourCompleteFragment[]) : [];
};
