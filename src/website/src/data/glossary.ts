import { gql } from "../__generated__/gql";
import {
  GlossaryCompleteFragment,
  GlossaryWhereInput,
  QueryGlossaryArgs,
} from "../__generated__/graphql";
import { getClient } from "./gqlClient";
import { whereNarrativesExist } from "./narrative";

export const glossaryFragment = gql(`fragment GlossaryComplete on Glossary {
    slug
    label
    description {
      document
    }
    type {
      label
      category
    }
    chemicalFormula
    dateOfDiscoveryInvention
    medias {
      id
        title
        image {
          id
          url
          width
          height
          extension
          filesize
        }
        type
        url
        embed
    }
    narratives {
      id
      slug
      title
      category {
        slug
        title
      }
      date
      authors {
        id
        name
        presentation

      }
      cover {
        id
        title
        image {
          id
          url
          width
          height
          extension
          filesize
        }
        type
        url
        embed
      }
    }
  }
`);

export const GLOSSARY = gql(`
  query glossary($where: GlossaryWhereUniqueInput!) {
    glossary(where: $where) {
      ...GlossaryComplete
    }
  }
`);

export const GLOSSARIES = gql(`
  query glossaries($where: GlossaryWhereInput) {
    glossaries(where: $where) {
      ...GlossaryComplete
    }
  }
`);

const _getGlossaries = async (
  type: GlossaryWhereInput["type"],
): Promise<GlossaryCompleteFragment[]> => {
  const result = (await getClient()).query({
    query: GLOSSARIES,
    variables: {
      where: {
        AND: [whereNarrativesExist, { type }],
      },
    },
    fetchPolicy: "no-cache",
  });
  const data = (await result).data;

  return data ? (data.glossaries as GlossaryCompleteFragment[]) : [];
};

const _getGlossary = async (
  where: QueryGlossaryArgs["where"],
): Promise<GlossaryCompleteFragment> => {
  const result = (await getClient()).query({
    query: GLOSSARY,
    variables: { where },
    fetchPolicy: "no-cache",
  });
  const data = (await result).data;

  return data.glossary as GlossaryCompleteFragment;
};

export const getGlossary = _getGlossary;

export const getTechnique = _getGlossary;

export const getGlossaries = () =>
  _getGlossaries({ category: { equals: "glossary" } });
export const getTechniques = () =>
  _getGlossaries({ category: { equals: "technique" } });
