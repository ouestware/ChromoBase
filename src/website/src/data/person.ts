import { gql } from "../__generated__/gql";
import {
  PersonCompleteFragment,
  QueryPersonArgs,
} from "../__generated__/graphql";
import { getClient } from "./gqlClient";
import { whereNarrativesExist } from "./narrative";

export const personFragment = gql(`fragment PersonComplete on Person {
    slug
    label
    firstName
    lastName
    fullName
    biography {
      document
    }
    dateOfBirth
    dateOfDeath
    occupations {
      label
    }
    narratives {
      id
      slug
      title
      category {
        slug
        title
      }
      date
      authors {
        id
        name
        presentation

      }
      cover {
        id
        title
        image {
          id
          url
          width
          height
          extension
          filesize
        }
        type
        url
        embed
      }
    }
  }
`);

export const PERSON = gql(`

  query person($where: PersonWhereUniqueInput!) {
    person(where: $where) {
      ...PersonComplete
    }
  }
`);

export const PEOPLE = gql(`

  query people($where: PersonWhereInput) {
    people(where:$where) {
      ...PersonComplete
    }
  }
`);

export const getPeople = async (): Promise<PersonCompleteFragment[]> => {
  const result = (await getClient()).query({
    query: PEOPLE,
    variables: { where: whereNarrativesExist },
    fetchPolicy: "no-cache",
  });
  const data = (await result).data;

  return data ? (data.people as PersonCompleteFragment[]) : [];
};

export const getPerson = async (
  where: QueryPersonArgs["where"],
): Promise<PersonCompleteFragment> => {
  const result = (await getClient()).query({
    query: PERSON,
    variables: { where },
    fetchPolicy: "no-cache",
  });
  const data = (await result).data;

  return data.person as PersonCompleteFragment;
};
