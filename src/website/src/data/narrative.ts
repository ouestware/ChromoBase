import { gql } from "../__generated__/gql";
import {
  NarrativeCompleteFragment,
  QueryNarrativeArgs,
} from "../__generated__/graphql";
import { getClient } from "./gqlClient";

export const NarrativeFragment = gql(`fragment NarrativeComplete on Narrative {
  id
  slug
  title
  date
  language
  cover {
    id
    title
    image {
      id
      url
      width
      height
      extension
      filesize
    }
    type
    url
    embed
  }
  content {
    document(hydrateRelationships: true)
  }
  references { 
    slug
    label
    citation {
      document
    }
  }
  extraReferences { 
    slug
    label
    citation {
      document
    }
  }
  authors {
    id
    name
    presentation
    personalPageURL
  }
  category {
    slug
   title
  }
}
`);

export const NARRATIVE = gql(`
  query narrative($where: NarrativeWhereUniqueInput!) {
    narrative(where: $where) {
      ...NarrativeComplete
    }
  }
`);

export const NARRATIVES = gql(`
  query narratives {
    narratives {
      ...NarrativeComplete
    }
  }
`);

const PUBLIC_NARRATIVES = gql(`
  query public_narratives {
    narratives(where: {status: {equals: "public"}}) {
      ...NarrativeComplete
    }
  }
`);

export const whereNarrativesExist =
  import.meta.env.PUBLIC_BUILD_TARGET === "static"
    ? { narratives: { some: { status: { equals: "public" } } } }
    : { narratives: { some: {} } };

export const getNarratives = async (): Promise<NarrativeCompleteFragment[]> => {
  const result = (await getClient()).query({
    query:
      import.meta.env.PUBLIC_BUILD_TARGET === "static"
        ? PUBLIC_NARRATIVES
        : NARRATIVES,
    fetchPolicy: "no-cache",
  });
  const data = (await result).data;

  return data ? (data.narratives as NarrativeCompleteFragment[]) : [];
};

export const getNarrative = async (
  where: QueryNarrativeArgs["where"],
): Promise<NarrativeCompleteFragment> => {
  const result = (await getClient()).query({
    query: NARRATIVE,
    variables: { where },
    fetchPolicy: "no-cache",
  });
  const data = (await result).data;

  return data.narrative as NarrativeCompleteFragment;
};
