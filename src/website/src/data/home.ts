import { pick } from "lodash";

import { gql } from "../__generated__/gql";
import {
  NarrativeCompleteFragment,
  NarrativeWhereInput,
  ObjectCompleteFragment,
  ObjectWhereInput,
} from "../__generated__/graphql";
import { getClient } from "./gqlClient";

export const HOME = gql(`
  query NarrativesHome($whereNarrative: NarrativeWhereInput, $whereObject: ObjectWhereInput) {
      narratives(where: $whereNarrative, orderBy: [{createdAt:	desc}, {updatedAt: desc}] ) {
        ...NarrativeComplete
      }
      objects(where: $whereObject){
        ...ObjectComplete
      }
      staticContent {
        baseline
      }
      
      }
`);

export const getHomeData = async () => {
  const whereNarrative: NarrativeWhereInput =
    import.meta.env.PUBLIC_BUILD_TARGET === "static"
      ? {
          AND: [
            { selectForHome: { equals: true } },
            { status: { equals: "public" } },
          ],
        }
      : { selectForHome: { equals: true } };
  const whereObject: ObjectWhereInput =
    import.meta.env.PUBLIC_BUILD_TARGET === "static"
      ? { narratives: { some: { status: { equals: "public" } } } }
      : { narratives: { some: {} } };

  const result = (await getClient()).query({
    query: HOME,
    variables: { whereNarrative, whereObject },
    fetchPolicy: "no-cache",
  });
  const data = (await result).data;

  return {
    ...pick(data, ["narratives", "objects"]),
    baseline: data.staticContent?.baseline,
  } as {
    narratives: NarrativeCompleteFragment[];
    objects: ObjectCompleteFragment[];
    baseline?: string;
  };
};
