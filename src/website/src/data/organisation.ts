import { gql } from "../__generated__/gql";
import {
  OrganisationCompleteFragment,
  QueryOrganisationArgs,
} from "../__generated__/graphql";
import { getClient } from "./gqlClient";
import { whereNarrativesExist } from "./narrative";

export const organisationFragment =
  gql(`fragment OrganisationComplete on Organisation {
    slug
    label
    dateOfInception
    type {
      label
    }
    description {
      document
    }
    narratives {
      id
      slug
      title
      category {
        slug
        title
      }
      date
      authors {
        id
        name
        presentation

      }
      cover {
        id
        title
        image {
          id
          url
          width
          height
          extension
          filesize
        }
        type
        url
        embed
      }
    }
  }
`);

export const ORGANISATION = gql(`
  query organisation($where: OrganisationWhereUniqueInput!) {
    organisation(where: $where) {
      ...OrganisationComplete
    }
  }
`);

export const ORGANISATIONS = gql(`
  query organisations($where: OrganisationWhereInput) {
    organisations(where: $where) {
      ...OrganisationComplete
    }
  }
`);

export const getOrganisations = async (): Promise<
  OrganisationCompleteFragment[]
> => {
  const result = (await getClient()).query({
    query: ORGANISATIONS,
    variables: { where: whereNarrativesExist },
    fetchPolicy: "no-cache",
  });
  const data = (await result).data;

  return data ? (data.organisations as OrganisationCompleteFragment[]) : [];
};

export const getOrganisation = async (
  where: QueryOrganisationArgs["where"],
): Promise<OrganisationCompleteFragment> => {
  const result = (await getClient()).query({
    query: ORGANISATION,
    variables: { where },
    fetchPolicy: "no-cache",
  });
  const data = (await result).data;

  return data.organisation as OrganisationCompleteFragment;
};
