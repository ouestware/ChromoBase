import { gql } from "../__generated__";
import { StaticContentQuery } from "../__generated__/graphql";
import { getClient } from "./gqlClient";

const staticContentQuery = gql(`query StaticContent {
  staticContent {
    id
    baseline
    colourWheelIntroduction {
      document
    }
    about {
      document
    }
    credits {
      document
    }
    partnersLogos {
      image {
        id
          url
          width
          height
          extension
          filesize
      }
      institutionName
      url
    }
    funding {
      document
    }
    fundingLogos {
      image {

      id
          url
          width
          height
          extension
          filesize
      }
      institutionName
      url
    }
    legalNotice {
     document 
    }
  }
}`);

export const getStaticContent = async (): Promise<
  StaticContentQuery["staticContent"]
> => {
  const result = (await getClient()).query({
    query: staticContentQuery,
    fetchPolicy: "no-cache",
  });
  const data = (await result).data;

  return data.staticContent;
};
