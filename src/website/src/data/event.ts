import { gql } from "../__generated__/gql";
import {
  EventCompleteFragment,
  QueryEventArgs,
} from "../__generated__/graphql";
import { getClient } from "./gqlClient";
import { whereNarrativesExist } from "./narrative";

export const eventFragment = gql(`fragment EventComplete on Event {
    slug
    label
    type {
      label
    }
    startDate
    endDate
    place
    description {
      document
    }
    narratives {
      id
      slug
      title
      category {
        slug
        title
      }
      date
      authors {
        id
        name
        presentation

      }
      cover {
        id
        title
        image {
          id
          url
          width
          height
          extension
          filesize
        }
        type
        url
        embed

      }
    }
  }
`);

export const EVENT = gql(`
  query event($where: EventWhereUniqueInput!) {
    event(where: $where) {
      ...EventComplete
    }
  }
`);

export const EVENTS = gql(`
  query events($where: EventWhereInput) {
    events(where: $where) {
      ...EventComplete
    }
  }
`);

export const getEvents = async (): Promise<EventCompleteFragment[]> => {
  const result = (await getClient()).query({
    query: EVENTS,
    variables: { where: whereNarrativesExist },
    fetchPolicy: "no-cache",
  });
  const data = (await result).data;

  return data ? (data.events as EventCompleteFragment[]) : [];
};

export const getEvent = async (
  where: QueryEventArgs["where"],
): Promise<EventCompleteFragment> => {
  const result = (await getClient()).query({
    query: EVENT,
    variables: { where },
    fetchPolicy: "no-cache",
  });
  const data = (await result).data;

  return data.event as EventCompleteFragment;
};
