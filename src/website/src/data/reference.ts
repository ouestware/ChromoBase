import { gql } from "../__generated__/gql";
import {
  QueryReferenceArgs,
  ReferenceCompleteFragment,
} from "../__generated__/graphql";
import { getClient } from "./gqlClient";

export const ReferenceFragment = gql(`fragment ReferenceComplete on Reference {
    label
    slug
    note
    citation {
      document
    }
    narratives {
      ...NarrativeComplete
    }

    medias {
      id
        title
        order
        image {
          id
          url
          width
          height
          extension
          filesize
        }
        type
        url
        embed
    }
}
`);

export const REFERENCE = gql(`
  query reference($where: ReferenceWhereUniqueInput!) {
    reference(where: $where) {
      ...ReferenceComplete
    }
  }
`);

export const REFERENCES = gql(`
  query references($where: ReferenceWhereInput) {
    references(where: $where, orderBy:[{label: asc}]) {
      ...ReferenceComplete
    }
  }
`);

export const getReference = async (
  where: QueryReferenceArgs["where"],
): Promise<ReferenceCompleteFragment> => {
  const result = (await getClient()).query({
    query: REFERENCE,
    variables: { where },
    fetchPolicy: "no-cache",
  });
  const data = (await result).data;

  return data.reference as ReferenceCompleteFragment;
};

export const getReferences = async (): Promise<ReferenceCompleteFragment[]> => {
  const result = (await getClient()).query({
    query: REFERENCES,
    // TODO: subject to change if we wan tto see Object's references in bibliographye
    variables: {
      where:
        import.meta.env.PUBLIC_BUILD_TARGET === "static"
          ? {
              OR: [
                { narratives: { some: { status: { equals: "public" } } } },
                { narrativesExtra: { some: { status: { equals: "public" } } } },
              ],
            }
          : {
              OR: [
                { narratives: { some: {} } },
                { narrativesExtra: { some: {} } },
              ],
            },
    },
    fetchPolicy: "no-cache",
  });
  const data = (await result).data;

  return data ? (data.references as ReferenceCompleteFragment[]) : [];
};
