import { gql } from "../__generated__/gql";
import {
  ObjectCompleteFragment,
  ObjectWhereUniqueInput,
} from "../__generated__/graphql";
import { getClient } from "./gqlClient";
import { whereNarrativesExist } from "./narrative";

export const objectComplete = gql(`fragment ObjectComplete on Object {
    id
    slug
    label
    author
    location
    medium
    dimensions
    description {
      document
    }
    digitalizedByChromotope
    type {
      label
    }
    date
    cover {
      id
        title
        image {
          id
          url
          width
          height
          extension
          filesize
        }
        type
        url
        embed
    }
    medias {
      id
        title
        order
        image {
          id
          url
          width
          height
          extension
          filesize
        }
        type
        url
        embed
    }
    mediasCount
    reference {
      id
      citation {
        document
      }
    }
    colours {
      id
      label
      color {
        h
        s
        v
      }
    }
    narratives {
      ...NarrativeComplete
    }
  }
`);

export const OBJECT = gql(/* GraphQL */ `
  query Object($where: ObjectWhereUniqueInput!) {
    object(where: $where) {
      ...ObjectComplete
    }
  }
`);

export const getObject = async (
  where: ObjectWhereUniqueInput,
): Promise<ObjectCompleteFragment> => {
  const result = (await getClient()).query({
    query: OBJECT,
    variables: { where },
    fetchPolicy: "no-cache",
  });
  const data = (await result).data;

  return data.object as ObjectCompleteFragment;
};

export const OBJECTS = gql(/* GraphQL */ `
  query Objects($where: ObjectWhereInput) {
    objects(where: $where) {
      ...ObjectComplete
    }
  }
`);

export const getObjects = async (): Promise<ObjectCompleteFragment[]> => {
  const client = await getClient();

  const result = await client.query({
    query: OBJECTS,
    variables: { where: whereNarrativesExist },
    fetchPolicy: "no-cache",
  });

  const data = result ? result.data : undefined;

  return data ? (data.objects as ObjectCompleteFragment[]) : [];
};
