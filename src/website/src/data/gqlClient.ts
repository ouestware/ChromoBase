import { ApolloClient, InMemoryCache, NormalizedCacheObject } from "@apollo/client";
import { gql } from "../__generated__";

const AUTH_MUTATION = gql(`mutation AuthenticateUserWithPassword( $password: String!, $name: String!) {
  authenticateUserWithPassword( password: $password, name: $name) {
    ... on UserAuthenticationWithPasswordSuccess {
      sessionToken
    }
  }
}`);

const AUTH_STATE = gql(`query AuthenticatedItem {
  authenticatedItem {
    ... on User {
      name
      
    }
  }
}`);

let client: ApolloClient<NormalizedCacheObject> | null = null;

const authenticate = async () => {
  const authClient = new ApolloClient({
    uri: `${import.meta.env.EDITOR_PUBLIC_URL}/api/graphql`,
    cache: new InMemoryCache(),
  });
  // auth
  const r = await authClient.mutate({
    mutation: AUTH_MUTATION,
    variables: {
      password: import.meta.env.GQL_PASSWORD,
      name: import.meta.env.GQL_USER,
    },
  });
  if (r && r.data && r.data.authenticateUserWithPassword && "sessionToken" in r.data.authenticateUserWithPassword) {
    const headers = { Cookie: `keystonejs-session=${r.data?.authenticateUserWithPassword.sessionToken}` };
    console.debug("new authentication to GQL succeeded");
    return new ApolloClient({
      uri: `${import.meta.env.EDITOR_PUBLIC_URL}/api/graphql`,
      cache: new InMemoryCache(),
      headers,
    });
  } else {
    //TODO: handle auth rejection
    console.error(r.errors);
    throw new Error(`Couldn't auth to GQL ${r.errors}`);
  }
};

const getClient = async () => {
  if (client === null) {
    client = await authenticate();
  } else {
    //check session status
    const r = await client.query({
      query: AUTH_STATE,
      fetchPolicy: "no-cache",
    });

    if (!(r.data && r.data?.authenticatedItem?.name) || r.data.authenticatedItem.name !== import.meta.env.GQL_USER) {
      console.debug("session expired");
      // expired token, renew
      client = await authenticate();
    }
  }

  return client;
};

export { getClient };
