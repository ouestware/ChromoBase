import { slugify } from "@chromobase/editor/schema/utils";
import { Colour } from "../../__generated__/graphql";

export const colourId = (colour: Pick<Colour, "name" | "color">): string => {
  return `${slugify(colour.name || "")}-${colour.color?.h}-${colour.color
    ?.s}-${colour.color?.v}`;
};
