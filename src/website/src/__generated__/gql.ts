/* eslint-disable */
import * as types from './graphql';
import { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';

/**
 * Map of all GraphQL operations in the project.
 *
 * This map has several performance disadvantages:
 * 1. It is not tree-shakeable, so it will include all operations in the project.
 * 2. It is not minifiable, so the string of a GraphQL query will be multiple times inside the bundle.
 * 3. It does not support dead code elimination, so it will add unused operations.
 *
 * Therefore it is highly recommended to use the babel or swc plugin for production.
 */
const documents = {
    "fragment ColourComplete on Colour {\n\n    label\n    name\n    color {\n      h\n      s\n      v\n    }\n    narratives {\n      id\n      slug\n      title\n      category {\n        slug\n        title\n      }\n      date\n      authors {\n        id\n        name\n        presentation\n\n      }\n      cover {\n        id\n        title\n        image {\n          id\n          url\n          width\n          height\n          extension\n          filesize\n        }\n        type\n        url\n        embed\n      }\n    }\n  }\n": types.ColourCompleteFragmentDoc,
    "\n  query colours($where: ColourWhereInput) {\n    colours(where:$where, orderBy: [{name: asc}]) {\n      ...ColourComplete\n    }\n  }\n": types.ColoursDocument,
    "fragment EventComplete on Event {\n    slug\n    label\n    type {\n      label\n    }\n    startDate\n    endDate\n    place\n    description {\n      document\n    }\n    narratives {\n      id\n      slug\n      title\n      category {\n        slug\n        title\n      }\n      date\n      authors {\n        id\n        name\n        presentation\n\n      }\n      cover {\n        id\n        title\n        image {\n          id\n          url\n          width\n          height\n          extension\n          filesize\n        }\n        type\n        url\n        embed\n\n      }\n    }\n  }\n": types.EventCompleteFragmentDoc,
    "\n  query event($where: EventWhereUniqueInput!) {\n    event(where: $where) {\n      ...EventComplete\n    }\n  }\n": types.EventDocument,
    "\n  query events($where: EventWhereInput) {\n    events(where: $where) {\n      ...EventComplete\n    }\n  }\n": types.EventsDocument,
    "fragment GlossaryComplete on Glossary {\n    slug\n    label\n    description {\n      document\n    }\n    type {\n      label\n      category\n    }\n    chemicalFormula\n    dateOfDiscoveryInvention\n    medias {\n      id\n        title\n        image {\n          id\n          url\n          width\n          height\n          extension\n          filesize\n        }\n        type\n        url\n        embed\n    }\n    narratives {\n      id\n      slug\n      title\n      category {\n        slug\n        title\n      }\n      date\n      authors {\n        id\n        name\n        presentation\n\n      }\n      cover {\n        id\n        title\n        image {\n          id\n          url\n          width\n          height\n          extension\n          filesize\n        }\n        type\n        url\n        embed\n      }\n    }\n  }\n": types.GlossaryCompleteFragmentDoc,
    "\n  query glossary($where: GlossaryWhereUniqueInput!) {\n    glossary(where: $where) {\n      ...GlossaryComplete\n    }\n  }\n": types.GlossaryDocument,
    "\n  query glossaries($where: GlossaryWhereInput) {\n    glossaries(where: $where) {\n      ...GlossaryComplete\n    }\n  }\n": types.GlossariesDocument,
    "mutation AuthenticateUserWithPassword( $password: String!, $name: String!) {\n  authenticateUserWithPassword( password: $password, name: $name) {\n    ... on UserAuthenticationWithPasswordSuccess {\n      sessionToken\n    }\n  }\n}": types.AuthenticateUserWithPasswordDocument,
    "query AuthenticatedItem {\n  authenticatedItem {\n    ... on User {\n      name\n      \n    }\n  }\n}": types.AuthenticatedItemDocument,
    "\n  query NarrativesHome($whereNarrative: NarrativeWhereInput, $whereObject: ObjectWhereInput) {\n      narratives(where: $whereNarrative, orderBy: [{createdAt:\tdesc}, {updatedAt: desc}] ) {\n        ...NarrativeComplete\n      }\n      objects(where: $whereObject){\n        ...ObjectComplete\n      }\n      staticContent {\n        baseline\n      }\n      \n      }\n": types.NarrativesHomeDocument,
    "fragment NarrativeComplete on Narrative {\n  id\n  slug\n  title\n  date\n  language\n  cover {\n    id\n    title\n    image {\n      id\n      url\n      width\n      height\n      extension\n      filesize\n    }\n    type\n    url\n    embed\n  }\n  content {\n    document(hydrateRelationships: true)\n  }\n  references { \n    slug\n    label\n    citation {\n      document\n    }\n  }\n  extraReferences { \n    slug\n    label\n    citation {\n      document\n    }\n  }\n  authors {\n    id\n    name\n    presentation\n    personalPageURL\n  }\n  category {\n    slug\n   title\n  }\n}\n": types.NarrativeCompleteFragmentDoc,
    "\n  query narrative($where: NarrativeWhereUniqueInput!) {\n    narrative(where: $where) {\n      ...NarrativeComplete\n    }\n  }\n": types.NarrativeDocument,
    "\n  query narratives {\n    narratives {\n      ...NarrativeComplete\n    }\n  }\n": types.NarrativesDocument,
    "\n  query public_narratives {\n    narratives(where: {status: {equals: \"public\"}}) {\n      ...NarrativeComplete\n    }\n  }\n": types.Public_NarrativesDocument,
    "\n  query NarrativeCategory($slug: String!, $narrativesWhere: NarrativeWhereInput) {\n    narrativeCategory(where: {slug:$slug}) {\n      description {\n        document\n      }\n      title\n      narratives(where: $narrativesWhere) {\n        ...NarrativeComplete\n      }\n    }\n  }\n": types.NarrativeCategoryDocument,
    "fragment ObjectComplete on Object {\n    id\n    slug\n    label\n    author\n    location\n    medium\n    dimensions\n    description {\n      document\n    }\n    digitalizedByChromotope\n    type {\n      label\n    }\n    date\n    cover {\n      id\n        title\n        image {\n          id\n          url\n          width\n          height\n          extension\n          filesize\n        }\n        type\n        url\n        embed\n    }\n    medias {\n      id\n        title\n        order\n        image {\n          id\n          url\n          width\n          height\n          extension\n          filesize\n        }\n        type\n        url\n        embed\n    }\n    mediasCount\n    reference {\n      id\n      citation {\n        document\n      }\n    }\n    colours {\n      id\n      label\n      color {\n        h\n        s\n        v\n      }\n    }\n    narratives {\n      ...NarrativeComplete\n    }\n  }\n": types.ObjectCompleteFragmentDoc,
    "\n  query Object($where: ObjectWhereUniqueInput!) {\n    object(where: $where) {\n      ...ObjectComplete\n    }\n  }\n": types.ObjectDocument,
    "\n  query Objects($where: ObjectWhereInput) {\n    objects(where: $where) {\n      ...ObjectComplete\n    }\n  }\n": types.ObjectsDocument,
    "fragment OrganisationComplete on Organisation {\n    slug\n    label\n    dateOfInception\n    type {\n      label\n    }\n    description {\n      document\n    }\n    narratives {\n      id\n      slug\n      title\n      category {\n        slug\n        title\n      }\n      date\n      authors {\n        id\n        name\n        presentation\n\n      }\n      cover {\n        id\n        title\n        image {\n          id\n          url\n          width\n          height\n          extension\n          filesize\n        }\n        type\n        url\n        embed\n      }\n    }\n  }\n": types.OrganisationCompleteFragmentDoc,
    "\n  query organisation($where: OrganisationWhereUniqueInput!) {\n    organisation(where: $where) {\n      ...OrganisationComplete\n    }\n  }\n": types.OrganisationDocument,
    "\n  query organisations($where: OrganisationWhereInput) {\n    organisations(where: $where) {\n      ...OrganisationComplete\n    }\n  }\n": types.OrganisationsDocument,
    "fragment PersonComplete on Person {\n    slug\n    label\n    firstName\n    lastName\n    fullName\n    biography {\n      document\n    }\n    dateOfBirth\n    dateOfDeath\n    occupations {\n      label\n    }\n    narratives {\n      id\n      slug\n      title\n      category {\n        slug\n        title\n      }\n      date\n      authors {\n        id\n        name\n        presentation\n\n      }\n      cover {\n        id\n        title\n        image {\n          id\n          url\n          width\n          height\n          extension\n          filesize\n        }\n        type\n        url\n        embed\n      }\n    }\n  }\n": types.PersonCompleteFragmentDoc,
    "\n\n  query person($where: PersonWhereUniqueInput!) {\n    person(where: $where) {\n      ...PersonComplete\n    }\n  }\n": types.PersonDocument,
    "\n\n  query people($where: PersonWhereInput) {\n    people(where:$where) {\n      ...PersonComplete\n    }\n  }\n": types.PeopleDocument,
    "fragment ReferenceComplete on Reference {\n    label\n    slug\n    note\n    citation {\n      document\n    }\n    narratives {\n      ...NarrativeComplete\n    }\n\n    medias {\n      id\n        title\n        order\n        image {\n          id\n          url\n          width\n          height\n          extension\n          filesize\n        }\n        type\n        url\n        embed\n    }\n}\n": types.ReferenceCompleteFragmentDoc,
    "\n  query reference($where: ReferenceWhereUniqueInput!) {\n    reference(where: $where) {\n      ...ReferenceComplete\n    }\n  }\n": types.ReferenceDocument,
    "\n  query references($where: ReferenceWhereInput) {\n    references(where: $where, orderBy:[{label: asc}]) {\n      ...ReferenceComplete\n    }\n  }\n": types.ReferencesDocument,
    "query StaticContent {\n  staticContent {\n    id\n    baseline\n    colourWheelIntroduction {\n      document\n    }\n    about {\n      document\n    }\n    credits {\n      document\n    }\n    partnersLogos {\n      image {\n        id\n          url\n          width\n          height\n          extension\n          filesize\n      }\n      institutionName\n      url\n    }\n    funding {\n      document\n    }\n    fundingLogos {\n      image {\n\n      id\n          url\n          width\n          height\n          extension\n          filesize\n      }\n      institutionName\n      url\n    }\n    legalNotice {\n     document \n    }\n  }\n}": types.StaticContentDocument,
};

/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 *
 *
 * @example
 * ```ts
 * const query = gql(`query GetUser($id: ID!) { user(id: $id) { name } }`);
 * ```
 *
 * The query argument is unknown!
 * Please regenerate the types.
 */
export function gql(source: string): unknown;

/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "fragment ColourComplete on Colour {\n\n    label\n    name\n    color {\n      h\n      s\n      v\n    }\n    narratives {\n      id\n      slug\n      title\n      category {\n        slug\n        title\n      }\n      date\n      authors {\n        id\n        name\n        presentation\n\n      }\n      cover {\n        id\n        title\n        image {\n          id\n          url\n          width\n          height\n          extension\n          filesize\n        }\n        type\n        url\n        embed\n      }\n    }\n  }\n"): (typeof documents)["fragment ColourComplete on Colour {\n\n    label\n    name\n    color {\n      h\n      s\n      v\n    }\n    narratives {\n      id\n      slug\n      title\n      category {\n        slug\n        title\n      }\n      date\n      authors {\n        id\n        name\n        presentation\n\n      }\n      cover {\n        id\n        title\n        image {\n          id\n          url\n          width\n          height\n          extension\n          filesize\n        }\n        type\n        url\n        embed\n      }\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  query colours($where: ColourWhereInput) {\n    colours(where:$where, orderBy: [{name: asc}]) {\n      ...ColourComplete\n    }\n  }\n"): (typeof documents)["\n  query colours($where: ColourWhereInput) {\n    colours(where:$where, orderBy: [{name: asc}]) {\n      ...ColourComplete\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "fragment EventComplete on Event {\n    slug\n    label\n    type {\n      label\n    }\n    startDate\n    endDate\n    place\n    description {\n      document\n    }\n    narratives {\n      id\n      slug\n      title\n      category {\n        slug\n        title\n      }\n      date\n      authors {\n        id\n        name\n        presentation\n\n      }\n      cover {\n        id\n        title\n        image {\n          id\n          url\n          width\n          height\n          extension\n          filesize\n        }\n        type\n        url\n        embed\n\n      }\n    }\n  }\n"): (typeof documents)["fragment EventComplete on Event {\n    slug\n    label\n    type {\n      label\n    }\n    startDate\n    endDate\n    place\n    description {\n      document\n    }\n    narratives {\n      id\n      slug\n      title\n      category {\n        slug\n        title\n      }\n      date\n      authors {\n        id\n        name\n        presentation\n\n      }\n      cover {\n        id\n        title\n        image {\n          id\n          url\n          width\n          height\n          extension\n          filesize\n        }\n        type\n        url\n        embed\n\n      }\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  query event($where: EventWhereUniqueInput!) {\n    event(where: $where) {\n      ...EventComplete\n    }\n  }\n"): (typeof documents)["\n  query event($where: EventWhereUniqueInput!) {\n    event(where: $where) {\n      ...EventComplete\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  query events($where: EventWhereInput) {\n    events(where: $where) {\n      ...EventComplete\n    }\n  }\n"): (typeof documents)["\n  query events($where: EventWhereInput) {\n    events(where: $where) {\n      ...EventComplete\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "fragment GlossaryComplete on Glossary {\n    slug\n    label\n    description {\n      document\n    }\n    type {\n      label\n      category\n    }\n    chemicalFormula\n    dateOfDiscoveryInvention\n    medias {\n      id\n        title\n        image {\n          id\n          url\n          width\n          height\n          extension\n          filesize\n        }\n        type\n        url\n        embed\n    }\n    narratives {\n      id\n      slug\n      title\n      category {\n        slug\n        title\n      }\n      date\n      authors {\n        id\n        name\n        presentation\n\n      }\n      cover {\n        id\n        title\n        image {\n          id\n          url\n          width\n          height\n          extension\n          filesize\n        }\n        type\n        url\n        embed\n      }\n    }\n  }\n"): (typeof documents)["fragment GlossaryComplete on Glossary {\n    slug\n    label\n    description {\n      document\n    }\n    type {\n      label\n      category\n    }\n    chemicalFormula\n    dateOfDiscoveryInvention\n    medias {\n      id\n        title\n        image {\n          id\n          url\n          width\n          height\n          extension\n          filesize\n        }\n        type\n        url\n        embed\n    }\n    narratives {\n      id\n      slug\n      title\n      category {\n        slug\n        title\n      }\n      date\n      authors {\n        id\n        name\n        presentation\n\n      }\n      cover {\n        id\n        title\n        image {\n          id\n          url\n          width\n          height\n          extension\n          filesize\n        }\n        type\n        url\n        embed\n      }\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  query glossary($where: GlossaryWhereUniqueInput!) {\n    glossary(where: $where) {\n      ...GlossaryComplete\n    }\n  }\n"): (typeof documents)["\n  query glossary($where: GlossaryWhereUniqueInput!) {\n    glossary(where: $where) {\n      ...GlossaryComplete\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  query glossaries($where: GlossaryWhereInput) {\n    glossaries(where: $where) {\n      ...GlossaryComplete\n    }\n  }\n"): (typeof documents)["\n  query glossaries($where: GlossaryWhereInput) {\n    glossaries(where: $where) {\n      ...GlossaryComplete\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "mutation AuthenticateUserWithPassword( $password: String!, $name: String!) {\n  authenticateUserWithPassword( password: $password, name: $name) {\n    ... on UserAuthenticationWithPasswordSuccess {\n      sessionToken\n    }\n  }\n}"): (typeof documents)["mutation AuthenticateUserWithPassword( $password: String!, $name: String!) {\n  authenticateUserWithPassword( password: $password, name: $name) {\n    ... on UserAuthenticationWithPasswordSuccess {\n      sessionToken\n    }\n  }\n}"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "query AuthenticatedItem {\n  authenticatedItem {\n    ... on User {\n      name\n      \n    }\n  }\n}"): (typeof documents)["query AuthenticatedItem {\n  authenticatedItem {\n    ... on User {\n      name\n      \n    }\n  }\n}"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  query NarrativesHome($whereNarrative: NarrativeWhereInput, $whereObject: ObjectWhereInput) {\n      narratives(where: $whereNarrative, orderBy: [{createdAt:\tdesc}, {updatedAt: desc}] ) {\n        ...NarrativeComplete\n      }\n      objects(where: $whereObject){\n        ...ObjectComplete\n      }\n      staticContent {\n        baseline\n      }\n      \n      }\n"): (typeof documents)["\n  query NarrativesHome($whereNarrative: NarrativeWhereInput, $whereObject: ObjectWhereInput) {\n      narratives(where: $whereNarrative, orderBy: [{createdAt:\tdesc}, {updatedAt: desc}] ) {\n        ...NarrativeComplete\n      }\n      objects(where: $whereObject){\n        ...ObjectComplete\n      }\n      staticContent {\n        baseline\n      }\n      \n      }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "fragment NarrativeComplete on Narrative {\n  id\n  slug\n  title\n  date\n  language\n  cover {\n    id\n    title\n    image {\n      id\n      url\n      width\n      height\n      extension\n      filesize\n    }\n    type\n    url\n    embed\n  }\n  content {\n    document(hydrateRelationships: true)\n  }\n  references { \n    slug\n    label\n    citation {\n      document\n    }\n  }\n  extraReferences { \n    slug\n    label\n    citation {\n      document\n    }\n  }\n  authors {\n    id\n    name\n    presentation\n    personalPageURL\n  }\n  category {\n    slug\n   title\n  }\n}\n"): (typeof documents)["fragment NarrativeComplete on Narrative {\n  id\n  slug\n  title\n  date\n  language\n  cover {\n    id\n    title\n    image {\n      id\n      url\n      width\n      height\n      extension\n      filesize\n    }\n    type\n    url\n    embed\n  }\n  content {\n    document(hydrateRelationships: true)\n  }\n  references { \n    slug\n    label\n    citation {\n      document\n    }\n  }\n  extraReferences { \n    slug\n    label\n    citation {\n      document\n    }\n  }\n  authors {\n    id\n    name\n    presentation\n    personalPageURL\n  }\n  category {\n    slug\n   title\n  }\n}\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  query narrative($where: NarrativeWhereUniqueInput!) {\n    narrative(where: $where) {\n      ...NarrativeComplete\n    }\n  }\n"): (typeof documents)["\n  query narrative($where: NarrativeWhereUniqueInput!) {\n    narrative(where: $where) {\n      ...NarrativeComplete\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  query narratives {\n    narratives {\n      ...NarrativeComplete\n    }\n  }\n"): (typeof documents)["\n  query narratives {\n    narratives {\n      ...NarrativeComplete\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  query public_narratives {\n    narratives(where: {status: {equals: \"public\"}}) {\n      ...NarrativeComplete\n    }\n  }\n"): (typeof documents)["\n  query public_narratives {\n    narratives(where: {status: {equals: \"public\"}}) {\n      ...NarrativeComplete\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  query NarrativeCategory($slug: String!, $narrativesWhere: NarrativeWhereInput) {\n    narrativeCategory(where: {slug:$slug}) {\n      description {\n        document\n      }\n      title\n      narratives(where: $narrativesWhere) {\n        ...NarrativeComplete\n      }\n    }\n  }\n"): (typeof documents)["\n  query NarrativeCategory($slug: String!, $narrativesWhere: NarrativeWhereInput) {\n    narrativeCategory(where: {slug:$slug}) {\n      description {\n        document\n      }\n      title\n      narratives(where: $narrativesWhere) {\n        ...NarrativeComplete\n      }\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "fragment ObjectComplete on Object {\n    id\n    slug\n    label\n    author\n    location\n    medium\n    dimensions\n    description {\n      document\n    }\n    digitalizedByChromotope\n    type {\n      label\n    }\n    date\n    cover {\n      id\n        title\n        image {\n          id\n          url\n          width\n          height\n          extension\n          filesize\n        }\n        type\n        url\n        embed\n    }\n    medias {\n      id\n        title\n        order\n        image {\n          id\n          url\n          width\n          height\n          extension\n          filesize\n        }\n        type\n        url\n        embed\n    }\n    mediasCount\n    reference {\n      id\n      citation {\n        document\n      }\n    }\n    colours {\n      id\n      label\n      color {\n        h\n        s\n        v\n      }\n    }\n    narratives {\n      ...NarrativeComplete\n    }\n  }\n"): (typeof documents)["fragment ObjectComplete on Object {\n    id\n    slug\n    label\n    author\n    location\n    medium\n    dimensions\n    description {\n      document\n    }\n    digitalizedByChromotope\n    type {\n      label\n    }\n    date\n    cover {\n      id\n        title\n        image {\n          id\n          url\n          width\n          height\n          extension\n          filesize\n        }\n        type\n        url\n        embed\n    }\n    medias {\n      id\n        title\n        order\n        image {\n          id\n          url\n          width\n          height\n          extension\n          filesize\n        }\n        type\n        url\n        embed\n    }\n    mediasCount\n    reference {\n      id\n      citation {\n        document\n      }\n    }\n    colours {\n      id\n      label\n      color {\n        h\n        s\n        v\n      }\n    }\n    narratives {\n      ...NarrativeComplete\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  query Object($where: ObjectWhereUniqueInput!) {\n    object(where: $where) {\n      ...ObjectComplete\n    }\n  }\n"): (typeof documents)["\n  query Object($where: ObjectWhereUniqueInput!) {\n    object(where: $where) {\n      ...ObjectComplete\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  query Objects($where: ObjectWhereInput) {\n    objects(where: $where) {\n      ...ObjectComplete\n    }\n  }\n"): (typeof documents)["\n  query Objects($where: ObjectWhereInput) {\n    objects(where: $where) {\n      ...ObjectComplete\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "fragment OrganisationComplete on Organisation {\n    slug\n    label\n    dateOfInception\n    type {\n      label\n    }\n    description {\n      document\n    }\n    narratives {\n      id\n      slug\n      title\n      category {\n        slug\n        title\n      }\n      date\n      authors {\n        id\n        name\n        presentation\n\n      }\n      cover {\n        id\n        title\n        image {\n          id\n          url\n          width\n          height\n          extension\n          filesize\n        }\n        type\n        url\n        embed\n      }\n    }\n  }\n"): (typeof documents)["fragment OrganisationComplete on Organisation {\n    slug\n    label\n    dateOfInception\n    type {\n      label\n    }\n    description {\n      document\n    }\n    narratives {\n      id\n      slug\n      title\n      category {\n        slug\n        title\n      }\n      date\n      authors {\n        id\n        name\n        presentation\n\n      }\n      cover {\n        id\n        title\n        image {\n          id\n          url\n          width\n          height\n          extension\n          filesize\n        }\n        type\n        url\n        embed\n      }\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  query organisation($where: OrganisationWhereUniqueInput!) {\n    organisation(where: $where) {\n      ...OrganisationComplete\n    }\n  }\n"): (typeof documents)["\n  query organisation($where: OrganisationWhereUniqueInput!) {\n    organisation(where: $where) {\n      ...OrganisationComplete\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  query organisations($where: OrganisationWhereInput) {\n    organisations(where: $where) {\n      ...OrganisationComplete\n    }\n  }\n"): (typeof documents)["\n  query organisations($where: OrganisationWhereInput) {\n    organisations(where: $where) {\n      ...OrganisationComplete\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "fragment PersonComplete on Person {\n    slug\n    label\n    firstName\n    lastName\n    fullName\n    biography {\n      document\n    }\n    dateOfBirth\n    dateOfDeath\n    occupations {\n      label\n    }\n    narratives {\n      id\n      slug\n      title\n      category {\n        slug\n        title\n      }\n      date\n      authors {\n        id\n        name\n        presentation\n\n      }\n      cover {\n        id\n        title\n        image {\n          id\n          url\n          width\n          height\n          extension\n          filesize\n        }\n        type\n        url\n        embed\n      }\n    }\n  }\n"): (typeof documents)["fragment PersonComplete on Person {\n    slug\n    label\n    firstName\n    lastName\n    fullName\n    biography {\n      document\n    }\n    dateOfBirth\n    dateOfDeath\n    occupations {\n      label\n    }\n    narratives {\n      id\n      slug\n      title\n      category {\n        slug\n        title\n      }\n      date\n      authors {\n        id\n        name\n        presentation\n\n      }\n      cover {\n        id\n        title\n        image {\n          id\n          url\n          width\n          height\n          extension\n          filesize\n        }\n        type\n        url\n        embed\n      }\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n\n  query person($where: PersonWhereUniqueInput!) {\n    person(where: $where) {\n      ...PersonComplete\n    }\n  }\n"): (typeof documents)["\n\n  query person($where: PersonWhereUniqueInput!) {\n    person(where: $where) {\n      ...PersonComplete\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n\n  query people($where: PersonWhereInput) {\n    people(where:$where) {\n      ...PersonComplete\n    }\n  }\n"): (typeof documents)["\n\n  query people($where: PersonWhereInput) {\n    people(where:$where) {\n      ...PersonComplete\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "fragment ReferenceComplete on Reference {\n    label\n    slug\n    note\n    citation {\n      document\n    }\n    narratives {\n      ...NarrativeComplete\n    }\n\n    medias {\n      id\n        title\n        order\n        image {\n          id\n          url\n          width\n          height\n          extension\n          filesize\n        }\n        type\n        url\n        embed\n    }\n}\n"): (typeof documents)["fragment ReferenceComplete on Reference {\n    label\n    slug\n    note\n    citation {\n      document\n    }\n    narratives {\n      ...NarrativeComplete\n    }\n\n    medias {\n      id\n        title\n        order\n        image {\n          id\n          url\n          width\n          height\n          extension\n          filesize\n        }\n        type\n        url\n        embed\n    }\n}\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  query reference($where: ReferenceWhereUniqueInput!) {\n    reference(where: $where) {\n      ...ReferenceComplete\n    }\n  }\n"): (typeof documents)["\n  query reference($where: ReferenceWhereUniqueInput!) {\n    reference(where: $where) {\n      ...ReferenceComplete\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  query references($where: ReferenceWhereInput) {\n    references(where: $where, orderBy:[{label: asc}]) {\n      ...ReferenceComplete\n    }\n  }\n"): (typeof documents)["\n  query references($where: ReferenceWhereInput) {\n    references(where: $where, orderBy:[{label: asc}]) {\n      ...ReferenceComplete\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "query StaticContent {\n  staticContent {\n    id\n    baseline\n    colourWheelIntroduction {\n      document\n    }\n    about {\n      document\n    }\n    credits {\n      document\n    }\n    partnersLogos {\n      image {\n        id\n          url\n          width\n          height\n          extension\n          filesize\n      }\n      institutionName\n      url\n    }\n    funding {\n      document\n    }\n    fundingLogos {\n      image {\n\n      id\n          url\n          width\n          height\n          extension\n          filesize\n      }\n      institutionName\n      url\n    }\n    legalNotice {\n     document \n    }\n  }\n}"): (typeof documents)["query StaticContent {\n  staticContent {\n    id\n    baseline\n    colourWheelIntroduction {\n      document\n    }\n    about {\n      document\n    }\n    credits {\n      document\n    }\n    partnersLogos {\n      image {\n        id\n          url\n          width\n          height\n          extension\n          filesize\n      }\n      institutionName\n      url\n    }\n    funding {\n      document\n    }\n    fundingLogos {\n      image {\n\n      id\n          url\n          width\n          height\n          extension\n          filesize\n      }\n      institutionName\n      url\n    }\n    legalNotice {\n     document \n    }\n  }\n}"];

export function gql(source: string) {
  return (documents as any)[source] ?? {};
}

export type DocumentType<TDocumentNode extends DocumentNode<any, any>> = TDocumentNode extends DocumentNode<  infer TType,  any>  ? TType  : never;