/* eslint-disable */
import { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type MakeEmpty<T extends { [key: string]: unknown }, K extends keyof T> = { [_ in K]?: never };
export type Incremental<T> = T | { [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string; }
  String: { input: string; output: string; }
  Boolean: { input: boolean; output: boolean; }
  Int: { input: number; output: number; }
  Float: { input: number; output: number; }
  DateTime: { input: any; output: any; }
  /** The `JSON` scalar type represents JSON values as specified by [ECMA-404](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf). */
  JSON: { input: any; output: any; }
  /** The `Upload` scalar type represents a file upload. */
  Upload: { input: any; output: any; }
};

export type AuthenticatedItem = User;

export type Author = {
  __typename?: 'Author';
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  id: Scalars['ID']['output'];
  name?: Maybe<Scalars['String']['output']>;
  narratives?: Maybe<Array<Narrative>>;
  narrativesCount?: Maybe<Scalars['Int']['output']>;
  personalPageURL?: Maybe<Scalars['String']['output']>;
  presentation?: Maybe<Scalars['String']['output']>;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
  user?: Maybe<User>;
};


export type AuthorNarrativesArgs = {
  cursor?: InputMaybe<NarrativeWhereUniqueInput>;
  orderBy?: Array<NarrativeOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: NarrativeWhereInput;
};


export type AuthorNarrativesCountArgs = {
  where?: NarrativeWhereInput;
};

export type AuthorCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  narratives?: InputMaybe<NarrativeRelateToManyForCreateInput>;
  personalPageURL?: InputMaybe<Scalars['String']['input']>;
  presentation?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  user?: InputMaybe<UserRelateToOneForCreateInput>;
};

export type AuthorManyRelationFilter = {
  every?: InputMaybe<AuthorWhereInput>;
  none?: InputMaybe<AuthorWhereInput>;
  some?: InputMaybe<AuthorWhereInput>;
};

export type AuthorOrderByInput = {
  createdAt?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  name?: InputMaybe<OrderDirection>;
  personalPageURL?: InputMaybe<OrderDirection>;
  presentation?: InputMaybe<OrderDirection>;
  updatedAt?: InputMaybe<OrderDirection>;
};

export type AuthorRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<AuthorWhereUniqueInput>>;
  create?: InputMaybe<Array<AuthorCreateInput>>;
};

export type AuthorRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<AuthorWhereUniqueInput>>;
  create?: InputMaybe<Array<AuthorCreateInput>>;
  disconnect?: InputMaybe<Array<AuthorWhereUniqueInput>>;
  set?: InputMaybe<Array<AuthorWhereUniqueInput>>;
};

export type AuthorRelateToOneForCreateInput = {
  connect?: InputMaybe<AuthorWhereUniqueInput>;
  create?: InputMaybe<AuthorCreateInput>;
};

export type AuthorRelateToOneForUpdateInput = {
  connect?: InputMaybe<AuthorWhereUniqueInput>;
  create?: InputMaybe<AuthorCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

export type AuthorUpdateArgs = {
  data: AuthorUpdateInput;
  where: AuthorWhereUniqueInput;
};

export type AuthorUpdateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  narratives?: InputMaybe<NarrativeRelateToManyForUpdateInput>;
  personalPageURL?: InputMaybe<Scalars['String']['input']>;
  presentation?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  user?: InputMaybe<UserRelateToOneForUpdateInput>;
};

export type AuthorWhereInput = {
  AND?: InputMaybe<Array<AuthorWhereInput>>;
  NOT?: InputMaybe<Array<AuthorWhereInput>>;
  OR?: InputMaybe<Array<AuthorWhereInput>>;
  createdAt?: InputMaybe<DateTimeNullableFilter>;
  id?: InputMaybe<IdFilter>;
  name?: InputMaybe<StringFilter>;
  narratives?: InputMaybe<NarrativeManyRelationFilter>;
  personalPageURL?: InputMaybe<StringFilter>;
  presentation?: InputMaybe<StringFilter>;
  updatedAt?: InputMaybe<DateTimeNullableFilter>;
  user?: InputMaybe<UserWhereInput>;
};

export type AuthorWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type BooleanFilter = {
  equals?: InputMaybe<Scalars['Boolean']['input']>;
  not?: InputMaybe<BooleanFilter>;
};

export type ColorHsvFilter = {
  equals?: InputMaybe<ColorHsvInputPartial>;
  gt?: InputMaybe<ColorHsvInputPartial>;
  gte?: InputMaybe<ColorHsvInputPartial>;
  lt?: InputMaybe<ColorHsvInputPartial>;
  lte?: InputMaybe<ColorHsvInputPartial>;
};

export type ColorHsvInput = {
  h: Scalars['Float']['input'];
  s: Scalars['Float']['input'];
  v: Scalars['Float']['input'];
};

export type ColorHsvInputPartial = {
  h?: InputMaybe<Scalars['Float']['input']>;
  s?: InputMaybe<Scalars['Float']['input']>;
  v?: InputMaybe<Scalars['Float']['input']>;
};

export type ColorHsvOutput = {
  __typename?: 'ColorHSVOutput';
  h: Scalars['Float']['output'];
  s: Scalars['Float']['output'];
  v: Scalars['Float']['output'];
};

export type Colour = {
  __typename?: 'Colour';
  color?: Maybe<ColorHsvOutput>;
  colorOrder?: Maybe<Scalars['String']['output']>;
  id: Scalars['ID']['output'];
  label?: Maybe<Scalars['String']['output']>;
  name?: Maybe<Scalars['String']['output']>;
  narratives?: Maybe<Array<Narrative>>;
  narrativesCount?: Maybe<Scalars['Int']['output']>;
};


export type ColourNarrativesArgs = {
  cursor?: InputMaybe<NarrativeWhereUniqueInput>;
  orderBy?: Array<NarrativeOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: NarrativeWhereInput;
};


export type ColourNarrativesCountArgs = {
  where?: NarrativeWhereInput;
};

export type ColourCreateInput = {
  color?: InputMaybe<ColorHsvInput>;
  colorOrder?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  narratives?: InputMaybe<NarrativeRelateToManyForCreateInput>;
};

export type ColourManyRelationFilter = {
  every?: InputMaybe<ColourWhereInput>;
  none?: InputMaybe<ColourWhereInput>;
  some?: InputMaybe<ColourWhereInput>;
};

export type ColourOrderByInput = {
  colorOrder?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  label?: InputMaybe<OrderDirection>;
  name?: InputMaybe<OrderDirection>;
};

export type ColourRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<ColourWhereUniqueInput>>;
  create?: InputMaybe<Array<ColourCreateInput>>;
};

export type ColourRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<ColourWhereUniqueInput>>;
  create?: InputMaybe<Array<ColourCreateInput>>;
  disconnect?: InputMaybe<Array<ColourWhereUniqueInput>>;
  set?: InputMaybe<Array<ColourWhereUniqueInput>>;
};

export type ColourUpdateArgs = {
  data: ColourUpdateInput;
  where: ColourWhereUniqueInput;
};

export type ColourUpdateInput = {
  color?: InputMaybe<ColorHsvInput>;
  colorOrder?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  narratives?: InputMaybe<NarrativeRelateToManyForUpdateInput>;
};

export type ColourWhereInput = {
  AND?: InputMaybe<Array<ColourWhereInput>>;
  NOT?: InputMaybe<Array<ColourWhereInput>>;
  OR?: InputMaybe<Array<ColourWhereInput>>;
  color?: InputMaybe<ColorHsvFilter>;
  colorOrder?: InputMaybe<StringFilter>;
  id?: InputMaybe<IdFilter>;
  label?: InputMaybe<StringFilter>;
  name?: InputMaybe<StringFilter>;
  narratives?: InputMaybe<NarrativeManyRelationFilter>;
};

export type ColourWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type CreateInitialUserInput = {
  email?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  password?: InputMaybe<Scalars['String']['input']>;
  role?: InputMaybe<Scalars['String']['input']>;
};

export type DateTimeNullableFilter = {
  equals?: InputMaybe<Scalars['DateTime']['input']>;
  gt?: InputMaybe<Scalars['DateTime']['input']>;
  gte?: InputMaybe<Scalars['DateTime']['input']>;
  in?: InputMaybe<Array<Scalars['DateTime']['input']>>;
  lt?: InputMaybe<Scalars['DateTime']['input']>;
  lte?: InputMaybe<Scalars['DateTime']['input']>;
  not?: InputMaybe<DateTimeNullableFilter>;
  notIn?: InputMaybe<Array<Scalars['DateTime']['input']>>;
};

export type Event = {
  __typename?: 'Event';
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  description?: Maybe<Event_Description_Document>;
  endDate?: Maybe<Scalars['String']['output']>;
  id: Scalars['ID']['output'];
  label?: Maybe<Scalars['String']['output']>;
  narratives?: Maybe<Array<Narrative>>;
  narrativesCount?: Maybe<Scalars['Int']['output']>;
  place?: Maybe<Scalars['String']['output']>;
  slug?: Maybe<Scalars['String']['output']>;
  startDate?: Maybe<Scalars['String']['output']>;
  type?: Maybe<EventType>;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
  wikidataId?: Maybe<Scalars['String']['output']>;
};


export type EventNarrativesArgs = {
  cursor?: InputMaybe<NarrativeWhereUniqueInput>;
  orderBy?: Array<NarrativeOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: NarrativeWhereInput;
};


export type EventNarrativesCountArgs = {
  where?: NarrativeWhereInput;
};

export type EventCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  description?: InputMaybe<Scalars['JSON']['input']>;
  endDate?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  narratives?: InputMaybe<NarrativeRelateToManyForCreateInput>;
  place?: InputMaybe<Scalars['String']['input']>;
  slug?: InputMaybe<Scalars['String']['input']>;
  startDate?: InputMaybe<Scalars['String']['input']>;
  type?: InputMaybe<EventTypeRelateToOneForCreateInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  wikidataId?: InputMaybe<Scalars['String']['input']>;
};

export type EventManyRelationFilter = {
  every?: InputMaybe<EventWhereInput>;
  none?: InputMaybe<EventWhereInput>;
  some?: InputMaybe<EventWhereInput>;
};

export type EventOrderByInput = {
  createdAt?: InputMaybe<OrderDirection>;
  endDate?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  label?: InputMaybe<OrderDirection>;
  place?: InputMaybe<OrderDirection>;
  slug?: InputMaybe<OrderDirection>;
  startDate?: InputMaybe<OrderDirection>;
  updatedAt?: InputMaybe<OrderDirection>;
  wikidataId?: InputMaybe<OrderDirection>;
};

export type EventRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<EventWhereUniqueInput>>;
  create?: InputMaybe<Array<EventCreateInput>>;
};

export type EventRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<EventWhereUniqueInput>>;
  create?: InputMaybe<Array<EventCreateInput>>;
  disconnect?: InputMaybe<Array<EventWhereUniqueInput>>;
  set?: InputMaybe<Array<EventWhereUniqueInput>>;
};

export type EventType = {
  __typename?: 'EventType';
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  description?: Maybe<EventType_Description_Document>;
  id: Scalars['ID']['output'];
  label?: Maybe<Scalars['String']['output']>;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
  wikidataId?: Maybe<Scalars['String']['output']>;
};

export type EventTypeCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  description?: InputMaybe<Scalars['JSON']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  wikidataId?: InputMaybe<Scalars['String']['input']>;
};

export type EventTypeOrderByInput = {
  createdAt?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  label?: InputMaybe<OrderDirection>;
  updatedAt?: InputMaybe<OrderDirection>;
  wikidataId?: InputMaybe<OrderDirection>;
};

export type EventTypeRelateToOneForCreateInput = {
  connect?: InputMaybe<EventTypeWhereUniqueInput>;
  create?: InputMaybe<EventTypeCreateInput>;
};

export type EventTypeRelateToOneForUpdateInput = {
  connect?: InputMaybe<EventTypeWhereUniqueInput>;
  create?: InputMaybe<EventTypeCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

export type EventTypeUpdateArgs = {
  data: EventTypeUpdateInput;
  where: EventTypeWhereUniqueInput;
};

export type EventTypeUpdateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  description?: InputMaybe<Scalars['JSON']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  wikidataId?: InputMaybe<Scalars['String']['input']>;
};

export type EventTypeWhereInput = {
  AND?: InputMaybe<Array<EventTypeWhereInput>>;
  NOT?: InputMaybe<Array<EventTypeWhereInput>>;
  OR?: InputMaybe<Array<EventTypeWhereInput>>;
  createdAt?: InputMaybe<DateTimeNullableFilter>;
  id?: InputMaybe<IdFilter>;
  label?: InputMaybe<StringFilter>;
  updatedAt?: InputMaybe<DateTimeNullableFilter>;
  wikidataId?: InputMaybe<StringNullableFilter>;
};

export type EventTypeWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  wikidataId?: InputMaybe<Scalars['String']['input']>;
};

export type EventType_Description_Document = {
  __typename?: 'EventType_description_Document';
  document: Scalars['JSON']['output'];
};


export type EventType_Description_DocumentDocumentArgs = {
  hydrateRelationships?: Scalars['Boolean']['input'];
};

export type EventUpdateArgs = {
  data: EventUpdateInput;
  where: EventWhereUniqueInput;
};

export type EventUpdateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  description?: InputMaybe<Scalars['JSON']['input']>;
  endDate?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  narratives?: InputMaybe<NarrativeRelateToManyForUpdateInput>;
  place?: InputMaybe<Scalars['String']['input']>;
  slug?: InputMaybe<Scalars['String']['input']>;
  startDate?: InputMaybe<Scalars['String']['input']>;
  type?: InputMaybe<EventTypeRelateToOneForUpdateInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  wikidataId?: InputMaybe<Scalars['String']['input']>;
};

export type EventWhereInput = {
  AND?: InputMaybe<Array<EventWhereInput>>;
  NOT?: InputMaybe<Array<EventWhereInput>>;
  OR?: InputMaybe<Array<EventWhereInput>>;
  createdAt?: InputMaybe<DateTimeNullableFilter>;
  endDate?: InputMaybe<StringNullableFilter>;
  id?: InputMaybe<IdFilter>;
  label?: InputMaybe<StringFilter>;
  narratives?: InputMaybe<NarrativeManyRelationFilter>;
  place?: InputMaybe<StringFilter>;
  slug?: InputMaybe<StringFilter>;
  startDate?: InputMaybe<StringNullableFilter>;
  type?: InputMaybe<EventTypeWhereInput>;
  updatedAt?: InputMaybe<DateTimeNullableFilter>;
  wikidataId?: InputMaybe<StringNullableFilter>;
};

export type EventWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  slug?: InputMaybe<Scalars['String']['input']>;
  wikidataId?: InputMaybe<Scalars['String']['input']>;
};

export type Event_Description_Document = {
  __typename?: 'Event_description_Document';
  document: Scalars['JSON']['output'];
};


export type Event_Description_DocumentDocumentArgs = {
  hydrateRelationships?: Scalars['Boolean']['input'];
};

export type Glossary = {
  __typename?: 'Glossary';
  chemicalFormula?: Maybe<Scalars['String']['output']>;
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  dateOfDiscoveryInvention?: Maybe<Scalars['String']['output']>;
  description?: Maybe<Glossary_Description_Document>;
  id: Scalars['ID']['output'];
  label?: Maybe<Scalars['String']['output']>;
  medias?: Maybe<Array<Media>>;
  mediasCount?: Maybe<Scalars['Int']['output']>;
  narratives?: Maybe<Array<Narrative>>;
  narrativesCount?: Maybe<Scalars['Int']['output']>;
  slug?: Maybe<Scalars['String']['output']>;
  type?: Maybe<GlossaryType>;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
  wikidataId?: Maybe<Scalars['String']['output']>;
};


export type GlossaryMediasArgs = {
  cursor?: InputMaybe<MediaWhereUniqueInput>;
  orderBy?: Array<MediaOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: MediaWhereInput;
};


export type GlossaryMediasCountArgs = {
  where?: MediaWhereInput;
};


export type GlossaryNarrativesArgs = {
  cursor?: InputMaybe<NarrativeWhereUniqueInput>;
  orderBy?: Array<NarrativeOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: NarrativeWhereInput;
};


export type GlossaryNarrativesCountArgs = {
  where?: NarrativeWhereInput;
};

export type GlossaryCreateInput = {
  chemicalFormula?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  dateOfDiscoveryInvention?: InputMaybe<Scalars['String']['input']>;
  description?: InputMaybe<Scalars['JSON']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  medias?: InputMaybe<MediaRelateToManyForCreateInput>;
  narratives?: InputMaybe<NarrativeRelateToManyForCreateInput>;
  slug?: InputMaybe<Scalars['String']['input']>;
  type?: InputMaybe<GlossaryTypeRelateToOneForCreateInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  wikidataId?: InputMaybe<Scalars['String']['input']>;
};

export type GlossaryManyRelationFilter = {
  every?: InputMaybe<GlossaryWhereInput>;
  none?: InputMaybe<GlossaryWhereInput>;
  some?: InputMaybe<GlossaryWhereInput>;
};

export type GlossaryOrderByInput = {
  chemicalFormula?: InputMaybe<OrderDirection>;
  createdAt?: InputMaybe<OrderDirection>;
  dateOfDiscoveryInvention?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  label?: InputMaybe<OrderDirection>;
  slug?: InputMaybe<OrderDirection>;
  updatedAt?: InputMaybe<OrderDirection>;
  wikidataId?: InputMaybe<OrderDirection>;
};

export type GlossaryRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<GlossaryWhereUniqueInput>>;
  create?: InputMaybe<Array<GlossaryCreateInput>>;
};

export type GlossaryRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<GlossaryWhereUniqueInput>>;
  create?: InputMaybe<Array<GlossaryCreateInput>>;
  disconnect?: InputMaybe<Array<GlossaryWhereUniqueInput>>;
  set?: InputMaybe<Array<GlossaryWhereUniqueInput>>;
};

export type GlossaryType = {
  __typename?: 'GlossaryType';
  category?: Maybe<Scalars['String']['output']>;
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  description?: Maybe<GlossaryType_Description_Document>;
  id: Scalars['ID']['output'];
  label?: Maybe<Scalars['String']['output']>;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
  wikidataId?: Maybe<Scalars['String']['output']>;
};

export type GlossaryTypeCreateInput = {
  category?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  description?: InputMaybe<Scalars['JSON']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  wikidataId?: InputMaybe<Scalars['String']['input']>;
};

export type GlossaryTypeOrderByInput = {
  category?: InputMaybe<OrderDirection>;
  createdAt?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  label?: InputMaybe<OrderDirection>;
  updatedAt?: InputMaybe<OrderDirection>;
  wikidataId?: InputMaybe<OrderDirection>;
};

export type GlossaryTypeRelateToOneForCreateInput = {
  connect?: InputMaybe<GlossaryTypeWhereUniqueInput>;
  create?: InputMaybe<GlossaryTypeCreateInput>;
};

export type GlossaryTypeRelateToOneForUpdateInput = {
  connect?: InputMaybe<GlossaryTypeWhereUniqueInput>;
  create?: InputMaybe<GlossaryTypeCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

export type GlossaryTypeUpdateArgs = {
  data: GlossaryTypeUpdateInput;
  where: GlossaryTypeWhereUniqueInput;
};

export type GlossaryTypeUpdateInput = {
  category?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  description?: InputMaybe<Scalars['JSON']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  wikidataId?: InputMaybe<Scalars['String']['input']>;
};

export type GlossaryTypeWhereInput = {
  AND?: InputMaybe<Array<GlossaryTypeWhereInput>>;
  NOT?: InputMaybe<Array<GlossaryTypeWhereInput>>;
  OR?: InputMaybe<Array<GlossaryTypeWhereInput>>;
  category?: InputMaybe<StringFilter>;
  createdAt?: InputMaybe<DateTimeNullableFilter>;
  id?: InputMaybe<IdFilter>;
  label?: InputMaybe<StringFilter>;
  updatedAt?: InputMaybe<DateTimeNullableFilter>;
  wikidataId?: InputMaybe<StringNullableFilter>;
};

export type GlossaryTypeWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  wikidataId?: InputMaybe<Scalars['String']['input']>;
};

export type GlossaryType_Description_Document = {
  __typename?: 'GlossaryType_description_Document';
  document: Scalars['JSON']['output'];
};


export type GlossaryType_Description_DocumentDocumentArgs = {
  hydrateRelationships?: Scalars['Boolean']['input'];
};

export type GlossaryUpdateArgs = {
  data: GlossaryUpdateInput;
  where: GlossaryWhereUniqueInput;
};

export type GlossaryUpdateInput = {
  chemicalFormula?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  dateOfDiscoveryInvention?: InputMaybe<Scalars['String']['input']>;
  description?: InputMaybe<Scalars['JSON']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  medias?: InputMaybe<MediaRelateToManyForUpdateInput>;
  narratives?: InputMaybe<NarrativeRelateToManyForUpdateInput>;
  slug?: InputMaybe<Scalars['String']['input']>;
  type?: InputMaybe<GlossaryTypeRelateToOneForUpdateInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  wikidataId?: InputMaybe<Scalars['String']['input']>;
};

export type GlossaryWhereInput = {
  AND?: InputMaybe<Array<GlossaryWhereInput>>;
  NOT?: InputMaybe<Array<GlossaryWhereInput>>;
  OR?: InputMaybe<Array<GlossaryWhereInput>>;
  chemicalFormula?: InputMaybe<StringFilter>;
  createdAt?: InputMaybe<DateTimeNullableFilter>;
  dateOfDiscoveryInvention?: InputMaybe<StringNullableFilter>;
  id?: InputMaybe<IdFilter>;
  label?: InputMaybe<StringFilter>;
  medias?: InputMaybe<MediaManyRelationFilter>;
  narratives?: InputMaybe<NarrativeManyRelationFilter>;
  slug?: InputMaybe<StringFilter>;
  type?: InputMaybe<GlossaryTypeWhereInput>;
  updatedAt?: InputMaybe<DateTimeNullableFilter>;
  wikidataId?: InputMaybe<StringNullableFilter>;
};

export type GlossaryWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  slug?: InputMaybe<Scalars['String']['input']>;
  wikidataId?: InputMaybe<Scalars['String']['input']>;
};

export type Glossary_Description_Document = {
  __typename?: 'Glossary_description_Document';
  document: Scalars['JSON']['output'];
};


export type Glossary_Description_DocumentDocumentArgs = {
  hydrateRelationships?: Scalars['Boolean']['input'];
};

export type IdFilter = {
  equals?: InputMaybe<Scalars['ID']['input']>;
  gt?: InputMaybe<Scalars['ID']['input']>;
  gte?: InputMaybe<Scalars['ID']['input']>;
  in?: InputMaybe<Array<Scalars['ID']['input']>>;
  lt?: InputMaybe<Scalars['ID']['input']>;
  lte?: InputMaybe<Scalars['ID']['input']>;
  not?: InputMaybe<IdFilter>;
  notIn?: InputMaybe<Array<Scalars['ID']['input']>>;
};

export enum ImageExtension {
  Gif = 'gif',
  Jpg = 'jpg',
  Png = 'png',
  Webp = 'webp'
}

export type ImageFieldInput = {
  upload: Scalars['Upload']['input'];
};

export type ImageFieldOutput = {
  __typename?: 'ImageFieldOutput';
  extension: ImageExtension;
  filesize: Scalars['Int']['output'];
  height: Scalars['Int']['output'];
  id: Scalars['ID']['output'];
  url: Scalars['String']['output'];
  width: Scalars['Int']['output'];
};

export type IntNullableFilter = {
  equals?: InputMaybe<Scalars['Int']['input']>;
  gt?: InputMaybe<Scalars['Int']['input']>;
  gte?: InputMaybe<Scalars['Int']['input']>;
  in?: InputMaybe<Array<Scalars['Int']['input']>>;
  lt?: InputMaybe<Scalars['Int']['input']>;
  lte?: InputMaybe<Scalars['Int']['input']>;
  not?: InputMaybe<IntNullableFilter>;
  notIn?: InputMaybe<Array<Scalars['Int']['input']>>;
};

export type KeystoneAdminMeta = {
  __typename?: 'KeystoneAdminMeta';
  list?: Maybe<KeystoneAdminUiListMeta>;
  lists: Array<KeystoneAdminUiListMeta>;
};


export type KeystoneAdminMetaListArgs = {
  key: Scalars['String']['input'];
};

export type KeystoneAdminUiFieldGroupMeta = {
  __typename?: 'KeystoneAdminUIFieldGroupMeta';
  description?: Maybe<Scalars['String']['output']>;
  fields: Array<KeystoneAdminUiFieldMeta>;
  label: Scalars['String']['output'];
};

export type KeystoneAdminUiFieldMeta = {
  __typename?: 'KeystoneAdminUIFieldMeta';
  createView: KeystoneAdminUiFieldMetaCreateView;
  customViewsIndex?: Maybe<Scalars['Int']['output']>;
  description?: Maybe<Scalars['String']['output']>;
  fieldMeta?: Maybe<Scalars['JSON']['output']>;
  isFilterable: Scalars['Boolean']['output'];
  isNonNull?: Maybe<Array<KeystoneAdminUiFieldMetaIsNonNull>>;
  isOrderable: Scalars['Boolean']['output'];
  itemView?: Maybe<KeystoneAdminUiFieldMetaItemView>;
  label: Scalars['String']['output'];
  listView: KeystoneAdminUiFieldMetaListView;
  path: Scalars['String']['output'];
  search?: Maybe<QueryMode>;
  viewsIndex: Scalars['Int']['output'];
};


export type KeystoneAdminUiFieldMetaItemViewArgs = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type KeystoneAdminUiFieldMetaCreateView = {
  __typename?: 'KeystoneAdminUIFieldMetaCreateView';
  fieldMode: KeystoneAdminUiFieldMetaCreateViewFieldMode;
};

export enum KeystoneAdminUiFieldMetaCreateViewFieldMode {
  Edit = 'edit',
  Hidden = 'hidden'
}

export enum KeystoneAdminUiFieldMetaIsNonNull {
  Create = 'create',
  Read = 'read',
  Update = 'update'
}

export type KeystoneAdminUiFieldMetaItemView = {
  __typename?: 'KeystoneAdminUIFieldMetaItemView';
  fieldMode?: Maybe<KeystoneAdminUiFieldMetaItemViewFieldMode>;
  fieldPosition?: Maybe<KeystoneAdminUiFieldMetaItemViewFieldPosition>;
};

export enum KeystoneAdminUiFieldMetaItemViewFieldMode {
  Edit = 'edit',
  Hidden = 'hidden',
  Read = 'read'
}

export enum KeystoneAdminUiFieldMetaItemViewFieldPosition {
  Form = 'form',
  Sidebar = 'sidebar'
}

export type KeystoneAdminUiFieldMetaListView = {
  __typename?: 'KeystoneAdminUIFieldMetaListView';
  fieldMode: KeystoneAdminUiFieldMetaListViewFieldMode;
};

export enum KeystoneAdminUiFieldMetaListViewFieldMode {
  Hidden = 'hidden',
  Read = 'read'
}

export type KeystoneAdminUiListMeta = {
  __typename?: 'KeystoneAdminUIListMeta';
  description?: Maybe<Scalars['String']['output']>;
  fields: Array<KeystoneAdminUiFieldMeta>;
  groups: Array<KeystoneAdminUiFieldGroupMeta>;
  hideCreate: Scalars['Boolean']['output'];
  hideDelete: Scalars['Boolean']['output'];
  initialColumns: Array<Scalars['String']['output']>;
  initialSort?: Maybe<KeystoneAdminUiSort>;
  isHidden: Scalars['Boolean']['output'];
  isSingleton: Scalars['Boolean']['output'];
  itemQueryName: Scalars['String']['output'];
  key: Scalars['String']['output'];
  label: Scalars['String']['output'];
  labelField: Scalars['String']['output'];
  listQueryName: Scalars['String']['output'];
  pageSize: Scalars['Int']['output'];
  path: Scalars['String']['output'];
  plural: Scalars['String']['output'];
  singular: Scalars['String']['output'];
};

export type KeystoneAdminUiSort = {
  __typename?: 'KeystoneAdminUISort';
  direction: KeystoneAdminUiSortDirection;
  field: Scalars['String']['output'];
};

export enum KeystoneAdminUiSortDirection {
  Asc = 'ASC',
  Desc = 'DESC'
}

export type KeystoneMeta = {
  __typename?: 'KeystoneMeta';
  adminMeta: KeystoneAdminMeta;
};

export type Logo = {
  __typename?: 'Logo';
  id: Scalars['ID']['output'];
  image?: Maybe<ImageFieldOutput>;
  institutionName?: Maybe<Scalars['String']['output']>;
  order?: Maybe<Scalars['Int']['output']>;
  url?: Maybe<Scalars['String']['output']>;
};

export type LogoCreateInput = {
  image?: InputMaybe<ImageFieldInput>;
  institutionName?: InputMaybe<Scalars['String']['input']>;
  order?: InputMaybe<Scalars['Int']['input']>;
  url?: InputMaybe<Scalars['String']['input']>;
};

export type LogoManyRelationFilter = {
  every?: InputMaybe<LogoWhereInput>;
  none?: InputMaybe<LogoWhereInput>;
  some?: InputMaybe<LogoWhereInput>;
};

export type LogoOrderByInput = {
  id?: InputMaybe<OrderDirection>;
  institutionName?: InputMaybe<OrderDirection>;
  order?: InputMaybe<OrderDirection>;
  url?: InputMaybe<OrderDirection>;
};

export type LogoRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<LogoWhereUniqueInput>>;
  create?: InputMaybe<Array<LogoCreateInput>>;
};

export type LogoRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<LogoWhereUniqueInput>>;
  create?: InputMaybe<Array<LogoCreateInput>>;
  disconnect?: InputMaybe<Array<LogoWhereUniqueInput>>;
  set?: InputMaybe<Array<LogoWhereUniqueInput>>;
};

export type LogoUpdateArgs = {
  data: LogoUpdateInput;
  where: LogoWhereUniqueInput;
};

export type LogoUpdateInput = {
  image?: InputMaybe<ImageFieldInput>;
  institutionName?: InputMaybe<Scalars['String']['input']>;
  order?: InputMaybe<Scalars['Int']['input']>;
  url?: InputMaybe<Scalars['String']['input']>;
};

export type LogoWhereInput = {
  AND?: InputMaybe<Array<LogoWhereInput>>;
  NOT?: InputMaybe<Array<LogoWhereInput>>;
  OR?: InputMaybe<Array<LogoWhereInput>>;
  id?: InputMaybe<IdFilter>;
  institutionName?: InputMaybe<StringFilter>;
  order?: InputMaybe<IntNullableFilter>;
  url?: InputMaybe<StringFilter>;
};

export type LogoWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type Media = {
  __typename?: 'Media';
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  embed?: Maybe<Scalars['String']['output']>;
  id: Scalars['ID']['output'];
  image?: Maybe<ImageFieldOutput>;
  order?: Maybe<Scalars['Int']['output']>;
  title?: Maybe<Scalars['String']['output']>;
  type?: Maybe<Scalars['String']['output']>;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
  url?: Maybe<Scalars['String']['output']>;
};

export type MediaCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  embed?: InputMaybe<Scalars['String']['input']>;
  image?: InputMaybe<ImageFieldInput>;
  order?: InputMaybe<Scalars['Int']['input']>;
  title?: InputMaybe<Scalars['String']['input']>;
  type?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  url?: InputMaybe<Scalars['String']['input']>;
};

export type MediaManyRelationFilter = {
  every?: InputMaybe<MediaWhereInput>;
  none?: InputMaybe<MediaWhereInput>;
  some?: InputMaybe<MediaWhereInput>;
};

export type MediaOrderByInput = {
  createdAt?: InputMaybe<OrderDirection>;
  embed?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  order?: InputMaybe<OrderDirection>;
  title?: InputMaybe<OrderDirection>;
  type?: InputMaybe<OrderDirection>;
  updatedAt?: InputMaybe<OrderDirection>;
  url?: InputMaybe<OrderDirection>;
};

export type MediaRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<MediaWhereUniqueInput>>;
  create?: InputMaybe<Array<MediaCreateInput>>;
};

export type MediaRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<MediaWhereUniqueInput>>;
  create?: InputMaybe<Array<MediaCreateInput>>;
  disconnect?: InputMaybe<Array<MediaWhereUniqueInput>>;
  set?: InputMaybe<Array<MediaWhereUniqueInput>>;
};

export type MediaRelateToOneForCreateInput = {
  connect?: InputMaybe<MediaWhereUniqueInput>;
  create?: InputMaybe<MediaCreateInput>;
};

export type MediaRelateToOneForUpdateInput = {
  connect?: InputMaybe<MediaWhereUniqueInput>;
  create?: InputMaybe<MediaCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MediaUpdateArgs = {
  data: MediaUpdateInput;
  where: MediaWhereUniqueInput;
};

export type MediaUpdateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  embed?: InputMaybe<Scalars['String']['input']>;
  image?: InputMaybe<ImageFieldInput>;
  order?: InputMaybe<Scalars['Int']['input']>;
  title?: InputMaybe<Scalars['String']['input']>;
  type?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  url?: InputMaybe<Scalars['String']['input']>;
};

export type MediaWhereInput = {
  AND?: InputMaybe<Array<MediaWhereInput>>;
  NOT?: InputMaybe<Array<MediaWhereInput>>;
  OR?: InputMaybe<Array<MediaWhereInput>>;
  createdAt?: InputMaybe<DateTimeNullableFilter>;
  embed?: InputMaybe<StringFilter>;
  id?: InputMaybe<IdFilter>;
  order?: InputMaybe<IntNullableFilter>;
  title?: InputMaybe<StringFilter>;
  type?: InputMaybe<StringFilter>;
  updatedAt?: InputMaybe<DateTimeNullableFilter>;
  url?: InputMaybe<StringFilter>;
};

export type MediaWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type Mutation = {
  __typename?: 'Mutation';
  authenticateUserWithPassword?: Maybe<UserAuthenticationWithPasswordResult>;
  createAuthor?: Maybe<Author>;
  createAuthors?: Maybe<Array<Maybe<Author>>>;
  createColour?: Maybe<Colour>;
  createColours?: Maybe<Array<Maybe<Colour>>>;
  createEvent?: Maybe<Event>;
  createEventType?: Maybe<EventType>;
  createEventTypes?: Maybe<Array<Maybe<EventType>>>;
  createEvents?: Maybe<Array<Maybe<Event>>>;
  createGlossaries?: Maybe<Array<Maybe<Glossary>>>;
  createGlossary?: Maybe<Glossary>;
  createGlossaryType?: Maybe<GlossaryType>;
  createGlossaryTypes?: Maybe<Array<Maybe<GlossaryType>>>;
  createInitialUser: UserAuthenticationWithPasswordSuccess;
  createLogo?: Maybe<Logo>;
  createLogos?: Maybe<Array<Maybe<Logo>>>;
  createMedia?: Maybe<Media>;
  createNarrative?: Maybe<Narrative>;
  createNarrativeCategories?: Maybe<Array<Maybe<NarrativeCategory>>>;
  createNarrativeCategory?: Maybe<NarrativeCategory>;
  createNarrativeTranslation?: Maybe<NarrativeTranslation>;
  createNarrativeTranslations?: Maybe<Array<Maybe<NarrativeTranslation>>>;
  createNarratives?: Maybe<Array<Maybe<Narrative>>>;
  createObject?: Maybe<Object>;
  createObjectType?: Maybe<ObjectType>;
  createObjectTypes?: Maybe<Array<Maybe<ObjectType>>>;
  createObjects?: Maybe<Array<Maybe<Object>>>;
  createOccupation?: Maybe<Occupation>;
  createOccupations?: Maybe<Array<Maybe<Occupation>>>;
  createOrganisation?: Maybe<Organisation>;
  createOrganisationType?: Maybe<OrganisationType>;
  createOrganisationTypes?: Maybe<Array<Maybe<OrganisationType>>>;
  createOrganisations?: Maybe<Array<Maybe<Organisation>>>;
  createPeople?: Maybe<Array<Maybe<Person>>>;
  createPerson?: Maybe<Person>;
  createReference?: Maybe<Reference>;
  createReferences?: Maybe<Array<Maybe<Reference>>>;
  createStaticContent?: Maybe<StaticContent>;
  createStaticContents?: Maybe<Array<Maybe<StaticContent>>>;
  createUser?: Maybe<User>;
  createUsers?: Maybe<Array<Maybe<User>>>;
  createmedias?: Maybe<Array<Maybe<Media>>>;
  deleteAuthor?: Maybe<Author>;
  deleteAuthors?: Maybe<Array<Maybe<Author>>>;
  deleteColour?: Maybe<Colour>;
  deleteColours?: Maybe<Array<Maybe<Colour>>>;
  deleteEvent?: Maybe<Event>;
  deleteEventType?: Maybe<EventType>;
  deleteEventTypes?: Maybe<Array<Maybe<EventType>>>;
  deleteEvents?: Maybe<Array<Maybe<Event>>>;
  deleteGlossaries?: Maybe<Array<Maybe<Glossary>>>;
  deleteGlossary?: Maybe<Glossary>;
  deleteGlossaryType?: Maybe<GlossaryType>;
  deleteGlossaryTypes?: Maybe<Array<Maybe<GlossaryType>>>;
  deleteLogo?: Maybe<Logo>;
  deleteLogos?: Maybe<Array<Maybe<Logo>>>;
  deleteMedia?: Maybe<Media>;
  deleteNarrative?: Maybe<Narrative>;
  deleteNarrativeCategories?: Maybe<Array<Maybe<NarrativeCategory>>>;
  deleteNarrativeCategory?: Maybe<NarrativeCategory>;
  deleteNarrativeTranslation?: Maybe<NarrativeTranslation>;
  deleteNarrativeTranslations?: Maybe<Array<Maybe<NarrativeTranslation>>>;
  deleteNarratives?: Maybe<Array<Maybe<Narrative>>>;
  deleteObject?: Maybe<Object>;
  deleteObjectType?: Maybe<ObjectType>;
  deleteObjectTypes?: Maybe<Array<Maybe<ObjectType>>>;
  deleteObjects?: Maybe<Array<Maybe<Object>>>;
  deleteOccupation?: Maybe<Occupation>;
  deleteOccupations?: Maybe<Array<Maybe<Occupation>>>;
  deleteOrganisation?: Maybe<Organisation>;
  deleteOrganisationType?: Maybe<OrganisationType>;
  deleteOrganisationTypes?: Maybe<Array<Maybe<OrganisationType>>>;
  deleteOrganisations?: Maybe<Array<Maybe<Organisation>>>;
  deletePeople?: Maybe<Array<Maybe<Person>>>;
  deletePerson?: Maybe<Person>;
  deleteReference?: Maybe<Reference>;
  deleteReferences?: Maybe<Array<Maybe<Reference>>>;
  deleteStaticContent?: Maybe<StaticContent>;
  deleteStaticContents?: Maybe<Array<Maybe<StaticContent>>>;
  deleteUser?: Maybe<User>;
  deleteUsers?: Maybe<Array<Maybe<User>>>;
  deletemedias?: Maybe<Array<Maybe<Media>>>;
  endSession: Scalars['Boolean']['output'];
  updateAuthor?: Maybe<Author>;
  updateAuthors?: Maybe<Array<Maybe<Author>>>;
  updateColour?: Maybe<Colour>;
  updateColours?: Maybe<Array<Maybe<Colour>>>;
  updateEvent?: Maybe<Event>;
  updateEventType?: Maybe<EventType>;
  updateEventTypes?: Maybe<Array<Maybe<EventType>>>;
  updateEvents?: Maybe<Array<Maybe<Event>>>;
  updateGlossaries?: Maybe<Array<Maybe<Glossary>>>;
  updateGlossary?: Maybe<Glossary>;
  updateGlossaryType?: Maybe<GlossaryType>;
  updateGlossaryTypes?: Maybe<Array<Maybe<GlossaryType>>>;
  updateLogo?: Maybe<Logo>;
  updateLogos?: Maybe<Array<Maybe<Logo>>>;
  updateMedia?: Maybe<Media>;
  updateNarrative?: Maybe<Narrative>;
  updateNarrativeCategories?: Maybe<Array<Maybe<NarrativeCategory>>>;
  updateNarrativeCategory?: Maybe<NarrativeCategory>;
  updateNarrativeTranslation?: Maybe<NarrativeTranslation>;
  updateNarrativeTranslations?: Maybe<Array<Maybe<NarrativeTranslation>>>;
  updateNarratives?: Maybe<Array<Maybe<Narrative>>>;
  updateObject?: Maybe<Object>;
  updateObjectType?: Maybe<ObjectType>;
  updateObjectTypes?: Maybe<Array<Maybe<ObjectType>>>;
  updateObjects?: Maybe<Array<Maybe<Object>>>;
  updateOccupation?: Maybe<Occupation>;
  updateOccupations?: Maybe<Array<Maybe<Occupation>>>;
  updateOrganisation?: Maybe<Organisation>;
  updateOrganisationType?: Maybe<OrganisationType>;
  updateOrganisationTypes?: Maybe<Array<Maybe<OrganisationType>>>;
  updateOrganisations?: Maybe<Array<Maybe<Organisation>>>;
  updatePeople?: Maybe<Array<Maybe<Person>>>;
  updatePerson?: Maybe<Person>;
  updateReference?: Maybe<Reference>;
  updateReferences?: Maybe<Array<Maybe<Reference>>>;
  updateStaticContent?: Maybe<StaticContent>;
  updateStaticContents?: Maybe<Array<Maybe<StaticContent>>>;
  updateUser?: Maybe<User>;
  updateUsers?: Maybe<Array<Maybe<User>>>;
  updatemedias?: Maybe<Array<Maybe<Media>>>;
};


export type MutationAuthenticateUserWithPasswordArgs = {
  name: Scalars['String']['input'];
  password: Scalars['String']['input'];
};


export type MutationCreateAuthorArgs = {
  data: AuthorCreateInput;
};


export type MutationCreateAuthorsArgs = {
  data: Array<AuthorCreateInput>;
};


export type MutationCreateColourArgs = {
  data: ColourCreateInput;
};


export type MutationCreateColoursArgs = {
  data: Array<ColourCreateInput>;
};


export type MutationCreateEventArgs = {
  data: EventCreateInput;
};


export type MutationCreateEventTypeArgs = {
  data: EventTypeCreateInput;
};


export type MutationCreateEventTypesArgs = {
  data: Array<EventTypeCreateInput>;
};


export type MutationCreateEventsArgs = {
  data: Array<EventCreateInput>;
};


export type MutationCreateGlossariesArgs = {
  data: Array<GlossaryCreateInput>;
};


export type MutationCreateGlossaryArgs = {
  data: GlossaryCreateInput;
};


export type MutationCreateGlossaryTypeArgs = {
  data: GlossaryTypeCreateInput;
};


export type MutationCreateGlossaryTypesArgs = {
  data: Array<GlossaryTypeCreateInput>;
};


export type MutationCreateInitialUserArgs = {
  data: CreateInitialUserInput;
};


export type MutationCreateLogoArgs = {
  data: LogoCreateInput;
};


export type MutationCreateLogosArgs = {
  data: Array<LogoCreateInput>;
};


export type MutationCreateMediaArgs = {
  data: MediaCreateInput;
};


export type MutationCreateNarrativeArgs = {
  data: NarrativeCreateInput;
};


export type MutationCreateNarrativeCategoriesArgs = {
  data: Array<NarrativeCategoryCreateInput>;
};


export type MutationCreateNarrativeCategoryArgs = {
  data: NarrativeCategoryCreateInput;
};


export type MutationCreateNarrativeTranslationArgs = {
  data: NarrativeTranslationCreateInput;
};


export type MutationCreateNarrativeTranslationsArgs = {
  data: Array<NarrativeTranslationCreateInput>;
};


export type MutationCreateNarrativesArgs = {
  data: Array<NarrativeCreateInput>;
};


export type MutationCreateObjectArgs = {
  data: ObjectCreateInput;
};


export type MutationCreateObjectTypeArgs = {
  data: ObjectTypeCreateInput;
};


export type MutationCreateObjectTypesArgs = {
  data: Array<ObjectTypeCreateInput>;
};


export type MutationCreateObjectsArgs = {
  data: Array<ObjectCreateInput>;
};


export type MutationCreateOccupationArgs = {
  data: OccupationCreateInput;
};


export type MutationCreateOccupationsArgs = {
  data: Array<OccupationCreateInput>;
};


export type MutationCreateOrganisationArgs = {
  data: OrganisationCreateInput;
};


export type MutationCreateOrganisationTypeArgs = {
  data: OrganisationTypeCreateInput;
};


export type MutationCreateOrganisationTypesArgs = {
  data: Array<OrganisationTypeCreateInput>;
};


export type MutationCreateOrganisationsArgs = {
  data: Array<OrganisationCreateInput>;
};


export type MutationCreatePeopleArgs = {
  data: Array<PersonCreateInput>;
};


export type MutationCreatePersonArgs = {
  data: PersonCreateInput;
};


export type MutationCreateReferenceArgs = {
  data: ReferenceCreateInput;
};


export type MutationCreateReferencesArgs = {
  data: Array<ReferenceCreateInput>;
};


export type MutationCreateStaticContentArgs = {
  data: StaticContentCreateInput;
};


export type MutationCreateStaticContentsArgs = {
  data: Array<StaticContentCreateInput>;
};


export type MutationCreateUserArgs = {
  data: UserCreateInput;
};


export type MutationCreateUsersArgs = {
  data: Array<UserCreateInput>;
};


export type MutationCreatemediasArgs = {
  data: Array<MediaCreateInput>;
};


export type MutationDeleteAuthorArgs = {
  where: AuthorWhereUniqueInput;
};


export type MutationDeleteAuthorsArgs = {
  where: Array<AuthorWhereUniqueInput>;
};


export type MutationDeleteColourArgs = {
  where: ColourWhereUniqueInput;
};


export type MutationDeleteColoursArgs = {
  where: Array<ColourWhereUniqueInput>;
};


export type MutationDeleteEventArgs = {
  where: EventWhereUniqueInput;
};


export type MutationDeleteEventTypeArgs = {
  where: EventTypeWhereUniqueInput;
};


export type MutationDeleteEventTypesArgs = {
  where: Array<EventTypeWhereUniqueInput>;
};


export type MutationDeleteEventsArgs = {
  where: Array<EventWhereUniqueInput>;
};


export type MutationDeleteGlossariesArgs = {
  where: Array<GlossaryWhereUniqueInput>;
};


export type MutationDeleteGlossaryArgs = {
  where: GlossaryWhereUniqueInput;
};


export type MutationDeleteGlossaryTypeArgs = {
  where: GlossaryTypeWhereUniqueInput;
};


export type MutationDeleteGlossaryTypesArgs = {
  where: Array<GlossaryTypeWhereUniqueInput>;
};


export type MutationDeleteLogoArgs = {
  where: LogoWhereUniqueInput;
};


export type MutationDeleteLogosArgs = {
  where: Array<LogoWhereUniqueInput>;
};


export type MutationDeleteMediaArgs = {
  where: MediaWhereUniqueInput;
};


export type MutationDeleteNarrativeArgs = {
  where: NarrativeWhereUniqueInput;
};


export type MutationDeleteNarrativeCategoriesArgs = {
  where: Array<NarrativeCategoryWhereUniqueInput>;
};


export type MutationDeleteNarrativeCategoryArgs = {
  where: NarrativeCategoryWhereUniqueInput;
};


export type MutationDeleteNarrativeTranslationArgs = {
  where: NarrativeTranslationWhereUniqueInput;
};


export type MutationDeleteNarrativeTranslationsArgs = {
  where: Array<NarrativeTranslationWhereUniqueInput>;
};


export type MutationDeleteNarrativesArgs = {
  where: Array<NarrativeWhereUniqueInput>;
};


export type MutationDeleteObjectArgs = {
  where: ObjectWhereUniqueInput;
};


export type MutationDeleteObjectTypeArgs = {
  where: ObjectTypeWhereUniqueInput;
};


export type MutationDeleteObjectTypesArgs = {
  where: Array<ObjectTypeWhereUniqueInput>;
};


export type MutationDeleteObjectsArgs = {
  where: Array<ObjectWhereUniqueInput>;
};


export type MutationDeleteOccupationArgs = {
  where: OccupationWhereUniqueInput;
};


export type MutationDeleteOccupationsArgs = {
  where: Array<OccupationWhereUniqueInput>;
};


export type MutationDeleteOrganisationArgs = {
  where: OrganisationWhereUniqueInput;
};


export type MutationDeleteOrganisationTypeArgs = {
  where: OrganisationTypeWhereUniqueInput;
};


export type MutationDeleteOrganisationTypesArgs = {
  where: Array<OrganisationTypeWhereUniqueInput>;
};


export type MutationDeleteOrganisationsArgs = {
  where: Array<OrganisationWhereUniqueInput>;
};


export type MutationDeletePeopleArgs = {
  where: Array<PersonWhereUniqueInput>;
};


export type MutationDeletePersonArgs = {
  where: PersonWhereUniqueInput;
};


export type MutationDeleteReferenceArgs = {
  where: ReferenceWhereUniqueInput;
};


export type MutationDeleteReferencesArgs = {
  where: Array<ReferenceWhereUniqueInput>;
};


export type MutationDeleteStaticContentArgs = {
  where?: StaticContentWhereUniqueInput;
};


export type MutationDeleteStaticContentsArgs = {
  where: Array<StaticContentWhereUniqueInput>;
};


export type MutationDeleteUserArgs = {
  where: UserWhereUniqueInput;
};


export type MutationDeleteUsersArgs = {
  where: Array<UserWhereUniqueInput>;
};


export type MutationDeletemediasArgs = {
  where: Array<MediaWhereUniqueInput>;
};


export type MutationUpdateAuthorArgs = {
  data: AuthorUpdateInput;
  where: AuthorWhereUniqueInput;
};


export type MutationUpdateAuthorsArgs = {
  data: Array<AuthorUpdateArgs>;
};


export type MutationUpdateColourArgs = {
  data: ColourUpdateInput;
  where: ColourWhereUniqueInput;
};


export type MutationUpdateColoursArgs = {
  data: Array<ColourUpdateArgs>;
};


export type MutationUpdateEventArgs = {
  data: EventUpdateInput;
  where: EventWhereUniqueInput;
};


export type MutationUpdateEventTypeArgs = {
  data: EventTypeUpdateInput;
  where: EventTypeWhereUniqueInput;
};


export type MutationUpdateEventTypesArgs = {
  data: Array<EventTypeUpdateArgs>;
};


export type MutationUpdateEventsArgs = {
  data: Array<EventUpdateArgs>;
};


export type MutationUpdateGlossariesArgs = {
  data: Array<GlossaryUpdateArgs>;
};


export type MutationUpdateGlossaryArgs = {
  data: GlossaryUpdateInput;
  where: GlossaryWhereUniqueInput;
};


export type MutationUpdateGlossaryTypeArgs = {
  data: GlossaryTypeUpdateInput;
  where: GlossaryTypeWhereUniqueInput;
};


export type MutationUpdateGlossaryTypesArgs = {
  data: Array<GlossaryTypeUpdateArgs>;
};


export type MutationUpdateLogoArgs = {
  data: LogoUpdateInput;
  where: LogoWhereUniqueInput;
};


export type MutationUpdateLogosArgs = {
  data: Array<LogoUpdateArgs>;
};


export type MutationUpdateMediaArgs = {
  data: MediaUpdateInput;
  where: MediaWhereUniqueInput;
};


export type MutationUpdateNarrativeArgs = {
  data: NarrativeUpdateInput;
  where: NarrativeWhereUniqueInput;
};


export type MutationUpdateNarrativeCategoriesArgs = {
  data: Array<NarrativeCategoryUpdateArgs>;
};


export type MutationUpdateNarrativeCategoryArgs = {
  data: NarrativeCategoryUpdateInput;
  where: NarrativeCategoryWhereUniqueInput;
};


export type MutationUpdateNarrativeTranslationArgs = {
  data: NarrativeTranslationUpdateInput;
  where: NarrativeTranslationWhereUniqueInput;
};


export type MutationUpdateNarrativeTranslationsArgs = {
  data: Array<NarrativeTranslationUpdateArgs>;
};


export type MutationUpdateNarrativesArgs = {
  data: Array<NarrativeUpdateArgs>;
};


export type MutationUpdateObjectArgs = {
  data: ObjectUpdateInput;
  where: ObjectWhereUniqueInput;
};


export type MutationUpdateObjectTypeArgs = {
  data: ObjectTypeUpdateInput;
  where: ObjectTypeWhereUniqueInput;
};


export type MutationUpdateObjectTypesArgs = {
  data: Array<ObjectTypeUpdateArgs>;
};


export type MutationUpdateObjectsArgs = {
  data: Array<ObjectUpdateArgs>;
};


export type MutationUpdateOccupationArgs = {
  data: OccupationUpdateInput;
  where: OccupationWhereUniqueInput;
};


export type MutationUpdateOccupationsArgs = {
  data: Array<OccupationUpdateArgs>;
};


export type MutationUpdateOrganisationArgs = {
  data: OrganisationUpdateInput;
  where: OrganisationWhereUniqueInput;
};


export type MutationUpdateOrganisationTypeArgs = {
  data: OrganisationTypeUpdateInput;
  where: OrganisationTypeWhereUniqueInput;
};


export type MutationUpdateOrganisationTypesArgs = {
  data: Array<OrganisationTypeUpdateArgs>;
};


export type MutationUpdateOrganisationsArgs = {
  data: Array<OrganisationUpdateArgs>;
};


export type MutationUpdatePeopleArgs = {
  data: Array<PersonUpdateArgs>;
};


export type MutationUpdatePersonArgs = {
  data: PersonUpdateInput;
  where: PersonWhereUniqueInput;
};


export type MutationUpdateReferenceArgs = {
  data: ReferenceUpdateInput;
  where: ReferenceWhereUniqueInput;
};


export type MutationUpdateReferencesArgs = {
  data: Array<ReferenceUpdateArgs>;
};


export type MutationUpdateStaticContentArgs = {
  data: StaticContentUpdateInput;
  where?: StaticContentWhereUniqueInput;
};


export type MutationUpdateStaticContentsArgs = {
  data: Array<StaticContentUpdateArgs>;
};


export type MutationUpdateUserArgs = {
  data: UserUpdateInput;
  where: UserWhereUniqueInput;
};


export type MutationUpdateUsersArgs = {
  data: Array<UserUpdateArgs>;
};


export type MutationUpdatemediasArgs = {
  data: Array<MediaUpdateArgs>;
};

export type Narrative = {
  __typename?: 'Narrative';
  authors?: Maybe<Array<Author>>;
  authorsCount?: Maybe<Scalars['Int']['output']>;
  category?: Maybe<NarrativeCategory>;
  colours?: Maybe<Array<Colour>>;
  coloursCount?: Maybe<Scalars['Int']['output']>;
  content?: Maybe<Narrative_Content_Document>;
  cover?: Maybe<Media>;
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  date?: Maybe<Scalars['String']['output']>;
  events?: Maybe<Array<Event>>;
  eventsCount?: Maybe<Scalars['Int']['output']>;
  extraReferences?: Maybe<Array<Reference>>;
  extraReferencesCount?: Maybe<Scalars['Int']['output']>;
  glossary?: Maybe<Array<Glossary>>;
  glossaryCount?: Maybe<Scalars['Int']['output']>;
  id: Scalars['ID']['output'];
  language?: Maybe<Scalars['String']['output']>;
  objects?: Maybe<Array<Object>>;
  objectsCount?: Maybe<Scalars['Int']['output']>;
  organisations?: Maybe<Array<Organisation>>;
  organisationsCount?: Maybe<Scalars['Int']['output']>;
  people?: Maybe<Array<Person>>;
  peopleCount?: Maybe<Scalars['Int']['output']>;
  references?: Maybe<Array<Reference>>;
  referencesCount?: Maybe<Scalars['Int']['output']>;
  selectForHome?: Maybe<Scalars['Boolean']['output']>;
  slug?: Maybe<Scalars['String']['output']>;
  status?: Maybe<Scalars['String']['output']>;
  title?: Maybe<Scalars['String']['output']>;
  translations?: Maybe<Array<NarrativeTranslation>>;
  translationsCount?: Maybe<Scalars['Int']['output']>;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
};


export type NarrativeAuthorsArgs = {
  cursor?: InputMaybe<AuthorWhereUniqueInput>;
  orderBy?: Array<AuthorOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: AuthorWhereInput;
};


export type NarrativeAuthorsCountArgs = {
  where?: AuthorWhereInput;
};


export type NarrativeColoursArgs = {
  cursor?: InputMaybe<ColourWhereUniqueInput>;
  orderBy?: Array<ColourOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: ColourWhereInput;
};


export type NarrativeColoursCountArgs = {
  where?: ColourWhereInput;
};


export type NarrativeEventsArgs = {
  cursor?: InputMaybe<EventWhereUniqueInput>;
  orderBy?: Array<EventOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: EventWhereInput;
};


export type NarrativeEventsCountArgs = {
  where?: EventWhereInput;
};


export type NarrativeExtraReferencesArgs = {
  cursor?: InputMaybe<ReferenceWhereUniqueInput>;
  orderBy?: Array<ReferenceOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: ReferenceWhereInput;
};


export type NarrativeExtraReferencesCountArgs = {
  where?: ReferenceWhereInput;
};


export type NarrativeGlossaryArgs = {
  cursor?: InputMaybe<GlossaryWhereUniqueInput>;
  orderBy?: Array<GlossaryOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: GlossaryWhereInput;
};


export type NarrativeGlossaryCountArgs = {
  where?: GlossaryWhereInput;
};


export type NarrativeObjectsArgs = {
  cursor?: InputMaybe<ObjectWhereUniqueInput>;
  orderBy?: Array<ObjectOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: ObjectWhereInput;
};


export type NarrativeObjectsCountArgs = {
  where?: ObjectWhereInput;
};


export type NarrativeOrganisationsArgs = {
  cursor?: InputMaybe<OrganisationWhereUniqueInput>;
  orderBy?: Array<OrganisationOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: OrganisationWhereInput;
};


export type NarrativeOrganisationsCountArgs = {
  where?: OrganisationWhereInput;
};


export type NarrativePeopleArgs = {
  cursor?: InputMaybe<PersonWhereUniqueInput>;
  orderBy?: Array<PersonOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: PersonWhereInput;
};


export type NarrativePeopleCountArgs = {
  where?: PersonWhereInput;
};


export type NarrativeReferencesArgs = {
  cursor?: InputMaybe<ReferenceWhereUniqueInput>;
  orderBy?: Array<ReferenceOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: ReferenceWhereInput;
};


export type NarrativeReferencesCountArgs = {
  where?: ReferenceWhereInput;
};


export type NarrativeTranslationsArgs = {
  cursor?: InputMaybe<NarrativeTranslationWhereUniqueInput>;
  orderBy?: Array<NarrativeTranslationOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: NarrativeTranslationWhereInput;
};


export type NarrativeTranslationsCountArgs = {
  where?: NarrativeTranslationWhereInput;
};

export type NarrativeCategory = {
  __typename?: 'NarrativeCategory';
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  description?: Maybe<NarrativeCategory_Description_Document>;
  id: Scalars['ID']['output'];
  narratives?: Maybe<Array<Narrative>>;
  narrativesCount?: Maybe<Scalars['Int']['output']>;
  slug?: Maybe<Scalars['String']['output']>;
  title?: Maybe<Scalars['String']['output']>;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
};


export type NarrativeCategoryNarrativesArgs = {
  cursor?: InputMaybe<NarrativeWhereUniqueInput>;
  orderBy?: Array<NarrativeOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: NarrativeWhereInput;
};


export type NarrativeCategoryNarrativesCountArgs = {
  where?: NarrativeWhereInput;
};

export type NarrativeCategoryCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  description?: InputMaybe<Scalars['JSON']['input']>;
  narratives?: InputMaybe<NarrativeRelateToManyForCreateInput>;
  slug?: InputMaybe<Scalars['String']['input']>;
  title?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type NarrativeCategoryOrderByInput = {
  createdAt?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  slug?: InputMaybe<OrderDirection>;
  title?: InputMaybe<OrderDirection>;
  updatedAt?: InputMaybe<OrderDirection>;
};

export type NarrativeCategoryRelateToOneForCreateInput = {
  connect?: InputMaybe<NarrativeCategoryWhereUniqueInput>;
  create?: InputMaybe<NarrativeCategoryCreateInput>;
};

export type NarrativeCategoryRelateToOneForUpdateInput = {
  connect?: InputMaybe<NarrativeCategoryWhereUniqueInput>;
  create?: InputMaybe<NarrativeCategoryCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

export type NarrativeCategoryUpdateArgs = {
  data: NarrativeCategoryUpdateInput;
  where: NarrativeCategoryWhereUniqueInput;
};

export type NarrativeCategoryUpdateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  description?: InputMaybe<Scalars['JSON']['input']>;
  narratives?: InputMaybe<NarrativeRelateToManyForUpdateInput>;
  slug?: InputMaybe<Scalars['String']['input']>;
  title?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type NarrativeCategoryWhereInput = {
  AND?: InputMaybe<Array<NarrativeCategoryWhereInput>>;
  NOT?: InputMaybe<Array<NarrativeCategoryWhereInput>>;
  OR?: InputMaybe<Array<NarrativeCategoryWhereInput>>;
  createdAt?: InputMaybe<DateTimeNullableFilter>;
  id?: InputMaybe<IdFilter>;
  narratives?: InputMaybe<NarrativeManyRelationFilter>;
  slug?: InputMaybe<StringFilter>;
  title?: InputMaybe<StringFilter>;
  updatedAt?: InputMaybe<DateTimeNullableFilter>;
};

export type NarrativeCategoryWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  slug?: InputMaybe<Scalars['String']['input']>;
};

export type NarrativeCategory_Description_Document = {
  __typename?: 'NarrativeCategory_description_Document';
  document: Scalars['JSON']['output'];
};


export type NarrativeCategory_Description_DocumentDocumentArgs = {
  hydrateRelationships?: Scalars['Boolean']['input'];
};

export type NarrativeCreateInput = {
  authors?: InputMaybe<AuthorRelateToManyForCreateInput>;
  category?: InputMaybe<NarrativeCategoryRelateToOneForCreateInput>;
  colours?: InputMaybe<ColourRelateToManyForCreateInput>;
  content?: InputMaybe<Scalars['JSON']['input']>;
  cover?: InputMaybe<MediaRelateToOneForCreateInput>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  date?: InputMaybe<Scalars['String']['input']>;
  events?: InputMaybe<EventRelateToManyForCreateInput>;
  extraReferences?: InputMaybe<ReferenceRelateToManyForCreateInput>;
  glossary?: InputMaybe<GlossaryRelateToManyForCreateInput>;
  language?: InputMaybe<Scalars['String']['input']>;
  objects?: InputMaybe<ObjectRelateToManyForCreateInput>;
  organisations?: InputMaybe<OrganisationRelateToManyForCreateInput>;
  people?: InputMaybe<PersonRelateToManyForCreateInput>;
  references?: InputMaybe<ReferenceRelateToManyForCreateInput>;
  selectForHome?: InputMaybe<Scalars['Boolean']['input']>;
  slug?: InputMaybe<Scalars['String']['input']>;
  status?: InputMaybe<Scalars['String']['input']>;
  title?: InputMaybe<Scalars['String']['input']>;
  translations?: InputMaybe<NarrativeTranslationRelateToManyForCreateInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type NarrativeManyRelationFilter = {
  every?: InputMaybe<NarrativeWhereInput>;
  none?: InputMaybe<NarrativeWhereInput>;
  some?: InputMaybe<NarrativeWhereInput>;
};

export type NarrativeOrderByInput = {
  createdAt?: InputMaybe<OrderDirection>;
  date?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  language?: InputMaybe<OrderDirection>;
  selectForHome?: InputMaybe<OrderDirection>;
  slug?: InputMaybe<OrderDirection>;
  status?: InputMaybe<OrderDirection>;
  title?: InputMaybe<OrderDirection>;
  updatedAt?: InputMaybe<OrderDirection>;
};

export type NarrativeRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<NarrativeWhereUniqueInput>>;
  create?: InputMaybe<Array<NarrativeCreateInput>>;
};

export type NarrativeRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<NarrativeWhereUniqueInput>>;
  create?: InputMaybe<Array<NarrativeCreateInput>>;
  disconnect?: InputMaybe<Array<NarrativeWhereUniqueInput>>;
  set?: InputMaybe<Array<NarrativeWhereUniqueInput>>;
};

export type NarrativeRelateToOneForCreateInput = {
  connect?: InputMaybe<NarrativeWhereUniqueInput>;
  create?: InputMaybe<NarrativeCreateInput>;
};

export type NarrativeRelateToOneForUpdateInput = {
  connect?: InputMaybe<NarrativeWhereUniqueInput>;
  create?: InputMaybe<NarrativeCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

export type NarrativeTranslation = {
  __typename?: 'NarrativeTranslation';
  content?: Maybe<NarrativeTranslation_Content_Document>;
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  id: Scalars['ID']['output'];
  language?: Maybe<Scalars['String']['output']>;
  selectForHome?: Maybe<Scalars['Boolean']['output']>;
  slug?: Maybe<Scalars['String']['output']>;
  status?: Maybe<Scalars['String']['output']>;
  title?: Maybe<Scalars['String']['output']>;
  translatedNarrative?: Maybe<Narrative>;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
};

export type NarrativeTranslationCreateInput = {
  content?: InputMaybe<Scalars['JSON']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  language?: InputMaybe<Scalars['String']['input']>;
  selectForHome?: InputMaybe<Scalars['Boolean']['input']>;
  slug?: InputMaybe<Scalars['String']['input']>;
  status?: InputMaybe<Scalars['String']['input']>;
  title?: InputMaybe<Scalars['String']['input']>;
  translatedNarrative?: InputMaybe<NarrativeRelateToOneForCreateInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type NarrativeTranslationManyRelationFilter = {
  every?: InputMaybe<NarrativeTranslationWhereInput>;
  none?: InputMaybe<NarrativeTranslationWhereInput>;
  some?: InputMaybe<NarrativeTranslationWhereInput>;
};

export type NarrativeTranslationOrderByInput = {
  createdAt?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  language?: InputMaybe<OrderDirection>;
  selectForHome?: InputMaybe<OrderDirection>;
  slug?: InputMaybe<OrderDirection>;
  status?: InputMaybe<OrderDirection>;
  title?: InputMaybe<OrderDirection>;
  updatedAt?: InputMaybe<OrderDirection>;
};

export type NarrativeTranslationRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<NarrativeTranslationWhereUniqueInput>>;
  create?: InputMaybe<Array<NarrativeTranslationCreateInput>>;
};

export type NarrativeTranslationRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<NarrativeTranslationWhereUniqueInput>>;
  create?: InputMaybe<Array<NarrativeTranslationCreateInput>>;
  disconnect?: InputMaybe<Array<NarrativeTranslationWhereUniqueInput>>;
  set?: InputMaybe<Array<NarrativeTranslationWhereUniqueInput>>;
};

export type NarrativeTranslationUpdateArgs = {
  data: NarrativeTranslationUpdateInput;
  where: NarrativeTranslationWhereUniqueInput;
};

export type NarrativeTranslationUpdateInput = {
  content?: InputMaybe<Scalars['JSON']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  language?: InputMaybe<Scalars['String']['input']>;
  selectForHome?: InputMaybe<Scalars['Boolean']['input']>;
  slug?: InputMaybe<Scalars['String']['input']>;
  status?: InputMaybe<Scalars['String']['input']>;
  title?: InputMaybe<Scalars['String']['input']>;
  translatedNarrative?: InputMaybe<NarrativeRelateToOneForUpdateInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type NarrativeTranslationWhereInput = {
  AND?: InputMaybe<Array<NarrativeTranslationWhereInput>>;
  NOT?: InputMaybe<Array<NarrativeTranslationWhereInput>>;
  OR?: InputMaybe<Array<NarrativeTranslationWhereInput>>;
  createdAt?: InputMaybe<DateTimeNullableFilter>;
  id?: InputMaybe<IdFilter>;
  language?: InputMaybe<StringNullableFilter>;
  selectForHome?: InputMaybe<BooleanFilter>;
  slug?: InputMaybe<StringFilter>;
  status?: InputMaybe<StringNullableFilter>;
  title?: InputMaybe<StringFilter>;
  translatedNarrative?: InputMaybe<NarrativeWhereInput>;
  updatedAt?: InputMaybe<DateTimeNullableFilter>;
};

export type NarrativeTranslationWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  slug?: InputMaybe<Scalars['String']['input']>;
  title?: InputMaybe<Scalars['String']['input']>;
};

export type NarrativeTranslation_Content_Document = {
  __typename?: 'NarrativeTranslation_content_Document';
  document: Scalars['JSON']['output'];
};


export type NarrativeTranslation_Content_DocumentDocumentArgs = {
  hydrateRelationships?: Scalars['Boolean']['input'];
};

export type NarrativeUpdateArgs = {
  data: NarrativeUpdateInput;
  where: NarrativeWhereUniqueInput;
};

export type NarrativeUpdateInput = {
  authors?: InputMaybe<AuthorRelateToManyForUpdateInput>;
  category?: InputMaybe<NarrativeCategoryRelateToOneForUpdateInput>;
  colours?: InputMaybe<ColourRelateToManyForUpdateInput>;
  content?: InputMaybe<Scalars['JSON']['input']>;
  cover?: InputMaybe<MediaRelateToOneForUpdateInput>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  date?: InputMaybe<Scalars['String']['input']>;
  events?: InputMaybe<EventRelateToManyForUpdateInput>;
  extraReferences?: InputMaybe<ReferenceRelateToManyForUpdateInput>;
  glossary?: InputMaybe<GlossaryRelateToManyForUpdateInput>;
  language?: InputMaybe<Scalars['String']['input']>;
  objects?: InputMaybe<ObjectRelateToManyForUpdateInput>;
  organisations?: InputMaybe<OrganisationRelateToManyForUpdateInput>;
  people?: InputMaybe<PersonRelateToManyForUpdateInput>;
  references?: InputMaybe<ReferenceRelateToManyForUpdateInput>;
  selectForHome?: InputMaybe<Scalars['Boolean']['input']>;
  slug?: InputMaybe<Scalars['String']['input']>;
  status?: InputMaybe<Scalars['String']['input']>;
  title?: InputMaybe<Scalars['String']['input']>;
  translations?: InputMaybe<NarrativeTranslationRelateToManyForUpdateInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type NarrativeWhereInput = {
  AND?: InputMaybe<Array<NarrativeWhereInput>>;
  NOT?: InputMaybe<Array<NarrativeWhereInput>>;
  OR?: InputMaybe<Array<NarrativeWhereInput>>;
  authors?: InputMaybe<AuthorManyRelationFilter>;
  category?: InputMaybe<NarrativeCategoryWhereInput>;
  colours?: InputMaybe<ColourManyRelationFilter>;
  cover?: InputMaybe<MediaWhereInput>;
  createdAt?: InputMaybe<DateTimeNullableFilter>;
  date?: InputMaybe<StringNullableFilter>;
  events?: InputMaybe<EventManyRelationFilter>;
  extraReferences?: InputMaybe<ReferenceManyRelationFilter>;
  glossary?: InputMaybe<GlossaryManyRelationFilter>;
  id?: InputMaybe<IdFilter>;
  language?: InputMaybe<StringNullableFilter>;
  objects?: InputMaybe<ObjectManyRelationFilter>;
  organisations?: InputMaybe<OrganisationManyRelationFilter>;
  people?: InputMaybe<PersonManyRelationFilter>;
  references?: InputMaybe<ReferenceManyRelationFilter>;
  selectForHome?: InputMaybe<BooleanFilter>;
  slug?: InputMaybe<StringFilter>;
  status?: InputMaybe<StringNullableFilter>;
  title?: InputMaybe<StringFilter>;
  translations?: InputMaybe<NarrativeTranslationManyRelationFilter>;
  updatedAt?: InputMaybe<DateTimeNullableFilter>;
};

export type NarrativeWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  slug?: InputMaybe<Scalars['String']['input']>;
  title?: InputMaybe<Scalars['String']['input']>;
};

export type Narrative_Content_Document = {
  __typename?: 'Narrative_content_Document';
  document: Scalars['JSON']['output'];
};


export type Narrative_Content_DocumentDocumentArgs = {
  hydrateRelationships?: Scalars['Boolean']['input'];
};

export type NestedStringFilter = {
  contains?: InputMaybe<Scalars['String']['input']>;
  endsWith?: InputMaybe<Scalars['String']['input']>;
  equals?: InputMaybe<Scalars['String']['input']>;
  gt?: InputMaybe<Scalars['String']['input']>;
  gte?: InputMaybe<Scalars['String']['input']>;
  in?: InputMaybe<Array<Scalars['String']['input']>>;
  lt?: InputMaybe<Scalars['String']['input']>;
  lte?: InputMaybe<Scalars['String']['input']>;
  not?: InputMaybe<NestedStringFilter>;
  notIn?: InputMaybe<Array<Scalars['String']['input']>>;
  startsWith?: InputMaybe<Scalars['String']['input']>;
};

export type Object = {
  __typename?: 'Object';
  author?: Maybe<Scalars['String']['output']>;
  colours?: Maybe<Array<Colour>>;
  coloursCount?: Maybe<Scalars['Int']['output']>;
  cover?: Maybe<Media>;
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  date?: Maybe<Scalars['String']['output']>;
  description?: Maybe<Object_Description_Document>;
  digitalizedByChromotope?: Maybe<Scalars['Boolean']['output']>;
  dimensions?: Maybe<Scalars['String']['output']>;
  id: Scalars['ID']['output'];
  label?: Maybe<Scalars['String']['output']>;
  location?: Maybe<Scalars['String']['output']>;
  medias?: Maybe<Array<Media>>;
  mediasCount?: Maybe<Scalars['Int']['output']>;
  medium?: Maybe<Scalars['String']['output']>;
  narratives?: Maybe<Array<Narrative>>;
  narrativesCount?: Maybe<Scalars['Int']['output']>;
  reference?: Maybe<Reference>;
  slug?: Maybe<Scalars['String']['output']>;
  type?: Maybe<ObjectType>;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
};


export type ObjectColoursArgs = {
  cursor?: InputMaybe<ColourWhereUniqueInput>;
  orderBy?: Array<ColourOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: ColourWhereInput;
};


export type ObjectColoursCountArgs = {
  where?: ColourWhereInput;
};


export type ObjectMediasArgs = {
  cursor?: InputMaybe<MediaWhereUniqueInput>;
  orderBy?: Array<MediaOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: MediaWhereInput;
};


export type ObjectMediasCountArgs = {
  where?: MediaWhereInput;
};


export type ObjectNarrativesArgs = {
  cursor?: InputMaybe<NarrativeWhereUniqueInput>;
  orderBy?: Array<NarrativeOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: NarrativeWhereInput;
};


export type ObjectNarrativesCountArgs = {
  where?: NarrativeWhereInput;
};

export type ObjectCreateInput = {
  author?: InputMaybe<Scalars['String']['input']>;
  colours?: InputMaybe<ColourRelateToManyForCreateInput>;
  cover?: InputMaybe<MediaRelateToOneForCreateInput>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  date?: InputMaybe<Scalars['String']['input']>;
  description?: InputMaybe<Scalars['JSON']['input']>;
  digitalizedByChromotope?: InputMaybe<Scalars['Boolean']['input']>;
  dimensions?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  location?: InputMaybe<Scalars['String']['input']>;
  medias?: InputMaybe<MediaRelateToManyForCreateInput>;
  medium?: InputMaybe<Scalars['String']['input']>;
  narratives?: InputMaybe<NarrativeRelateToManyForCreateInput>;
  reference?: InputMaybe<ReferenceRelateToOneForCreateInput>;
  slug?: InputMaybe<Scalars['String']['input']>;
  type?: InputMaybe<ObjectTypeRelateToOneForCreateInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type ObjectManyRelationFilter = {
  every?: InputMaybe<ObjectWhereInput>;
  none?: InputMaybe<ObjectWhereInput>;
  some?: InputMaybe<ObjectWhereInput>;
};

export type ObjectOrderByInput = {
  author?: InputMaybe<OrderDirection>;
  createdAt?: InputMaybe<OrderDirection>;
  date?: InputMaybe<OrderDirection>;
  digitalizedByChromotope?: InputMaybe<OrderDirection>;
  dimensions?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  label?: InputMaybe<OrderDirection>;
  location?: InputMaybe<OrderDirection>;
  medium?: InputMaybe<OrderDirection>;
  slug?: InputMaybe<OrderDirection>;
  updatedAt?: InputMaybe<OrderDirection>;
};

export type ObjectRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<ObjectWhereUniqueInput>>;
  create?: InputMaybe<Array<ObjectCreateInput>>;
};

export type ObjectRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<ObjectWhereUniqueInput>>;
  create?: InputMaybe<Array<ObjectCreateInput>>;
  disconnect?: InputMaybe<Array<ObjectWhereUniqueInput>>;
  set?: InputMaybe<Array<ObjectWhereUniqueInput>>;
};

export type ObjectType = {
  __typename?: 'ObjectType';
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  id: Scalars['ID']['output'];
  label?: Maybe<Scalars['String']['output']>;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
};

export type ObjectTypeCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type ObjectTypeOrderByInput = {
  createdAt?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  label?: InputMaybe<OrderDirection>;
  updatedAt?: InputMaybe<OrderDirection>;
};

export type ObjectTypeRelateToOneForCreateInput = {
  connect?: InputMaybe<ObjectTypeWhereUniqueInput>;
  create?: InputMaybe<ObjectTypeCreateInput>;
};

export type ObjectTypeRelateToOneForUpdateInput = {
  connect?: InputMaybe<ObjectTypeWhereUniqueInput>;
  create?: InputMaybe<ObjectTypeCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

export type ObjectTypeUpdateArgs = {
  data: ObjectTypeUpdateInput;
  where: ObjectTypeWhereUniqueInput;
};

export type ObjectTypeUpdateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type ObjectTypeWhereInput = {
  AND?: InputMaybe<Array<ObjectTypeWhereInput>>;
  NOT?: InputMaybe<Array<ObjectTypeWhereInput>>;
  OR?: InputMaybe<Array<ObjectTypeWhereInput>>;
  createdAt?: InputMaybe<DateTimeNullableFilter>;
  id?: InputMaybe<IdFilter>;
  label?: InputMaybe<StringFilter>;
  updatedAt?: InputMaybe<DateTimeNullableFilter>;
};

export type ObjectTypeWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
};

export type ObjectUpdateArgs = {
  data: ObjectUpdateInput;
  where: ObjectWhereUniqueInput;
};

export type ObjectUpdateInput = {
  author?: InputMaybe<Scalars['String']['input']>;
  colours?: InputMaybe<ColourRelateToManyForUpdateInput>;
  cover?: InputMaybe<MediaRelateToOneForUpdateInput>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  date?: InputMaybe<Scalars['String']['input']>;
  description?: InputMaybe<Scalars['JSON']['input']>;
  digitalizedByChromotope?: InputMaybe<Scalars['Boolean']['input']>;
  dimensions?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  location?: InputMaybe<Scalars['String']['input']>;
  medias?: InputMaybe<MediaRelateToManyForUpdateInput>;
  medium?: InputMaybe<Scalars['String']['input']>;
  narratives?: InputMaybe<NarrativeRelateToManyForUpdateInput>;
  reference?: InputMaybe<ReferenceRelateToOneForUpdateInput>;
  slug?: InputMaybe<Scalars['String']['input']>;
  type?: InputMaybe<ObjectTypeRelateToOneForUpdateInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type ObjectWhereInput = {
  AND?: InputMaybe<Array<ObjectWhereInput>>;
  NOT?: InputMaybe<Array<ObjectWhereInput>>;
  OR?: InputMaybe<Array<ObjectWhereInput>>;
  author?: InputMaybe<StringFilter>;
  colours?: InputMaybe<ColourManyRelationFilter>;
  cover?: InputMaybe<MediaWhereInput>;
  createdAt?: InputMaybe<DateTimeNullableFilter>;
  date?: InputMaybe<StringNullableFilter>;
  digitalizedByChromotope?: InputMaybe<BooleanFilter>;
  dimensions?: InputMaybe<StringFilter>;
  id?: InputMaybe<IdFilter>;
  label?: InputMaybe<StringFilter>;
  location?: InputMaybe<StringFilter>;
  medias?: InputMaybe<MediaManyRelationFilter>;
  medium?: InputMaybe<StringFilter>;
  narratives?: InputMaybe<NarrativeManyRelationFilter>;
  reference?: InputMaybe<ReferenceWhereInput>;
  slug?: InputMaybe<StringFilter>;
  type?: InputMaybe<ObjectTypeWhereInput>;
  updatedAt?: InputMaybe<DateTimeNullableFilter>;
};

export type ObjectWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  slug?: InputMaybe<Scalars['String']['input']>;
};

export type Object_Description_Document = {
  __typename?: 'Object_description_Document';
  document: Scalars['JSON']['output'];
};


export type Object_Description_DocumentDocumentArgs = {
  hydrateRelationships?: Scalars['Boolean']['input'];
};

export type Occupation = {
  __typename?: 'Occupation';
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  description?: Maybe<Occupation_Description_Document>;
  id: Scalars['ID']['output'];
  label?: Maybe<Scalars['String']['output']>;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
  wikidataId?: Maybe<Scalars['String']['output']>;
};

export type OccupationCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  description?: InputMaybe<Scalars['JSON']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  wikidataId?: InputMaybe<Scalars['String']['input']>;
};

export type OccupationManyRelationFilter = {
  every?: InputMaybe<OccupationWhereInput>;
  none?: InputMaybe<OccupationWhereInput>;
  some?: InputMaybe<OccupationWhereInput>;
};

export type OccupationOrderByInput = {
  createdAt?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  label?: InputMaybe<OrderDirection>;
  updatedAt?: InputMaybe<OrderDirection>;
  wikidataId?: InputMaybe<OrderDirection>;
};

export type OccupationRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<OccupationWhereUniqueInput>>;
  create?: InputMaybe<Array<OccupationCreateInput>>;
};

export type OccupationRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<OccupationWhereUniqueInput>>;
  create?: InputMaybe<Array<OccupationCreateInput>>;
  disconnect?: InputMaybe<Array<OccupationWhereUniqueInput>>;
  set?: InputMaybe<Array<OccupationWhereUniqueInput>>;
};

export type OccupationUpdateArgs = {
  data: OccupationUpdateInput;
  where: OccupationWhereUniqueInput;
};

export type OccupationUpdateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  description?: InputMaybe<Scalars['JSON']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  wikidataId?: InputMaybe<Scalars['String']['input']>;
};

export type OccupationWhereInput = {
  AND?: InputMaybe<Array<OccupationWhereInput>>;
  NOT?: InputMaybe<Array<OccupationWhereInput>>;
  OR?: InputMaybe<Array<OccupationWhereInput>>;
  createdAt?: InputMaybe<DateTimeNullableFilter>;
  id?: InputMaybe<IdFilter>;
  label?: InputMaybe<StringFilter>;
  updatedAt?: InputMaybe<DateTimeNullableFilter>;
  wikidataId?: InputMaybe<StringNullableFilter>;
};

export type OccupationWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  wikidataId?: InputMaybe<Scalars['String']['input']>;
};

export type Occupation_Description_Document = {
  __typename?: 'Occupation_description_Document';
  document: Scalars['JSON']['output'];
};


export type Occupation_Description_DocumentDocumentArgs = {
  hydrateRelationships?: Scalars['Boolean']['input'];
};

export enum OrderDirection {
  Asc = 'asc',
  Desc = 'desc'
}

export type Organisation = {
  __typename?: 'Organisation';
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  dateOfInception?: Maybe<Scalars['String']['output']>;
  description?: Maybe<Organisation_Description_Document>;
  id: Scalars['ID']['output'];
  label?: Maybe<Scalars['String']['output']>;
  narratives?: Maybe<Array<Narrative>>;
  narrativesCount?: Maybe<Scalars['Int']['output']>;
  slug?: Maybe<Scalars['String']['output']>;
  type?: Maybe<OrganisationType>;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
  wikidataId?: Maybe<Scalars['String']['output']>;
};


export type OrganisationNarrativesArgs = {
  cursor?: InputMaybe<NarrativeWhereUniqueInput>;
  orderBy?: Array<NarrativeOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: NarrativeWhereInput;
};


export type OrganisationNarrativesCountArgs = {
  where?: NarrativeWhereInput;
};

export type OrganisationCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  dateOfInception?: InputMaybe<Scalars['String']['input']>;
  description?: InputMaybe<Scalars['JSON']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  narratives?: InputMaybe<NarrativeRelateToManyForCreateInput>;
  slug?: InputMaybe<Scalars['String']['input']>;
  type?: InputMaybe<OrganisationTypeRelateToOneForCreateInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  wikidataId?: InputMaybe<Scalars['String']['input']>;
};

export type OrganisationManyRelationFilter = {
  every?: InputMaybe<OrganisationWhereInput>;
  none?: InputMaybe<OrganisationWhereInput>;
  some?: InputMaybe<OrganisationWhereInput>;
};

export type OrganisationOrderByInput = {
  createdAt?: InputMaybe<OrderDirection>;
  dateOfInception?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  label?: InputMaybe<OrderDirection>;
  slug?: InputMaybe<OrderDirection>;
  updatedAt?: InputMaybe<OrderDirection>;
  wikidataId?: InputMaybe<OrderDirection>;
};

export type OrganisationRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<OrganisationWhereUniqueInput>>;
  create?: InputMaybe<Array<OrganisationCreateInput>>;
};

export type OrganisationRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<OrganisationWhereUniqueInput>>;
  create?: InputMaybe<Array<OrganisationCreateInput>>;
  disconnect?: InputMaybe<Array<OrganisationWhereUniqueInput>>;
  set?: InputMaybe<Array<OrganisationWhereUniqueInput>>;
};

export type OrganisationType = {
  __typename?: 'OrganisationType';
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  description?: Maybe<OrganisationType_Description_Document>;
  id: Scalars['ID']['output'];
  label?: Maybe<Scalars['String']['output']>;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
  wikidataId?: Maybe<Scalars['String']['output']>;
};

export type OrganisationTypeCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  description?: InputMaybe<Scalars['JSON']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  wikidataId?: InputMaybe<Scalars['String']['input']>;
};

export type OrganisationTypeOrderByInput = {
  createdAt?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  label?: InputMaybe<OrderDirection>;
  updatedAt?: InputMaybe<OrderDirection>;
  wikidataId?: InputMaybe<OrderDirection>;
};

export type OrganisationTypeRelateToOneForCreateInput = {
  connect?: InputMaybe<OrganisationTypeWhereUniqueInput>;
  create?: InputMaybe<OrganisationTypeCreateInput>;
};

export type OrganisationTypeRelateToOneForUpdateInput = {
  connect?: InputMaybe<OrganisationTypeWhereUniqueInput>;
  create?: InputMaybe<OrganisationTypeCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

export type OrganisationTypeUpdateArgs = {
  data: OrganisationTypeUpdateInput;
  where: OrganisationTypeWhereUniqueInput;
};

export type OrganisationTypeUpdateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  description?: InputMaybe<Scalars['JSON']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  wikidataId?: InputMaybe<Scalars['String']['input']>;
};

export type OrganisationTypeWhereInput = {
  AND?: InputMaybe<Array<OrganisationTypeWhereInput>>;
  NOT?: InputMaybe<Array<OrganisationTypeWhereInput>>;
  OR?: InputMaybe<Array<OrganisationTypeWhereInput>>;
  createdAt?: InputMaybe<DateTimeNullableFilter>;
  id?: InputMaybe<IdFilter>;
  label?: InputMaybe<StringFilter>;
  updatedAt?: InputMaybe<DateTimeNullableFilter>;
  wikidataId?: InputMaybe<StringNullableFilter>;
};

export type OrganisationTypeWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  wikidataId?: InputMaybe<Scalars['String']['input']>;
};

export type OrganisationType_Description_Document = {
  __typename?: 'OrganisationType_description_Document';
  document: Scalars['JSON']['output'];
};


export type OrganisationType_Description_DocumentDocumentArgs = {
  hydrateRelationships?: Scalars['Boolean']['input'];
};

export type OrganisationUpdateArgs = {
  data: OrganisationUpdateInput;
  where: OrganisationWhereUniqueInput;
};

export type OrganisationUpdateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  dateOfInception?: InputMaybe<Scalars['String']['input']>;
  description?: InputMaybe<Scalars['JSON']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  narratives?: InputMaybe<NarrativeRelateToManyForUpdateInput>;
  slug?: InputMaybe<Scalars['String']['input']>;
  type?: InputMaybe<OrganisationTypeRelateToOneForUpdateInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  wikidataId?: InputMaybe<Scalars['String']['input']>;
};

export type OrganisationWhereInput = {
  AND?: InputMaybe<Array<OrganisationWhereInput>>;
  NOT?: InputMaybe<Array<OrganisationWhereInput>>;
  OR?: InputMaybe<Array<OrganisationWhereInput>>;
  createdAt?: InputMaybe<DateTimeNullableFilter>;
  dateOfInception?: InputMaybe<StringNullableFilter>;
  id?: InputMaybe<IdFilter>;
  label?: InputMaybe<StringFilter>;
  narratives?: InputMaybe<NarrativeManyRelationFilter>;
  slug?: InputMaybe<StringFilter>;
  type?: InputMaybe<OrganisationTypeWhereInput>;
  updatedAt?: InputMaybe<DateTimeNullableFilter>;
  wikidataId?: InputMaybe<StringNullableFilter>;
};

export type OrganisationWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  slug?: InputMaybe<Scalars['String']['input']>;
  wikidataId?: InputMaybe<Scalars['String']['input']>;
};

export type Organisation_Description_Document = {
  __typename?: 'Organisation_description_Document';
  document: Scalars['JSON']['output'];
};


export type Organisation_Description_DocumentDocumentArgs = {
  hydrateRelationships?: Scalars['Boolean']['input'];
};

export type PasswordState = {
  __typename?: 'PasswordState';
  isSet: Scalars['Boolean']['output'];
};

export type Person = {
  __typename?: 'Person';
  biography?: Maybe<Person_Biography_Document>;
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  dateOfBirth?: Maybe<Scalars['String']['output']>;
  dateOfDeath?: Maybe<Scalars['String']['output']>;
  firstName?: Maybe<Scalars['String']['output']>;
  fullName?: Maybe<Scalars['String']['output']>;
  id: Scalars['ID']['output'];
  label?: Maybe<Scalars['String']['output']>;
  lastName?: Maybe<Scalars['String']['output']>;
  narratives?: Maybe<Array<Narrative>>;
  narrativesCount?: Maybe<Scalars['Int']['output']>;
  occupations?: Maybe<Array<Occupation>>;
  occupationsCount?: Maybe<Scalars['Int']['output']>;
  slug?: Maybe<Scalars['String']['output']>;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
  wikidataId?: Maybe<Scalars['String']['output']>;
};


export type PersonNarrativesArgs = {
  cursor?: InputMaybe<NarrativeWhereUniqueInput>;
  orderBy?: Array<NarrativeOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: NarrativeWhereInput;
};


export type PersonNarrativesCountArgs = {
  where?: NarrativeWhereInput;
};


export type PersonOccupationsArgs = {
  cursor?: InputMaybe<OccupationWhereUniqueInput>;
  orderBy?: Array<OccupationOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: OccupationWhereInput;
};


export type PersonOccupationsCountArgs = {
  where?: OccupationWhereInput;
};

export type PersonCreateInput = {
  biography?: InputMaybe<Scalars['JSON']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  dateOfBirth?: InputMaybe<Scalars['String']['input']>;
  dateOfDeath?: InputMaybe<Scalars['String']['input']>;
  firstName?: InputMaybe<Scalars['String']['input']>;
  fullName?: InputMaybe<Scalars['String']['input']>;
  lastName?: InputMaybe<Scalars['String']['input']>;
  narratives?: InputMaybe<NarrativeRelateToManyForCreateInput>;
  occupations?: InputMaybe<OccupationRelateToManyForCreateInput>;
  slug?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  wikidataId?: InputMaybe<Scalars['String']['input']>;
};

export type PersonManyRelationFilter = {
  every?: InputMaybe<PersonWhereInput>;
  none?: InputMaybe<PersonWhereInput>;
  some?: InputMaybe<PersonWhereInput>;
};

export type PersonOrderByInput = {
  createdAt?: InputMaybe<OrderDirection>;
  dateOfBirth?: InputMaybe<OrderDirection>;
  dateOfDeath?: InputMaybe<OrderDirection>;
  firstName?: InputMaybe<OrderDirection>;
  fullName?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  lastName?: InputMaybe<OrderDirection>;
  slug?: InputMaybe<OrderDirection>;
  updatedAt?: InputMaybe<OrderDirection>;
  wikidataId?: InputMaybe<OrderDirection>;
};

export type PersonRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<PersonWhereUniqueInput>>;
  create?: InputMaybe<Array<PersonCreateInput>>;
};

export type PersonRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<PersonWhereUniqueInput>>;
  create?: InputMaybe<Array<PersonCreateInput>>;
  disconnect?: InputMaybe<Array<PersonWhereUniqueInput>>;
  set?: InputMaybe<Array<PersonWhereUniqueInput>>;
};

export type PersonUpdateArgs = {
  data: PersonUpdateInput;
  where: PersonWhereUniqueInput;
};

export type PersonUpdateInput = {
  biography?: InputMaybe<Scalars['JSON']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  dateOfBirth?: InputMaybe<Scalars['String']['input']>;
  dateOfDeath?: InputMaybe<Scalars['String']['input']>;
  firstName?: InputMaybe<Scalars['String']['input']>;
  fullName?: InputMaybe<Scalars['String']['input']>;
  lastName?: InputMaybe<Scalars['String']['input']>;
  narratives?: InputMaybe<NarrativeRelateToManyForUpdateInput>;
  occupations?: InputMaybe<OccupationRelateToManyForUpdateInput>;
  slug?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  wikidataId?: InputMaybe<Scalars['String']['input']>;
};

export type PersonWhereInput = {
  AND?: InputMaybe<Array<PersonWhereInput>>;
  NOT?: InputMaybe<Array<PersonWhereInput>>;
  OR?: InputMaybe<Array<PersonWhereInput>>;
  createdAt?: InputMaybe<DateTimeNullableFilter>;
  dateOfBirth?: InputMaybe<StringNullableFilter>;
  dateOfDeath?: InputMaybe<StringNullableFilter>;
  firstName?: InputMaybe<StringFilter>;
  fullName?: InputMaybe<StringFilter>;
  id?: InputMaybe<IdFilter>;
  lastName?: InputMaybe<StringFilter>;
  narratives?: InputMaybe<NarrativeManyRelationFilter>;
  occupations?: InputMaybe<OccupationManyRelationFilter>;
  slug?: InputMaybe<StringFilter>;
  updatedAt?: InputMaybe<DateTimeNullableFilter>;
  wikidataId?: InputMaybe<StringNullableFilter>;
};

export type PersonWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  slug?: InputMaybe<Scalars['String']['input']>;
  wikidataId?: InputMaybe<Scalars['String']['input']>;
};

export type Person_Biography_Document = {
  __typename?: 'Person_biography_Document';
  document: Scalars['JSON']['output'];
};


export type Person_Biography_DocumentDocumentArgs = {
  hydrateRelationships?: Scalars['Boolean']['input'];
};

export type Query = {
  __typename?: 'Query';
  authenticatedItem?: Maybe<AuthenticatedItem>;
  author?: Maybe<Author>;
  authors?: Maybe<Array<Author>>;
  authorsCount?: Maybe<Scalars['Int']['output']>;
  colour?: Maybe<Colour>;
  colours?: Maybe<Array<Colour>>;
  coloursCount?: Maybe<Scalars['Int']['output']>;
  event?: Maybe<Event>;
  eventType?: Maybe<EventType>;
  eventTypes?: Maybe<Array<EventType>>;
  eventTypesCount?: Maybe<Scalars['Int']['output']>;
  events?: Maybe<Array<Event>>;
  eventsCount?: Maybe<Scalars['Int']['output']>;
  glossaries?: Maybe<Array<Glossary>>;
  glossariesCount?: Maybe<Scalars['Int']['output']>;
  glossary?: Maybe<Glossary>;
  glossaryType?: Maybe<GlossaryType>;
  glossaryTypes?: Maybe<Array<GlossaryType>>;
  glossaryTypesCount?: Maybe<Scalars['Int']['output']>;
  keystone: KeystoneMeta;
  logo?: Maybe<Logo>;
  logos?: Maybe<Array<Logo>>;
  logosCount?: Maybe<Scalars['Int']['output']>;
  media?: Maybe<Media>;
  medias?: Maybe<Array<Media>>;
  mediasCount?: Maybe<Scalars['Int']['output']>;
  narrative?: Maybe<Narrative>;
  narrativeCategories?: Maybe<Array<NarrativeCategory>>;
  narrativeCategoriesCount?: Maybe<Scalars['Int']['output']>;
  narrativeCategory?: Maybe<NarrativeCategory>;
  narrativeTranslation?: Maybe<NarrativeTranslation>;
  narrativeTranslations?: Maybe<Array<NarrativeTranslation>>;
  narrativeTranslationsCount?: Maybe<Scalars['Int']['output']>;
  narratives?: Maybe<Array<Narrative>>;
  narrativesCount?: Maybe<Scalars['Int']['output']>;
  object?: Maybe<Object>;
  objectType?: Maybe<ObjectType>;
  objectTypes?: Maybe<Array<ObjectType>>;
  objectTypesCount?: Maybe<Scalars['Int']['output']>;
  objects?: Maybe<Array<Object>>;
  objectsCount?: Maybe<Scalars['Int']['output']>;
  occupation?: Maybe<Occupation>;
  occupations?: Maybe<Array<Occupation>>;
  occupationsCount?: Maybe<Scalars['Int']['output']>;
  organisation?: Maybe<Organisation>;
  organisationType?: Maybe<OrganisationType>;
  organisationTypes?: Maybe<Array<OrganisationType>>;
  organisationTypesCount?: Maybe<Scalars['Int']['output']>;
  organisations?: Maybe<Array<Organisation>>;
  organisationsCount?: Maybe<Scalars['Int']['output']>;
  people?: Maybe<Array<Person>>;
  peopleCount?: Maybe<Scalars['Int']['output']>;
  person?: Maybe<Person>;
  reference?: Maybe<Reference>;
  references?: Maybe<Array<Reference>>;
  referencesCount?: Maybe<Scalars['Int']['output']>;
  staticContent?: Maybe<StaticContent>;
  staticContents?: Maybe<Array<StaticContent>>;
  staticContentsCount?: Maybe<Scalars['Int']['output']>;
  user?: Maybe<User>;
  users?: Maybe<Array<User>>;
  usersCount?: Maybe<Scalars['Int']['output']>;
};


export type QueryAuthorArgs = {
  where: AuthorWhereUniqueInput;
};


export type QueryAuthorsArgs = {
  cursor?: InputMaybe<AuthorWhereUniqueInput>;
  orderBy?: Array<AuthorOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: AuthorWhereInput;
};


export type QueryAuthorsCountArgs = {
  where?: AuthorWhereInput;
};


export type QueryColourArgs = {
  where: ColourWhereUniqueInput;
};


export type QueryColoursArgs = {
  cursor?: InputMaybe<ColourWhereUniqueInput>;
  orderBy?: Array<ColourOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: ColourWhereInput;
};


export type QueryColoursCountArgs = {
  where?: ColourWhereInput;
};


export type QueryEventArgs = {
  where: EventWhereUniqueInput;
};


export type QueryEventTypeArgs = {
  where: EventTypeWhereUniqueInput;
};


export type QueryEventTypesArgs = {
  cursor?: InputMaybe<EventTypeWhereUniqueInput>;
  orderBy?: Array<EventTypeOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: EventTypeWhereInput;
};


export type QueryEventTypesCountArgs = {
  where?: EventTypeWhereInput;
};


export type QueryEventsArgs = {
  cursor?: InputMaybe<EventWhereUniqueInput>;
  orderBy?: Array<EventOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: EventWhereInput;
};


export type QueryEventsCountArgs = {
  where?: EventWhereInput;
};


export type QueryGlossariesArgs = {
  cursor?: InputMaybe<GlossaryWhereUniqueInput>;
  orderBy?: Array<GlossaryOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: GlossaryWhereInput;
};


export type QueryGlossariesCountArgs = {
  where?: GlossaryWhereInput;
};


export type QueryGlossaryArgs = {
  where: GlossaryWhereUniqueInput;
};


export type QueryGlossaryTypeArgs = {
  where: GlossaryTypeWhereUniqueInput;
};


export type QueryGlossaryTypesArgs = {
  cursor?: InputMaybe<GlossaryTypeWhereUniqueInput>;
  orderBy?: Array<GlossaryTypeOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: GlossaryTypeWhereInput;
};


export type QueryGlossaryTypesCountArgs = {
  where?: GlossaryTypeWhereInput;
};


export type QueryLogoArgs = {
  where: LogoWhereUniqueInput;
};


export type QueryLogosArgs = {
  cursor?: InputMaybe<LogoWhereUniqueInput>;
  orderBy?: Array<LogoOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: LogoWhereInput;
};


export type QueryLogosCountArgs = {
  where?: LogoWhereInput;
};


export type QueryMediaArgs = {
  where: MediaWhereUniqueInput;
};


export type QueryMediasArgs = {
  cursor?: InputMaybe<MediaWhereUniqueInput>;
  orderBy?: Array<MediaOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: MediaWhereInput;
};


export type QueryMediasCountArgs = {
  where?: MediaWhereInput;
};


export type QueryNarrativeArgs = {
  where: NarrativeWhereUniqueInput;
};


export type QueryNarrativeCategoriesArgs = {
  cursor?: InputMaybe<NarrativeCategoryWhereUniqueInput>;
  orderBy?: Array<NarrativeCategoryOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: NarrativeCategoryWhereInput;
};


export type QueryNarrativeCategoriesCountArgs = {
  where?: NarrativeCategoryWhereInput;
};


export type QueryNarrativeCategoryArgs = {
  where: NarrativeCategoryWhereUniqueInput;
};


export type QueryNarrativeTranslationArgs = {
  where: NarrativeTranslationWhereUniqueInput;
};


export type QueryNarrativeTranslationsArgs = {
  cursor?: InputMaybe<NarrativeTranslationWhereUniqueInput>;
  orderBy?: Array<NarrativeTranslationOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: NarrativeTranslationWhereInput;
};


export type QueryNarrativeTranslationsCountArgs = {
  where?: NarrativeTranslationWhereInput;
};


export type QueryNarrativesArgs = {
  cursor?: InputMaybe<NarrativeWhereUniqueInput>;
  orderBy?: Array<NarrativeOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: NarrativeWhereInput;
};


export type QueryNarrativesCountArgs = {
  where?: NarrativeWhereInput;
};


export type QueryObjectArgs = {
  where: ObjectWhereUniqueInput;
};


export type QueryObjectTypeArgs = {
  where: ObjectTypeWhereUniqueInput;
};


export type QueryObjectTypesArgs = {
  cursor?: InputMaybe<ObjectTypeWhereUniqueInput>;
  orderBy?: Array<ObjectTypeOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: ObjectTypeWhereInput;
};


export type QueryObjectTypesCountArgs = {
  where?: ObjectTypeWhereInput;
};


export type QueryObjectsArgs = {
  cursor?: InputMaybe<ObjectWhereUniqueInput>;
  orderBy?: Array<ObjectOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: ObjectWhereInput;
};


export type QueryObjectsCountArgs = {
  where?: ObjectWhereInput;
};


export type QueryOccupationArgs = {
  where: OccupationWhereUniqueInput;
};


export type QueryOccupationsArgs = {
  cursor?: InputMaybe<OccupationWhereUniqueInput>;
  orderBy?: Array<OccupationOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: OccupationWhereInput;
};


export type QueryOccupationsCountArgs = {
  where?: OccupationWhereInput;
};


export type QueryOrganisationArgs = {
  where: OrganisationWhereUniqueInput;
};


export type QueryOrganisationTypeArgs = {
  where: OrganisationTypeWhereUniqueInput;
};


export type QueryOrganisationTypesArgs = {
  cursor?: InputMaybe<OrganisationTypeWhereUniqueInput>;
  orderBy?: Array<OrganisationTypeOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: OrganisationTypeWhereInput;
};


export type QueryOrganisationTypesCountArgs = {
  where?: OrganisationTypeWhereInput;
};


export type QueryOrganisationsArgs = {
  cursor?: InputMaybe<OrganisationWhereUniqueInput>;
  orderBy?: Array<OrganisationOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: OrganisationWhereInput;
};


export type QueryOrganisationsCountArgs = {
  where?: OrganisationWhereInput;
};


export type QueryPeopleArgs = {
  cursor?: InputMaybe<PersonWhereUniqueInput>;
  orderBy?: Array<PersonOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: PersonWhereInput;
};


export type QueryPeopleCountArgs = {
  where?: PersonWhereInput;
};


export type QueryPersonArgs = {
  where: PersonWhereUniqueInput;
};


export type QueryReferenceArgs = {
  where: ReferenceWhereUniqueInput;
};


export type QueryReferencesArgs = {
  cursor?: InputMaybe<ReferenceWhereUniqueInput>;
  orderBy?: Array<ReferenceOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: ReferenceWhereInput;
};


export type QueryReferencesCountArgs = {
  where?: ReferenceWhereInput;
};


export type QueryStaticContentArgs = {
  where?: StaticContentWhereUniqueInput;
};


export type QueryStaticContentsArgs = {
  cursor?: InputMaybe<StaticContentWhereUniqueInput>;
  orderBy?: Array<StaticContentOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: StaticContentWhereInput;
};


export type QueryStaticContentsCountArgs = {
  where?: StaticContentWhereInput;
};


export type QueryUserArgs = {
  where: UserWhereUniqueInput;
};


export type QueryUsersArgs = {
  cursor?: InputMaybe<UserWhereUniqueInput>;
  orderBy?: Array<UserOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: UserWhereInput;
};


export type QueryUsersCountArgs = {
  where?: UserWhereInput;
};

export enum QueryMode {
  Default = 'default',
  Insensitive = 'insensitive'
}

export type Reference = {
  __typename?: 'Reference';
  citation?: Maybe<Reference_Citation_Document>;
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  id: Scalars['ID']['output'];
  label?: Maybe<Scalars['String']['output']>;
  medias?: Maybe<Array<Media>>;
  mediasCount?: Maybe<Scalars['Int']['output']>;
  narratives?: Maybe<Array<Narrative>>;
  narrativesCount?: Maybe<Scalars['Int']['output']>;
  narrativesExtra?: Maybe<Array<Narrative>>;
  narrativesExtraCount?: Maybe<Scalars['Int']['output']>;
  note?: Maybe<Scalars['String']['output']>;
  slug?: Maybe<Scalars['String']['output']>;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
};


export type ReferenceMediasArgs = {
  cursor?: InputMaybe<MediaWhereUniqueInput>;
  orderBy?: Array<MediaOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: MediaWhereInput;
};


export type ReferenceMediasCountArgs = {
  where?: MediaWhereInput;
};


export type ReferenceNarrativesArgs = {
  cursor?: InputMaybe<NarrativeWhereUniqueInput>;
  orderBy?: Array<NarrativeOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: NarrativeWhereInput;
};


export type ReferenceNarrativesCountArgs = {
  where?: NarrativeWhereInput;
};


export type ReferenceNarrativesExtraArgs = {
  cursor?: InputMaybe<NarrativeWhereUniqueInput>;
  orderBy?: Array<NarrativeOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: NarrativeWhereInput;
};


export type ReferenceNarrativesExtraCountArgs = {
  where?: NarrativeWhereInput;
};

export type ReferenceCreateInput = {
  citation?: InputMaybe<Scalars['JSON']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  medias?: InputMaybe<MediaRelateToManyForCreateInput>;
  narratives?: InputMaybe<NarrativeRelateToManyForCreateInput>;
  narrativesExtra?: InputMaybe<NarrativeRelateToManyForCreateInput>;
  note?: InputMaybe<Scalars['String']['input']>;
  slug?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type ReferenceManyRelationFilter = {
  every?: InputMaybe<ReferenceWhereInput>;
  none?: InputMaybe<ReferenceWhereInput>;
  some?: InputMaybe<ReferenceWhereInput>;
};

export type ReferenceOrderByInput = {
  createdAt?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  label?: InputMaybe<OrderDirection>;
  note?: InputMaybe<OrderDirection>;
  slug?: InputMaybe<OrderDirection>;
  updatedAt?: InputMaybe<OrderDirection>;
};

export type ReferenceRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<ReferenceWhereUniqueInput>>;
  create?: InputMaybe<Array<ReferenceCreateInput>>;
};

export type ReferenceRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<ReferenceWhereUniqueInput>>;
  create?: InputMaybe<Array<ReferenceCreateInput>>;
  disconnect?: InputMaybe<Array<ReferenceWhereUniqueInput>>;
  set?: InputMaybe<Array<ReferenceWhereUniqueInput>>;
};

export type ReferenceRelateToOneForCreateInput = {
  connect?: InputMaybe<ReferenceWhereUniqueInput>;
  create?: InputMaybe<ReferenceCreateInput>;
};

export type ReferenceRelateToOneForUpdateInput = {
  connect?: InputMaybe<ReferenceWhereUniqueInput>;
  create?: InputMaybe<ReferenceCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

export type ReferenceUpdateArgs = {
  data: ReferenceUpdateInput;
  where: ReferenceWhereUniqueInput;
};

export type ReferenceUpdateInput = {
  citation?: InputMaybe<Scalars['JSON']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  medias?: InputMaybe<MediaRelateToManyForUpdateInput>;
  narratives?: InputMaybe<NarrativeRelateToManyForUpdateInput>;
  narrativesExtra?: InputMaybe<NarrativeRelateToManyForUpdateInput>;
  note?: InputMaybe<Scalars['String']['input']>;
  slug?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type ReferenceWhereInput = {
  AND?: InputMaybe<Array<ReferenceWhereInput>>;
  NOT?: InputMaybe<Array<ReferenceWhereInput>>;
  OR?: InputMaybe<Array<ReferenceWhereInput>>;
  createdAt?: InputMaybe<DateTimeNullableFilter>;
  id?: InputMaybe<IdFilter>;
  label?: InputMaybe<StringFilter>;
  medias?: InputMaybe<MediaManyRelationFilter>;
  narratives?: InputMaybe<NarrativeManyRelationFilter>;
  narrativesExtra?: InputMaybe<NarrativeManyRelationFilter>;
  note?: InputMaybe<StringFilter>;
  slug?: InputMaybe<StringFilter>;
  updatedAt?: InputMaybe<DateTimeNullableFilter>;
};

export type ReferenceWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  note?: InputMaybe<Scalars['String']['input']>;
  slug?: InputMaybe<Scalars['String']['input']>;
};

export type Reference_Citation_Document = {
  __typename?: 'Reference_citation_Document';
  document: Scalars['JSON']['output'];
};


export type Reference_Citation_DocumentDocumentArgs = {
  hydrateRelationships?: Scalars['Boolean']['input'];
};

export type StaticContent = {
  __typename?: 'StaticContent';
  about?: Maybe<StaticContent_About_Document>;
  baseline?: Maybe<Scalars['String']['output']>;
  colourWheelIntroduction?: Maybe<StaticContent_ColourWheelIntroduction_Document>;
  credits?: Maybe<StaticContent_Credits_Document>;
  funding?: Maybe<StaticContent_Funding_Document>;
  fundingLogos?: Maybe<Array<Logo>>;
  fundingLogosCount?: Maybe<Scalars['Int']['output']>;
  id: Scalars['ID']['output'];
  legalNotice?: Maybe<StaticContent_LegalNotice_Document>;
  partnersLogos?: Maybe<Array<Logo>>;
  partnersLogosCount?: Maybe<Scalars['Int']['output']>;
};


export type StaticContentFundingLogosArgs = {
  cursor?: InputMaybe<LogoWhereUniqueInput>;
  orderBy?: Array<LogoOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: LogoWhereInput;
};


export type StaticContentFundingLogosCountArgs = {
  where?: LogoWhereInput;
};


export type StaticContentPartnersLogosArgs = {
  cursor?: InputMaybe<LogoWhereUniqueInput>;
  orderBy?: Array<LogoOrderByInput>;
  skip?: Scalars['Int']['input'];
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: LogoWhereInput;
};


export type StaticContentPartnersLogosCountArgs = {
  where?: LogoWhereInput;
};

export type StaticContentCreateInput = {
  about?: InputMaybe<Scalars['JSON']['input']>;
  baseline?: InputMaybe<Scalars['String']['input']>;
  colourWheelIntroduction?: InputMaybe<Scalars['JSON']['input']>;
  credits?: InputMaybe<Scalars['JSON']['input']>;
  funding?: InputMaybe<Scalars['JSON']['input']>;
  fundingLogos?: InputMaybe<LogoRelateToManyForCreateInput>;
  legalNotice?: InputMaybe<Scalars['JSON']['input']>;
  partnersLogos?: InputMaybe<LogoRelateToManyForCreateInput>;
};

export type StaticContentOrderByInput = {
  baseline?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
};

export type StaticContentUpdateArgs = {
  data: StaticContentUpdateInput;
  where?: StaticContentWhereUniqueInput;
};

export type StaticContentUpdateInput = {
  about?: InputMaybe<Scalars['JSON']['input']>;
  baseline?: InputMaybe<Scalars['String']['input']>;
  colourWheelIntroduction?: InputMaybe<Scalars['JSON']['input']>;
  credits?: InputMaybe<Scalars['JSON']['input']>;
  funding?: InputMaybe<Scalars['JSON']['input']>;
  fundingLogos?: InputMaybe<LogoRelateToManyForUpdateInput>;
  legalNotice?: InputMaybe<Scalars['JSON']['input']>;
  partnersLogos?: InputMaybe<LogoRelateToManyForUpdateInput>;
};

export type StaticContentWhereInput = {
  AND?: InputMaybe<Array<StaticContentWhereInput>>;
  NOT?: InputMaybe<Array<StaticContentWhereInput>>;
  OR?: InputMaybe<Array<StaticContentWhereInput>>;
  baseline?: InputMaybe<StringFilter>;
  fundingLogos?: InputMaybe<LogoManyRelationFilter>;
  id?: InputMaybe<IdFilter>;
  partnersLogos?: InputMaybe<LogoManyRelationFilter>;
};

export type StaticContentWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type StaticContent_About_Document = {
  __typename?: 'StaticContent_about_Document';
  document: Scalars['JSON']['output'];
};


export type StaticContent_About_DocumentDocumentArgs = {
  hydrateRelationships?: Scalars['Boolean']['input'];
};

export type StaticContent_ColourWheelIntroduction_Document = {
  __typename?: 'StaticContent_colourWheelIntroduction_Document';
  document: Scalars['JSON']['output'];
};


export type StaticContent_ColourWheelIntroduction_DocumentDocumentArgs = {
  hydrateRelationships?: Scalars['Boolean']['input'];
};

export type StaticContent_Credits_Document = {
  __typename?: 'StaticContent_credits_Document';
  document: Scalars['JSON']['output'];
};


export type StaticContent_Credits_DocumentDocumentArgs = {
  hydrateRelationships?: Scalars['Boolean']['input'];
};

export type StaticContent_Funding_Document = {
  __typename?: 'StaticContent_funding_Document';
  document: Scalars['JSON']['output'];
};


export type StaticContent_Funding_DocumentDocumentArgs = {
  hydrateRelationships?: Scalars['Boolean']['input'];
};

export type StaticContent_LegalNotice_Document = {
  __typename?: 'StaticContent_legalNotice_Document';
  document: Scalars['JSON']['output'];
};


export type StaticContent_LegalNotice_DocumentDocumentArgs = {
  hydrateRelationships?: Scalars['Boolean']['input'];
};

export type StringFilter = {
  contains?: InputMaybe<Scalars['String']['input']>;
  endsWith?: InputMaybe<Scalars['String']['input']>;
  equals?: InputMaybe<Scalars['String']['input']>;
  gt?: InputMaybe<Scalars['String']['input']>;
  gte?: InputMaybe<Scalars['String']['input']>;
  in?: InputMaybe<Array<Scalars['String']['input']>>;
  lt?: InputMaybe<Scalars['String']['input']>;
  lte?: InputMaybe<Scalars['String']['input']>;
  mode?: InputMaybe<QueryMode>;
  not?: InputMaybe<NestedStringFilter>;
  notIn?: InputMaybe<Array<Scalars['String']['input']>>;
  startsWith?: InputMaybe<Scalars['String']['input']>;
};

export type StringNullableFilter = {
  contains?: InputMaybe<Scalars['String']['input']>;
  endsWith?: InputMaybe<Scalars['String']['input']>;
  equals?: InputMaybe<Scalars['String']['input']>;
  gt?: InputMaybe<Scalars['String']['input']>;
  gte?: InputMaybe<Scalars['String']['input']>;
  in?: InputMaybe<Array<Scalars['String']['input']>>;
  lt?: InputMaybe<Scalars['String']['input']>;
  lte?: InputMaybe<Scalars['String']['input']>;
  mode?: InputMaybe<QueryMode>;
  not?: InputMaybe<StringNullableFilter>;
  notIn?: InputMaybe<Array<Scalars['String']['input']>>;
  startsWith?: InputMaybe<Scalars['String']['input']>;
};

export type User = {
  __typename?: 'User';
  author?: Maybe<Author>;
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  email?: Maybe<Scalars['String']['output']>;
  id: Scalars['ID']['output'];
  isAdmin?: Maybe<Scalars['Boolean']['output']>;
  name?: Maybe<Scalars['String']['output']>;
  password?: Maybe<PasswordState>;
  role?: Maybe<Scalars['String']['output']>;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
};

export type UserAuthenticationWithPasswordFailure = {
  __typename?: 'UserAuthenticationWithPasswordFailure';
  message: Scalars['String']['output'];
};

export type UserAuthenticationWithPasswordResult = UserAuthenticationWithPasswordFailure | UserAuthenticationWithPasswordSuccess;

export type UserAuthenticationWithPasswordSuccess = {
  __typename?: 'UserAuthenticationWithPasswordSuccess';
  item: User;
  sessionToken: Scalars['String']['output'];
};

export type UserCreateInput = {
  author?: InputMaybe<AuthorRelateToOneForCreateInput>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  email?: InputMaybe<Scalars['String']['input']>;
  isAdmin?: InputMaybe<Scalars['Boolean']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  password?: InputMaybe<Scalars['String']['input']>;
  role?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type UserOrderByInput = {
  createdAt?: InputMaybe<OrderDirection>;
  email?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  isAdmin?: InputMaybe<OrderDirection>;
  name?: InputMaybe<OrderDirection>;
  role?: InputMaybe<OrderDirection>;
  updatedAt?: InputMaybe<OrderDirection>;
};

export type UserRelateToOneForCreateInput = {
  connect?: InputMaybe<UserWhereUniqueInput>;
  create?: InputMaybe<UserCreateInput>;
};

export type UserRelateToOneForUpdateInput = {
  connect?: InputMaybe<UserWhereUniqueInput>;
  create?: InputMaybe<UserCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

export type UserUpdateArgs = {
  data: UserUpdateInput;
  where: UserWhereUniqueInput;
};

export type UserUpdateInput = {
  author?: InputMaybe<AuthorRelateToOneForUpdateInput>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  email?: InputMaybe<Scalars['String']['input']>;
  isAdmin?: InputMaybe<Scalars['Boolean']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  password?: InputMaybe<Scalars['String']['input']>;
  role?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type UserWhereInput = {
  AND?: InputMaybe<Array<UserWhereInput>>;
  NOT?: InputMaybe<Array<UserWhereInput>>;
  OR?: InputMaybe<Array<UserWhereInput>>;
  author?: InputMaybe<AuthorWhereInput>;
  createdAt?: InputMaybe<DateTimeNullableFilter>;
  email?: InputMaybe<StringNullableFilter>;
  id?: InputMaybe<IdFilter>;
  isAdmin?: InputMaybe<BooleanFilter>;
  name?: InputMaybe<StringFilter>;
  role?: InputMaybe<StringFilter>;
  updatedAt?: InputMaybe<DateTimeNullableFilter>;
};

export type UserWhereUniqueInput = {
  email?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['ID']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
};

export type ColourCompleteFragment = { __typename?: 'Colour', label?: string | null, name?: string | null, color?: { __typename?: 'ColorHSVOutput', h: number, s: number, v: number } | null, narratives?: Array<{ __typename?: 'Narrative', id: string, slug?: string | null, title?: string | null, date?: string | null, category?: { __typename?: 'NarrativeCategory', slug?: string | null, title?: string | null } | null, authors?: Array<{ __typename?: 'Author', id: string, name?: string | null, presentation?: string | null }> | null, cover?: { __typename?: 'Media', id: string, title?: string | null, type?: string | null, url?: string | null, embed?: string | null, image?: { __typename?: 'ImageFieldOutput', id: string, url: string, width: number, height: number, extension: ImageExtension, filesize: number } | null } | null }> | null } & { ' $fragmentName'?: 'ColourCompleteFragment' };

export type ColoursQueryVariables = Exact<{
  where?: InputMaybe<ColourWhereInput>;
}>;


export type ColoursQuery = { __typename?: 'Query', colours?: Array<(
    { __typename?: 'Colour' }
    & { ' $fragmentRefs'?: { 'ColourCompleteFragment': ColourCompleteFragment } }
  )> | null };

export type EventCompleteFragment = { __typename?: 'Event', slug?: string | null, label?: string | null, startDate?: string | null, endDate?: string | null, place?: string | null, type?: { __typename?: 'EventType', label?: string | null } | null, description?: { __typename?: 'Event_description_Document', document: any } | null, narratives?: Array<{ __typename?: 'Narrative', id: string, slug?: string | null, title?: string | null, date?: string | null, category?: { __typename?: 'NarrativeCategory', slug?: string | null, title?: string | null } | null, authors?: Array<{ __typename?: 'Author', id: string, name?: string | null, presentation?: string | null }> | null, cover?: { __typename?: 'Media', id: string, title?: string | null, type?: string | null, url?: string | null, embed?: string | null, image?: { __typename?: 'ImageFieldOutput', id: string, url: string, width: number, height: number, extension: ImageExtension, filesize: number } | null } | null }> | null } & { ' $fragmentName'?: 'EventCompleteFragment' };

export type EventQueryVariables = Exact<{
  where: EventWhereUniqueInput;
}>;


export type EventQuery = { __typename?: 'Query', event?: (
    { __typename?: 'Event' }
    & { ' $fragmentRefs'?: { 'EventCompleteFragment': EventCompleteFragment } }
  ) | null };

export type EventsQueryVariables = Exact<{
  where?: InputMaybe<EventWhereInput>;
}>;


export type EventsQuery = { __typename?: 'Query', events?: Array<(
    { __typename?: 'Event' }
    & { ' $fragmentRefs'?: { 'EventCompleteFragment': EventCompleteFragment } }
  )> | null };

export type GlossaryCompleteFragment = { __typename?: 'Glossary', slug?: string | null, label?: string | null, chemicalFormula?: string | null, dateOfDiscoveryInvention?: string | null, description?: { __typename?: 'Glossary_description_Document', document: any } | null, type?: { __typename?: 'GlossaryType', label?: string | null, category?: string | null } | null, medias?: Array<{ __typename?: 'Media', id: string, title?: string | null, type?: string | null, url?: string | null, embed?: string | null, image?: { __typename?: 'ImageFieldOutput', id: string, url: string, width: number, height: number, extension: ImageExtension, filesize: number } | null }> | null, narratives?: Array<{ __typename?: 'Narrative', id: string, slug?: string | null, title?: string | null, date?: string | null, category?: { __typename?: 'NarrativeCategory', slug?: string | null, title?: string | null } | null, authors?: Array<{ __typename?: 'Author', id: string, name?: string | null, presentation?: string | null }> | null, cover?: { __typename?: 'Media', id: string, title?: string | null, type?: string | null, url?: string | null, embed?: string | null, image?: { __typename?: 'ImageFieldOutput', id: string, url: string, width: number, height: number, extension: ImageExtension, filesize: number } | null } | null }> | null } & { ' $fragmentName'?: 'GlossaryCompleteFragment' };

export type GlossaryQueryVariables = Exact<{
  where: GlossaryWhereUniqueInput;
}>;


export type GlossaryQuery = { __typename?: 'Query', glossary?: (
    { __typename?: 'Glossary' }
    & { ' $fragmentRefs'?: { 'GlossaryCompleteFragment': GlossaryCompleteFragment } }
  ) | null };

export type GlossariesQueryVariables = Exact<{
  where?: InputMaybe<GlossaryWhereInput>;
}>;


export type GlossariesQuery = { __typename?: 'Query', glossaries?: Array<(
    { __typename?: 'Glossary' }
    & { ' $fragmentRefs'?: { 'GlossaryCompleteFragment': GlossaryCompleteFragment } }
  )> | null };

export type AuthenticateUserWithPasswordMutationVariables = Exact<{
  password: Scalars['String']['input'];
  name: Scalars['String']['input'];
}>;


export type AuthenticateUserWithPasswordMutation = { __typename?: 'Mutation', authenticateUserWithPassword?: { __typename?: 'UserAuthenticationWithPasswordFailure' } | { __typename?: 'UserAuthenticationWithPasswordSuccess', sessionToken: string } | null };

export type AuthenticatedItemQueryVariables = Exact<{ [key: string]: never; }>;


export type AuthenticatedItemQuery = { __typename?: 'Query', authenticatedItem?: { __typename?: 'User', name?: string | null } | null };

export type NarrativesHomeQueryVariables = Exact<{
  whereNarrative?: InputMaybe<NarrativeWhereInput>;
  whereObject?: InputMaybe<ObjectWhereInput>;
}>;


export type NarrativesHomeQuery = { __typename?: 'Query', narratives?: Array<(
    { __typename?: 'Narrative' }
    & { ' $fragmentRefs'?: { 'NarrativeCompleteFragment': NarrativeCompleteFragment } }
  )> | null, objects?: Array<(
    { __typename?: 'Object' }
    & { ' $fragmentRefs'?: { 'ObjectCompleteFragment': ObjectCompleteFragment } }
  )> | null, staticContent?: { __typename?: 'StaticContent', baseline?: string | null } | null };

export type NarrativeCompleteFragment = { __typename?: 'Narrative', id: string, slug?: string | null, title?: string | null, date?: string | null, language?: string | null, cover?: { __typename?: 'Media', id: string, title?: string | null, type?: string | null, url?: string | null, embed?: string | null, image?: { __typename?: 'ImageFieldOutput', id: string, url: string, width: number, height: number, extension: ImageExtension, filesize: number } | null } | null, content?: { __typename?: 'Narrative_content_Document', document: any } | null, references?: Array<{ __typename?: 'Reference', slug?: string | null, label?: string | null, citation?: { __typename?: 'Reference_citation_Document', document: any } | null }> | null, extraReferences?: Array<{ __typename?: 'Reference', slug?: string | null, label?: string | null, citation?: { __typename?: 'Reference_citation_Document', document: any } | null }> | null, authors?: Array<{ __typename?: 'Author', id: string, name?: string | null, presentation?: string | null, personalPageURL?: string | null }> | null, category?: { __typename?: 'NarrativeCategory', slug?: string | null, title?: string | null } | null } & { ' $fragmentName'?: 'NarrativeCompleteFragment' };

export type NarrativeQueryVariables = Exact<{
  where: NarrativeWhereUniqueInput;
}>;


export type NarrativeQuery = { __typename?: 'Query', narrative?: (
    { __typename?: 'Narrative' }
    & { ' $fragmentRefs'?: { 'NarrativeCompleteFragment': NarrativeCompleteFragment } }
  ) | null };

export type NarrativesQueryVariables = Exact<{ [key: string]: never; }>;


export type NarrativesQuery = { __typename?: 'Query', narratives?: Array<(
    { __typename?: 'Narrative' }
    & { ' $fragmentRefs'?: { 'NarrativeCompleteFragment': NarrativeCompleteFragment } }
  )> | null };

export type Public_NarrativesQueryVariables = Exact<{ [key: string]: never; }>;


export type Public_NarrativesQuery = { __typename?: 'Query', narratives?: Array<(
    { __typename?: 'Narrative' }
    & { ' $fragmentRefs'?: { 'NarrativeCompleteFragment': NarrativeCompleteFragment } }
  )> | null };

export type NarrativeCategoryQueryVariables = Exact<{
  slug: Scalars['String']['input'];
  narrativesWhere?: InputMaybe<NarrativeWhereInput>;
}>;


export type NarrativeCategoryQuery = { __typename?: 'Query', narrativeCategory?: { __typename?: 'NarrativeCategory', title?: string | null, description?: { __typename?: 'NarrativeCategory_description_Document', document: any } | null, narratives?: Array<(
      { __typename?: 'Narrative' }
      & { ' $fragmentRefs'?: { 'NarrativeCompleteFragment': NarrativeCompleteFragment } }
    )> | null } | null };

export type ObjectCompleteFragment = { __typename?: 'Object', id: string, slug?: string | null, label?: string | null, author?: string | null, location?: string | null, medium?: string | null, dimensions?: string | null, digitalizedByChromotope?: boolean | null, date?: string | null, mediasCount?: number | null, description?: { __typename?: 'Object_description_Document', document: any } | null, type?: { __typename?: 'ObjectType', label?: string | null } | null, cover?: { __typename?: 'Media', id: string, title?: string | null, type?: string | null, url?: string | null, embed?: string | null, image?: { __typename?: 'ImageFieldOutput', id: string, url: string, width: number, height: number, extension: ImageExtension, filesize: number } | null } | null, medias?: Array<{ __typename?: 'Media', id: string, title?: string | null, order?: number | null, type?: string | null, url?: string | null, embed?: string | null, image?: { __typename?: 'ImageFieldOutput', id: string, url: string, width: number, height: number, extension: ImageExtension, filesize: number } | null }> | null, reference?: { __typename?: 'Reference', id: string, citation?: { __typename?: 'Reference_citation_Document', document: any } | null } | null, colours?: Array<{ __typename?: 'Colour', id: string, label?: string | null, color?: { __typename?: 'ColorHSVOutput', h: number, s: number, v: number } | null }> | null, narratives?: Array<(
    { __typename?: 'Narrative' }
    & { ' $fragmentRefs'?: { 'NarrativeCompleteFragment': NarrativeCompleteFragment } }
  )> | null } & { ' $fragmentName'?: 'ObjectCompleteFragment' };

export type ObjectQueryVariables = Exact<{
  where: ObjectWhereUniqueInput;
}>;


export type ObjectQuery = { __typename?: 'Query', object?: (
    { __typename?: 'Object' }
    & { ' $fragmentRefs'?: { 'ObjectCompleteFragment': ObjectCompleteFragment } }
  ) | null };

export type ObjectsQueryVariables = Exact<{
  where?: InputMaybe<ObjectWhereInput>;
}>;


export type ObjectsQuery = { __typename?: 'Query', objects?: Array<(
    { __typename?: 'Object' }
    & { ' $fragmentRefs'?: { 'ObjectCompleteFragment': ObjectCompleteFragment } }
  )> | null };

export type OrganisationCompleteFragment = { __typename?: 'Organisation', slug?: string | null, label?: string | null, dateOfInception?: string | null, type?: { __typename?: 'OrganisationType', label?: string | null } | null, description?: { __typename?: 'Organisation_description_Document', document: any } | null, narratives?: Array<{ __typename?: 'Narrative', id: string, slug?: string | null, title?: string | null, date?: string | null, category?: { __typename?: 'NarrativeCategory', slug?: string | null, title?: string | null } | null, authors?: Array<{ __typename?: 'Author', id: string, name?: string | null, presentation?: string | null }> | null, cover?: { __typename?: 'Media', id: string, title?: string | null, type?: string | null, url?: string | null, embed?: string | null, image?: { __typename?: 'ImageFieldOutput', id: string, url: string, width: number, height: number, extension: ImageExtension, filesize: number } | null } | null }> | null } & { ' $fragmentName'?: 'OrganisationCompleteFragment' };

export type OrganisationQueryVariables = Exact<{
  where: OrganisationWhereUniqueInput;
}>;


export type OrganisationQuery = { __typename?: 'Query', organisation?: (
    { __typename?: 'Organisation' }
    & { ' $fragmentRefs'?: { 'OrganisationCompleteFragment': OrganisationCompleteFragment } }
  ) | null };

export type OrganisationsQueryVariables = Exact<{
  where?: InputMaybe<OrganisationWhereInput>;
}>;


export type OrganisationsQuery = { __typename?: 'Query', organisations?: Array<(
    { __typename?: 'Organisation' }
    & { ' $fragmentRefs'?: { 'OrganisationCompleteFragment': OrganisationCompleteFragment } }
  )> | null };

export type PersonCompleteFragment = { __typename?: 'Person', slug?: string | null, label?: string | null, firstName?: string | null, lastName?: string | null, fullName?: string | null, dateOfBirth?: string | null, dateOfDeath?: string | null, biography?: { __typename?: 'Person_biography_Document', document: any } | null, occupations?: Array<{ __typename?: 'Occupation', label?: string | null }> | null, narratives?: Array<{ __typename?: 'Narrative', id: string, slug?: string | null, title?: string | null, date?: string | null, category?: { __typename?: 'NarrativeCategory', slug?: string | null, title?: string | null } | null, authors?: Array<{ __typename?: 'Author', id: string, name?: string | null, presentation?: string | null }> | null, cover?: { __typename?: 'Media', id: string, title?: string | null, type?: string | null, url?: string | null, embed?: string | null, image?: { __typename?: 'ImageFieldOutput', id: string, url: string, width: number, height: number, extension: ImageExtension, filesize: number } | null } | null }> | null } & { ' $fragmentName'?: 'PersonCompleteFragment' };

export type PersonQueryVariables = Exact<{
  where: PersonWhereUniqueInput;
}>;


export type PersonQuery = { __typename?: 'Query', person?: (
    { __typename?: 'Person' }
    & { ' $fragmentRefs'?: { 'PersonCompleteFragment': PersonCompleteFragment } }
  ) | null };

export type PeopleQueryVariables = Exact<{
  where?: InputMaybe<PersonWhereInput>;
}>;


export type PeopleQuery = { __typename?: 'Query', people?: Array<(
    { __typename?: 'Person' }
    & { ' $fragmentRefs'?: { 'PersonCompleteFragment': PersonCompleteFragment } }
  )> | null };

export type ReferenceCompleteFragment = { __typename?: 'Reference', label?: string | null, slug?: string | null, note?: string | null, citation?: { __typename?: 'Reference_citation_Document', document: any } | null, narratives?: Array<(
    { __typename?: 'Narrative' }
    & { ' $fragmentRefs'?: { 'NarrativeCompleteFragment': NarrativeCompleteFragment } }
  )> | null, medias?: Array<{ __typename?: 'Media', id: string, title?: string | null, order?: number | null, type?: string | null, url?: string | null, embed?: string | null, image?: { __typename?: 'ImageFieldOutput', id: string, url: string, width: number, height: number, extension: ImageExtension, filesize: number } | null }> | null } & { ' $fragmentName'?: 'ReferenceCompleteFragment' };

export type ReferenceQueryVariables = Exact<{
  where: ReferenceWhereUniqueInput;
}>;


export type ReferenceQuery = { __typename?: 'Query', reference?: (
    { __typename?: 'Reference' }
    & { ' $fragmentRefs'?: { 'ReferenceCompleteFragment': ReferenceCompleteFragment } }
  ) | null };

export type ReferencesQueryVariables = Exact<{
  where?: InputMaybe<ReferenceWhereInput>;
}>;


export type ReferencesQuery = { __typename?: 'Query', references?: Array<(
    { __typename?: 'Reference' }
    & { ' $fragmentRefs'?: { 'ReferenceCompleteFragment': ReferenceCompleteFragment } }
  )> | null };

export type StaticContentQueryVariables = Exact<{ [key: string]: never; }>;


export type StaticContentQuery = { __typename?: 'Query', staticContent?: { __typename?: 'StaticContent', id: string, baseline?: string | null, colourWheelIntroduction?: { __typename?: 'StaticContent_colourWheelIntroduction_Document', document: any } | null, about?: { __typename?: 'StaticContent_about_Document', document: any } | null, credits?: { __typename?: 'StaticContent_credits_Document', document: any } | null, partnersLogos?: Array<{ __typename?: 'Logo', institutionName?: string | null, url?: string | null, image?: { __typename?: 'ImageFieldOutput', id: string, url: string, width: number, height: number, extension: ImageExtension, filesize: number } | null }> | null, funding?: { __typename?: 'StaticContent_funding_Document', document: any } | null, fundingLogos?: Array<{ __typename?: 'Logo', institutionName?: string | null, url?: string | null, image?: { __typename?: 'ImageFieldOutput', id: string, url: string, width: number, height: number, extension: ImageExtension, filesize: number } | null }> | null, legalNotice?: { __typename?: 'StaticContent_legalNotice_Document', document: any } | null } | null };

export const ColourCompleteFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"ColourComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Colour"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"color"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"h"}},{"kind":"Field","name":{"kind":"Name","value":"s"}},{"kind":"Field","name":{"kind":"Name","value":"v"}}]}},{"kind":"Field","name":{"kind":"Name","value":"narratives"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"category"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}}]}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"authors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"presentation"}}]}},{"kind":"Field","name":{"kind":"Name","value":"cover"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}}]}}]}}]} as unknown as DocumentNode<ColourCompleteFragment, unknown>;
export const EventCompleteFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"EventComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Event"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"type"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"label"}}]}},{"kind":"Field","name":{"kind":"Name","value":"startDate"}},{"kind":"Field","name":{"kind":"Name","value":"endDate"}},{"kind":"Field","name":{"kind":"Name","value":"place"}},{"kind":"Field","name":{"kind":"Name","value":"description"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}},{"kind":"Field","name":{"kind":"Name","value":"narratives"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"category"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}}]}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"authors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"presentation"}}]}},{"kind":"Field","name":{"kind":"Name","value":"cover"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}}]}}]}}]} as unknown as DocumentNode<EventCompleteFragment, unknown>;
export const GlossaryCompleteFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"GlossaryComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Glossary"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"description"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"category"}}]}},{"kind":"Field","name":{"kind":"Name","value":"chemicalFormula"}},{"kind":"Field","name":{"kind":"Name","value":"dateOfDiscoveryInvention"}},{"kind":"Field","name":{"kind":"Name","value":"medias"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}},{"kind":"Field","name":{"kind":"Name","value":"narratives"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"category"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}}]}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"authors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"presentation"}}]}},{"kind":"Field","name":{"kind":"Name","value":"cover"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}}]}}]}}]} as unknown as DocumentNode<GlossaryCompleteFragment, unknown>;
export const NarrativeCompleteFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"NarrativeComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Narrative"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"language"}},{"kind":"Field","name":{"kind":"Name","value":"cover"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}},{"kind":"Field","name":{"kind":"Name","value":"content"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"hydrateRelationships"},"value":{"kind":"BooleanValue","value":true}}]}]}},{"kind":"Field","name":{"kind":"Name","value":"references"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"citation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"extraReferences"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"citation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"authors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"presentation"}},{"kind":"Field","name":{"kind":"Name","value":"personalPageURL"}}]}},{"kind":"Field","name":{"kind":"Name","value":"category"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}}]}}]}}]} as unknown as DocumentNode<NarrativeCompleteFragment, unknown>;
export const ObjectCompleteFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"ObjectComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Object"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"author"}},{"kind":"Field","name":{"kind":"Name","value":"location"}},{"kind":"Field","name":{"kind":"Name","value":"medium"}},{"kind":"Field","name":{"kind":"Name","value":"dimensions"}},{"kind":"Field","name":{"kind":"Name","value":"description"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}},{"kind":"Field","name":{"kind":"Name","value":"digitalizedByChromotope"}},{"kind":"Field","name":{"kind":"Name","value":"type"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"label"}}]}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"cover"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}},{"kind":"Field","name":{"kind":"Name","value":"medias"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"order"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}},{"kind":"Field","name":{"kind":"Name","value":"mediasCount"}},{"kind":"Field","name":{"kind":"Name","value":"reference"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"citation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"colours"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"color"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"h"}},{"kind":"Field","name":{"kind":"Name","value":"s"}},{"kind":"Field","name":{"kind":"Name","value":"v"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"narratives"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"NarrativeComplete"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"NarrativeComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Narrative"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"language"}},{"kind":"Field","name":{"kind":"Name","value":"cover"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}},{"kind":"Field","name":{"kind":"Name","value":"content"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"hydrateRelationships"},"value":{"kind":"BooleanValue","value":true}}]}]}},{"kind":"Field","name":{"kind":"Name","value":"references"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"citation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"extraReferences"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"citation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"authors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"presentation"}},{"kind":"Field","name":{"kind":"Name","value":"personalPageURL"}}]}},{"kind":"Field","name":{"kind":"Name","value":"category"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}}]}}]}}]} as unknown as DocumentNode<ObjectCompleteFragment, unknown>;
export const OrganisationCompleteFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"OrganisationComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Organisation"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"dateOfInception"}},{"kind":"Field","name":{"kind":"Name","value":"type"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"label"}}]}},{"kind":"Field","name":{"kind":"Name","value":"description"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}},{"kind":"Field","name":{"kind":"Name","value":"narratives"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"category"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}}]}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"authors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"presentation"}}]}},{"kind":"Field","name":{"kind":"Name","value":"cover"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}}]}}]}}]} as unknown as DocumentNode<OrganisationCompleteFragment, unknown>;
export const PersonCompleteFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"PersonComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Person"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"firstName"}},{"kind":"Field","name":{"kind":"Name","value":"lastName"}},{"kind":"Field","name":{"kind":"Name","value":"fullName"}},{"kind":"Field","name":{"kind":"Name","value":"biography"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}},{"kind":"Field","name":{"kind":"Name","value":"dateOfBirth"}},{"kind":"Field","name":{"kind":"Name","value":"dateOfDeath"}},{"kind":"Field","name":{"kind":"Name","value":"occupations"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"label"}}]}},{"kind":"Field","name":{"kind":"Name","value":"narratives"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"category"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}}]}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"authors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"presentation"}}]}},{"kind":"Field","name":{"kind":"Name","value":"cover"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}}]}}]}}]} as unknown as DocumentNode<PersonCompleteFragment, unknown>;
export const ReferenceCompleteFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"ReferenceComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Reference"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"note"}},{"kind":"Field","name":{"kind":"Name","value":"citation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}},{"kind":"Field","name":{"kind":"Name","value":"narratives"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"NarrativeComplete"}}]}},{"kind":"Field","name":{"kind":"Name","value":"medias"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"order"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"NarrativeComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Narrative"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"language"}},{"kind":"Field","name":{"kind":"Name","value":"cover"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}},{"kind":"Field","name":{"kind":"Name","value":"content"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"hydrateRelationships"},"value":{"kind":"BooleanValue","value":true}}]}]}},{"kind":"Field","name":{"kind":"Name","value":"references"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"citation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"extraReferences"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"citation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"authors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"presentation"}},{"kind":"Field","name":{"kind":"Name","value":"personalPageURL"}}]}},{"kind":"Field","name":{"kind":"Name","value":"category"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}}]}}]}}]} as unknown as DocumentNode<ReferenceCompleteFragment, unknown>;
export const ColoursDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"colours"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"where"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"ColourWhereInput"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"colours"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"Variable","name":{"kind":"Name","value":"where"}}},{"kind":"Argument","name":{"kind":"Name","value":"orderBy"},"value":{"kind":"ListValue","values":[{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"name"},"value":{"kind":"EnumValue","value":"asc"}}]}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"ColourComplete"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"ColourComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Colour"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"color"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"h"}},{"kind":"Field","name":{"kind":"Name","value":"s"}},{"kind":"Field","name":{"kind":"Name","value":"v"}}]}},{"kind":"Field","name":{"kind":"Name","value":"narratives"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"category"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}}]}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"authors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"presentation"}}]}},{"kind":"Field","name":{"kind":"Name","value":"cover"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}}]}}]}}]} as unknown as DocumentNode<ColoursQuery, ColoursQueryVariables>;
export const EventDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"event"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"where"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"EventWhereUniqueInput"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"event"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"Variable","name":{"kind":"Name","value":"where"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"EventComplete"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"EventComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Event"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"type"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"label"}}]}},{"kind":"Field","name":{"kind":"Name","value":"startDate"}},{"kind":"Field","name":{"kind":"Name","value":"endDate"}},{"kind":"Field","name":{"kind":"Name","value":"place"}},{"kind":"Field","name":{"kind":"Name","value":"description"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}},{"kind":"Field","name":{"kind":"Name","value":"narratives"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"category"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}}]}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"authors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"presentation"}}]}},{"kind":"Field","name":{"kind":"Name","value":"cover"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}}]}}]}}]} as unknown as DocumentNode<EventQuery, EventQueryVariables>;
export const EventsDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"events"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"where"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"EventWhereInput"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"events"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"Variable","name":{"kind":"Name","value":"where"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"EventComplete"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"EventComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Event"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"type"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"label"}}]}},{"kind":"Field","name":{"kind":"Name","value":"startDate"}},{"kind":"Field","name":{"kind":"Name","value":"endDate"}},{"kind":"Field","name":{"kind":"Name","value":"place"}},{"kind":"Field","name":{"kind":"Name","value":"description"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}},{"kind":"Field","name":{"kind":"Name","value":"narratives"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"category"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}}]}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"authors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"presentation"}}]}},{"kind":"Field","name":{"kind":"Name","value":"cover"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}}]}}]}}]} as unknown as DocumentNode<EventsQuery, EventsQueryVariables>;
export const GlossaryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"glossary"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"where"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"GlossaryWhereUniqueInput"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"glossary"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"Variable","name":{"kind":"Name","value":"where"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"GlossaryComplete"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"GlossaryComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Glossary"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"description"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"category"}}]}},{"kind":"Field","name":{"kind":"Name","value":"chemicalFormula"}},{"kind":"Field","name":{"kind":"Name","value":"dateOfDiscoveryInvention"}},{"kind":"Field","name":{"kind":"Name","value":"medias"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}},{"kind":"Field","name":{"kind":"Name","value":"narratives"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"category"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}}]}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"authors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"presentation"}}]}},{"kind":"Field","name":{"kind":"Name","value":"cover"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}}]}}]}}]} as unknown as DocumentNode<GlossaryQuery, GlossaryQueryVariables>;
export const GlossariesDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"glossaries"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"where"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"GlossaryWhereInput"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"glossaries"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"Variable","name":{"kind":"Name","value":"where"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"GlossaryComplete"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"GlossaryComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Glossary"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"description"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"category"}}]}},{"kind":"Field","name":{"kind":"Name","value":"chemicalFormula"}},{"kind":"Field","name":{"kind":"Name","value":"dateOfDiscoveryInvention"}},{"kind":"Field","name":{"kind":"Name","value":"medias"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}},{"kind":"Field","name":{"kind":"Name","value":"narratives"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"category"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}}]}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"authors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"presentation"}}]}},{"kind":"Field","name":{"kind":"Name","value":"cover"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}}]}}]}}]} as unknown as DocumentNode<GlossariesQuery, GlossariesQueryVariables>;
export const AuthenticateUserWithPasswordDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"AuthenticateUserWithPassword"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"password"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"name"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"authenticateUserWithPassword"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"password"},"value":{"kind":"Variable","name":{"kind":"Name","value":"password"}}},{"kind":"Argument","name":{"kind":"Name","value":"name"},"value":{"kind":"Variable","name":{"kind":"Name","value":"name"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"UserAuthenticationWithPasswordSuccess"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"sessionToken"}}]}}]}}]}}]} as unknown as DocumentNode<AuthenticateUserWithPasswordMutation, AuthenticateUserWithPasswordMutationVariables>;
export const AuthenticatedItemDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"AuthenticatedItem"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"authenticatedItem"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"User"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]}}]}}]} as unknown as DocumentNode<AuthenticatedItemQuery, AuthenticatedItemQueryVariables>;
export const NarrativesHomeDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"NarrativesHome"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"whereNarrative"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"NarrativeWhereInput"}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"whereObject"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"ObjectWhereInput"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"narratives"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"Variable","name":{"kind":"Name","value":"whereNarrative"}}},{"kind":"Argument","name":{"kind":"Name","value":"orderBy"},"value":{"kind":"ListValue","values":[{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"createdAt"},"value":{"kind":"EnumValue","value":"desc"}}]},{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"updatedAt"},"value":{"kind":"EnumValue","value":"desc"}}]}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"NarrativeComplete"}}]}},{"kind":"Field","name":{"kind":"Name","value":"objects"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"Variable","name":{"kind":"Name","value":"whereObject"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"ObjectComplete"}}]}},{"kind":"Field","name":{"kind":"Name","value":"staticContent"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"baseline"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"NarrativeComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Narrative"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"language"}},{"kind":"Field","name":{"kind":"Name","value":"cover"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}},{"kind":"Field","name":{"kind":"Name","value":"content"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"hydrateRelationships"},"value":{"kind":"BooleanValue","value":true}}]}]}},{"kind":"Field","name":{"kind":"Name","value":"references"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"citation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"extraReferences"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"citation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"authors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"presentation"}},{"kind":"Field","name":{"kind":"Name","value":"personalPageURL"}}]}},{"kind":"Field","name":{"kind":"Name","value":"category"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"ObjectComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Object"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"author"}},{"kind":"Field","name":{"kind":"Name","value":"location"}},{"kind":"Field","name":{"kind":"Name","value":"medium"}},{"kind":"Field","name":{"kind":"Name","value":"dimensions"}},{"kind":"Field","name":{"kind":"Name","value":"description"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}},{"kind":"Field","name":{"kind":"Name","value":"digitalizedByChromotope"}},{"kind":"Field","name":{"kind":"Name","value":"type"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"label"}}]}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"cover"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}},{"kind":"Field","name":{"kind":"Name","value":"medias"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"order"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}},{"kind":"Field","name":{"kind":"Name","value":"mediasCount"}},{"kind":"Field","name":{"kind":"Name","value":"reference"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"citation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"colours"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"color"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"h"}},{"kind":"Field","name":{"kind":"Name","value":"s"}},{"kind":"Field","name":{"kind":"Name","value":"v"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"narratives"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"NarrativeComplete"}}]}}]}}]} as unknown as DocumentNode<NarrativesHomeQuery, NarrativesHomeQueryVariables>;
export const NarrativeDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"narrative"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"where"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"NarrativeWhereUniqueInput"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"narrative"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"Variable","name":{"kind":"Name","value":"where"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"NarrativeComplete"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"NarrativeComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Narrative"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"language"}},{"kind":"Field","name":{"kind":"Name","value":"cover"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}},{"kind":"Field","name":{"kind":"Name","value":"content"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"hydrateRelationships"},"value":{"kind":"BooleanValue","value":true}}]}]}},{"kind":"Field","name":{"kind":"Name","value":"references"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"citation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"extraReferences"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"citation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"authors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"presentation"}},{"kind":"Field","name":{"kind":"Name","value":"personalPageURL"}}]}},{"kind":"Field","name":{"kind":"Name","value":"category"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}}]}}]}}]} as unknown as DocumentNode<NarrativeQuery, NarrativeQueryVariables>;
export const NarrativesDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"narratives"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"narratives"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"NarrativeComplete"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"NarrativeComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Narrative"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"language"}},{"kind":"Field","name":{"kind":"Name","value":"cover"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}},{"kind":"Field","name":{"kind":"Name","value":"content"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"hydrateRelationships"},"value":{"kind":"BooleanValue","value":true}}]}]}},{"kind":"Field","name":{"kind":"Name","value":"references"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"citation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"extraReferences"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"citation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"authors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"presentation"}},{"kind":"Field","name":{"kind":"Name","value":"personalPageURL"}}]}},{"kind":"Field","name":{"kind":"Name","value":"category"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}}]}}]}}]} as unknown as DocumentNode<NarrativesQuery, NarrativesQueryVariables>;
export const Public_NarrativesDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"public_narratives"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"narratives"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"status"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"equals"},"value":{"kind":"StringValue","value":"public","block":false}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"NarrativeComplete"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"NarrativeComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Narrative"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"language"}},{"kind":"Field","name":{"kind":"Name","value":"cover"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}},{"kind":"Field","name":{"kind":"Name","value":"content"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"hydrateRelationships"},"value":{"kind":"BooleanValue","value":true}}]}]}},{"kind":"Field","name":{"kind":"Name","value":"references"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"citation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"extraReferences"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"citation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"authors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"presentation"}},{"kind":"Field","name":{"kind":"Name","value":"personalPageURL"}}]}},{"kind":"Field","name":{"kind":"Name","value":"category"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}}]}}]}}]} as unknown as DocumentNode<Public_NarrativesQuery, Public_NarrativesQueryVariables>;
export const NarrativeCategoryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"NarrativeCategory"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"slug"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"narrativesWhere"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"NarrativeWhereInput"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"narrativeCategory"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"slug"},"value":{"kind":"Variable","name":{"kind":"Name","value":"slug"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"description"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"narratives"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"Variable","name":{"kind":"Name","value":"narrativesWhere"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"NarrativeComplete"}}]}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"NarrativeComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Narrative"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"language"}},{"kind":"Field","name":{"kind":"Name","value":"cover"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}},{"kind":"Field","name":{"kind":"Name","value":"content"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"hydrateRelationships"},"value":{"kind":"BooleanValue","value":true}}]}]}},{"kind":"Field","name":{"kind":"Name","value":"references"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"citation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"extraReferences"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"citation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"authors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"presentation"}},{"kind":"Field","name":{"kind":"Name","value":"personalPageURL"}}]}},{"kind":"Field","name":{"kind":"Name","value":"category"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}}]}}]}}]} as unknown as DocumentNode<NarrativeCategoryQuery, NarrativeCategoryQueryVariables>;
export const ObjectDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"Object"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"where"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ObjectWhereUniqueInput"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"object"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"Variable","name":{"kind":"Name","value":"where"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"ObjectComplete"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"NarrativeComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Narrative"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"language"}},{"kind":"Field","name":{"kind":"Name","value":"cover"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}},{"kind":"Field","name":{"kind":"Name","value":"content"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"hydrateRelationships"},"value":{"kind":"BooleanValue","value":true}}]}]}},{"kind":"Field","name":{"kind":"Name","value":"references"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"citation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"extraReferences"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"citation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"authors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"presentation"}},{"kind":"Field","name":{"kind":"Name","value":"personalPageURL"}}]}},{"kind":"Field","name":{"kind":"Name","value":"category"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"ObjectComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Object"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"author"}},{"kind":"Field","name":{"kind":"Name","value":"location"}},{"kind":"Field","name":{"kind":"Name","value":"medium"}},{"kind":"Field","name":{"kind":"Name","value":"dimensions"}},{"kind":"Field","name":{"kind":"Name","value":"description"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}},{"kind":"Field","name":{"kind":"Name","value":"digitalizedByChromotope"}},{"kind":"Field","name":{"kind":"Name","value":"type"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"label"}}]}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"cover"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}},{"kind":"Field","name":{"kind":"Name","value":"medias"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"order"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}},{"kind":"Field","name":{"kind":"Name","value":"mediasCount"}},{"kind":"Field","name":{"kind":"Name","value":"reference"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"citation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"colours"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"color"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"h"}},{"kind":"Field","name":{"kind":"Name","value":"s"}},{"kind":"Field","name":{"kind":"Name","value":"v"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"narratives"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"NarrativeComplete"}}]}}]}}]} as unknown as DocumentNode<ObjectQuery, ObjectQueryVariables>;
export const ObjectsDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"Objects"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"where"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"ObjectWhereInput"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"objects"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"Variable","name":{"kind":"Name","value":"where"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"ObjectComplete"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"NarrativeComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Narrative"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"language"}},{"kind":"Field","name":{"kind":"Name","value":"cover"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}},{"kind":"Field","name":{"kind":"Name","value":"content"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"hydrateRelationships"},"value":{"kind":"BooleanValue","value":true}}]}]}},{"kind":"Field","name":{"kind":"Name","value":"references"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"citation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"extraReferences"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"citation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"authors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"presentation"}},{"kind":"Field","name":{"kind":"Name","value":"personalPageURL"}}]}},{"kind":"Field","name":{"kind":"Name","value":"category"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"ObjectComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Object"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"author"}},{"kind":"Field","name":{"kind":"Name","value":"location"}},{"kind":"Field","name":{"kind":"Name","value":"medium"}},{"kind":"Field","name":{"kind":"Name","value":"dimensions"}},{"kind":"Field","name":{"kind":"Name","value":"description"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}},{"kind":"Field","name":{"kind":"Name","value":"digitalizedByChromotope"}},{"kind":"Field","name":{"kind":"Name","value":"type"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"label"}}]}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"cover"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}},{"kind":"Field","name":{"kind":"Name","value":"medias"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"order"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}},{"kind":"Field","name":{"kind":"Name","value":"mediasCount"}},{"kind":"Field","name":{"kind":"Name","value":"reference"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"citation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"colours"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"color"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"h"}},{"kind":"Field","name":{"kind":"Name","value":"s"}},{"kind":"Field","name":{"kind":"Name","value":"v"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"narratives"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"NarrativeComplete"}}]}}]}}]} as unknown as DocumentNode<ObjectsQuery, ObjectsQueryVariables>;
export const OrganisationDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"organisation"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"where"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"OrganisationWhereUniqueInput"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"organisation"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"Variable","name":{"kind":"Name","value":"where"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"OrganisationComplete"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"OrganisationComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Organisation"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"dateOfInception"}},{"kind":"Field","name":{"kind":"Name","value":"type"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"label"}}]}},{"kind":"Field","name":{"kind":"Name","value":"description"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}},{"kind":"Field","name":{"kind":"Name","value":"narratives"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"category"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}}]}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"authors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"presentation"}}]}},{"kind":"Field","name":{"kind":"Name","value":"cover"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}}]}}]}}]} as unknown as DocumentNode<OrganisationQuery, OrganisationQueryVariables>;
export const OrganisationsDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"organisations"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"where"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"OrganisationWhereInput"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"organisations"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"Variable","name":{"kind":"Name","value":"where"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"OrganisationComplete"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"OrganisationComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Organisation"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"dateOfInception"}},{"kind":"Field","name":{"kind":"Name","value":"type"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"label"}}]}},{"kind":"Field","name":{"kind":"Name","value":"description"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}},{"kind":"Field","name":{"kind":"Name","value":"narratives"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"category"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}}]}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"authors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"presentation"}}]}},{"kind":"Field","name":{"kind":"Name","value":"cover"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}}]}}]}}]} as unknown as DocumentNode<OrganisationsQuery, OrganisationsQueryVariables>;
export const PersonDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"person"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"where"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"PersonWhereUniqueInput"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"person"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"Variable","name":{"kind":"Name","value":"where"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"PersonComplete"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"PersonComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Person"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"firstName"}},{"kind":"Field","name":{"kind":"Name","value":"lastName"}},{"kind":"Field","name":{"kind":"Name","value":"fullName"}},{"kind":"Field","name":{"kind":"Name","value":"biography"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}},{"kind":"Field","name":{"kind":"Name","value":"dateOfBirth"}},{"kind":"Field","name":{"kind":"Name","value":"dateOfDeath"}},{"kind":"Field","name":{"kind":"Name","value":"occupations"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"label"}}]}},{"kind":"Field","name":{"kind":"Name","value":"narratives"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"category"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}}]}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"authors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"presentation"}}]}},{"kind":"Field","name":{"kind":"Name","value":"cover"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}}]}}]}}]} as unknown as DocumentNode<PersonQuery, PersonQueryVariables>;
export const PeopleDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"people"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"where"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"PersonWhereInput"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"people"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"Variable","name":{"kind":"Name","value":"where"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"PersonComplete"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"PersonComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Person"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"firstName"}},{"kind":"Field","name":{"kind":"Name","value":"lastName"}},{"kind":"Field","name":{"kind":"Name","value":"fullName"}},{"kind":"Field","name":{"kind":"Name","value":"biography"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}},{"kind":"Field","name":{"kind":"Name","value":"dateOfBirth"}},{"kind":"Field","name":{"kind":"Name","value":"dateOfDeath"}},{"kind":"Field","name":{"kind":"Name","value":"occupations"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"label"}}]}},{"kind":"Field","name":{"kind":"Name","value":"narratives"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"category"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}}]}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"authors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"presentation"}}]}},{"kind":"Field","name":{"kind":"Name","value":"cover"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}}]}}]}}]} as unknown as DocumentNode<PeopleQuery, PeopleQueryVariables>;
export const ReferenceDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"reference"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"where"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ReferenceWhereUniqueInput"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"reference"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"Variable","name":{"kind":"Name","value":"where"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"ReferenceComplete"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"NarrativeComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Narrative"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"language"}},{"kind":"Field","name":{"kind":"Name","value":"cover"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}},{"kind":"Field","name":{"kind":"Name","value":"content"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"hydrateRelationships"},"value":{"kind":"BooleanValue","value":true}}]}]}},{"kind":"Field","name":{"kind":"Name","value":"references"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"citation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"extraReferences"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"citation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"authors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"presentation"}},{"kind":"Field","name":{"kind":"Name","value":"personalPageURL"}}]}},{"kind":"Field","name":{"kind":"Name","value":"category"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"ReferenceComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Reference"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"note"}},{"kind":"Field","name":{"kind":"Name","value":"citation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}},{"kind":"Field","name":{"kind":"Name","value":"narratives"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"NarrativeComplete"}}]}},{"kind":"Field","name":{"kind":"Name","value":"medias"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"order"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}}]}}]} as unknown as DocumentNode<ReferenceQuery, ReferenceQueryVariables>;
export const ReferencesDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"references"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"where"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"ReferenceWhereInput"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"references"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"Variable","name":{"kind":"Name","value":"where"}}},{"kind":"Argument","name":{"kind":"Name","value":"orderBy"},"value":{"kind":"ListValue","values":[{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"label"},"value":{"kind":"EnumValue","value":"asc"}}]}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"ReferenceComplete"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"NarrativeComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Narrative"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"language"}},{"kind":"Field","name":{"kind":"Name","value":"cover"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}},{"kind":"Field","name":{"kind":"Name","value":"content"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"hydrateRelationships"},"value":{"kind":"BooleanValue","value":true}}]}]}},{"kind":"Field","name":{"kind":"Name","value":"references"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"citation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"extraReferences"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"citation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"authors"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"presentation"}},{"kind":"Field","name":{"kind":"Name","value":"personalPageURL"}}]}},{"kind":"Field","name":{"kind":"Name","value":"category"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"title"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"ReferenceComplete"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"Reference"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"slug"}},{"kind":"Field","name":{"kind":"Name","value":"note"}},{"kind":"Field","name":{"kind":"Name","value":"citation"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}},{"kind":"Field","name":{"kind":"Name","value":"narratives"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"NarrativeComplete"}}]}},{"kind":"Field","name":{"kind":"Name","value":"medias"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"order"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"embed"}}]}}]}}]} as unknown as DocumentNode<ReferencesQuery, ReferencesQueryVariables>;
export const StaticContentDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"StaticContent"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"staticContent"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"baseline"}},{"kind":"Field","name":{"kind":"Name","value":"colourWheelIntroduction"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}},{"kind":"Field","name":{"kind":"Name","value":"about"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}},{"kind":"Field","name":{"kind":"Name","value":"credits"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}},{"kind":"Field","name":{"kind":"Name","value":"partnersLogos"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"institutionName"}},{"kind":"Field","name":{"kind":"Name","value":"url"}}]}},{"kind":"Field","name":{"kind":"Name","value":"funding"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}},{"kind":"Field","name":{"kind":"Name","value":"fundingLogos"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"extension"}},{"kind":"Field","name":{"kind":"Name","value":"filesize"}}]}},{"kind":"Field","name":{"kind":"Name","value":"institutionName"}},{"kind":"Field","name":{"kind":"Name","value":"url"}}]}},{"kind":"Field","name":{"kind":"Name","value":"legalNotice"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"document"}}]}}]}}]}}]} as unknown as DocumentNode<StaticContentQuery, StaticContentQueryVariables>;