// la fonction overflowTimeline est utilisée pour ajuster le style des éléments de la timeline s'il y a trop d'items dans la colonne
// fonctionne aussi sans mais c'est mois joli

// Je pense avoir gérer ça à la génération && CSS

// function overflowTimeline() {
//   const timeline = document.querySelector("#timeline");
//   if (timeline) {
//     const yearLists = timeline.querySelectorAll(".container-list ul");
//     yearLists.forEach(function (list) {
//       const items = list.querySelectorAll(".timeline-item");
//       console.log(items);
//       console.log(items.length);
//       if (items.length > 9) {
//         const total = items.length;
//         list.style.height = "calc((var(--size-elem) + 3px) * " + total + ")";
//         const parent = list.closest(".year");
//         if (parent) parent.classList.add("overflow");
//       }
//     });
//   }
// }

function timelineClick(thisItem: HTMLElement) {
  localStorage.setItem("timelineStorage", "open");
  const idCard = thisItem.dataset.cardId;
  const dataCat = thisItem.dataset.category;
  const items = document.querySelectorAll<HTMLElement>("#timeline .timeline-item");
  items.forEach(function (item) {
    item.style.backgroundColor = "var(--color-text-grey)";
    if (item.dataset.category == dataCat) {
      item.style.backgroundColor = "var(--color-text)";
    }
  });
  thisItem.style.backgroundColor = "var(--selected-color)";

  const cards = document.querySelectorAll<HTMLElement>("#narratives-timeline-container article");
  cards.forEach(function (card) {
    card.style.display = "none";
  });

  const cardSelected = document.querySelector<HTMLElement>(
    '#narratives-timeline-container article[data-card-id="' + idCard + '"]',
  );
  if (cardSelected) cardSelected.style.display = "block";

  const containerCard = document.querySelector<HTMLElement>("#narratives-timeline-container");
  if (containerCard) containerCard.style.right = "0px";
}

function timelineOver(thisItem: HTMLElement) {
  const dataCat = thisItem.dataset.category;
  const items = document.querySelectorAll<HTMLElement>("#timeline .timeline-item");
  const timelineStorage = localStorage.getItem("timelineStorage");
  if (timelineStorage != "open") {
    items.forEach(function (item) {
      item.style.backgroundColor = "var(--color-text-grey)";
      if (item.dataset.category == dataCat) {
        item.style.backgroundColor = "var(--color-text)";
      }
    });
  }
}

function timelineLeave() {
  const items = document.querySelectorAll<HTMLElement>("#timeline .timeline-item");
  const timelineStorage = localStorage.getItem("timelineStorage");
  if (timelineStorage != "open") {
    items.forEach(function (item) {
      item.style.backgroundColor = "var(--color-text)";
    });
  }
}

document.querySelectorAll<HTMLElement>("li.timeline-item").forEach((el) => {
  el.addEventListener("click", () => timelineClick(el));
  el.addEventListener("mouseover", () => timelineOver(el));
  el.addEventListener("mouseleave", () => timelineLeave());
});

function closeCardTimeline() {
  localStorage.setItem("timelineStorage", "close");
  const containerCard = document.querySelector<HTMLElement>("#narratives-timeline-container");
  if (containerCard) containerCard.style.right = "var(--closed)";
  const items = document.querySelectorAll<HTMLElement>("#timeline .timeline-item");
  items.forEach(function (item) {
    item.style.backgroundColor = "var(--color-text)";
  });
}
const closeCardButton = document.querySelector("#narratives-timeline-container>.close");
closeCardButton?.addEventListener("click", closeCardTimeline);
