import { hsvaToHslString, hsvaToHsla } from "@uiw/color-convert";
import { FC } from "react";

import { ColorHsvOutput } from "../../__generated__/graphql";
import { colorWheelSize } from "./ColorWheel";
import { defaultLightness, layersLightnessInterval } from "./configuration";

const size = 7;
const sizeFrame = 10;

export const ColorPoint: FC<{
  x: number;
  y: number;
  id: string;
  color: ColorHsvOutput;
}> = ({ x, y, id, color }) => {
  const hslString = hsvaToHslString({ a: 1, ...color });
  const { l } = hsvaToHsla({ a: 1, ...color });

  // center point + add padding on border
  const adjustPosition = (xy: number) => {
    if (xy < size / 2) return xy;
    if (xy > colorWheelSize - size) return xy - size;
    return xy - size / 2;
  };

  return (
    <g
      id={`colorwheel-point-${id}`}
      key={`object-${id}`}
      data-target={`object-${id}`}
      data-lightness={l}
      className={`colorwheel-point ${
        defaultLightness - layersLightnessInterval / 2 <= l &&
        l <= defaultLightness + layersLightnessInterval / 2
          ? ""
          : "out-of-lightness-scope"
      }`}
    >
      <g className="frame">
        <rect
          x={adjustPosition(x) - (sizeFrame - size) / 2}
          y={adjustPosition(y) - (sizeFrame - size) / 2}
          rx={1}
          width={sizeFrame}
          height={sizeFrame}
          style={{ fill: l < 90 ? "white" : "#ddd", stroke: "black" }}
        />
      </g>
      <g id={`inside-${id}`}>
        <rect
          x={adjustPosition(x)}
          y={adjustPosition(y)}
          rx={1}
          width={size}
          height={size}
          fill={hslString}
        />
      </g>
    </g>
  );
};
