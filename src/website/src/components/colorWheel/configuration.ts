import { range } from "lodash";

export const firstLayerLightness = 15;
export const layersLightnessInterval = 10;
export const defaultLightness = 55;
export const lightnessSteps = range(
  firstLayerLightness,
  100,
  layersLightnessInterval,
);

export const wheelHues = {
  orange: 30, // 0-30
  yellow: 57, // 30-60
  green1: 80, // 60-90
  green2: 105, //90-120
  green3: 150, //120-150
  blue1: 180, //150-180
  blue2: 210, //180-210
  blue3: 240, //210-240
  purple1: 270, //240-270
  purple2: 300, //270-300
  pink: 330, //300-330
  red: 360, //330-360
};

export const wheelSaturations = [95, 75, 55, 35, 15];

// Julie's colors
// orange: 37, // 0-30
// yellow: 57, // 30-60
// green1: 80, // 60-90
// green2: 105, //90-120
// green3: 156, //120-150
// blue1: 197, //150-180
// blue2: 217, //180-210
// blue3: 246, //210-240
// purple1: 280, //240-270
// purple2: 323, //270-300
// pink: 332, //300-330
// red: 360, //330-360
