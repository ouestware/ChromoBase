import { GlossaryType, Media } from "../../../__generated__/graphql";

export type DocumentNode = Element | Text;

export type Mark =
  | "bold"
  | "italic"
  | "underline"
  | "strikethrough"
  | "code"
  | "superscript"
  | "subscript"
  | "keyboard";

export type NodeType =
  | "paragraph"
  | "relationship"
  | "link"
  | "ordered-list"
  | "list-item"
  | "list-item-content";

export type Element = {
  children?: DocumentNode[];
  type: NodeType;
  [key: string]: unknown;
};

export type Text = {
  text: string;
} & Record<Mark, boolean>;

export type LinkNode = {
  type: "link";
  text: string;
  href: string;
  children?: InlineNode[];
};

export interface ParagraphNode {
  children: InlineNode[];
  type: "paragraph";
}
export interface RelationshipNode {
  type: "relationship";
  data?: {
    label: string;
    id: string;
    data: {
      label: string;
      slug: string;
      narrativesCount: number;
      name?: string;
      color: { h: number; s: number; v: number };
      cover?: Media;
      type?: { category: GlossaryType["category"] };
    };
  };
  relationship: string;
}

export interface ListItemContentNode {
  type: "list-item-content";
  children: InlineNode[];
}
export interface ListItemNode {
  type: "list-item";
  children: [ListItemContentNode];
}
export interface ListNode {
  type: "ordered-list" | "unordered-list";
  children: ListItemNode[];
}

export interface HeadingNode {
  type: "heading";
  level: number;
  children: InlineNode[];
}

export type InlineNode =
  | RelationshipNode
  | Text
  | LinkNode
  | ListItemContentNode;

export type TopLevelNode = ParagraphNode | ListNode | HeadingNode;
