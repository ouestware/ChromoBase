import { FC } from "react";
import { Author } from "../__generated__/graphql";

export const AuthorPresentation: FC<{ author: Author }> = (props) => {
  const { author } = props;
  return (
    <div style={{ display: "flex", flexDirection: "column", marginRight: "1rem" }}>
      <div>{author.name}</div>
      <small>{author.presentation}</small>
    </div>
  );
};
