date
echo "build prod from editor data..."
yarn run build:static
echo "build done"
echo "copy editor images to prod..."
mkdir -p dist/images/
cp -R ../editor/public/images/* dist/images/
echo "adapt IIIF manifests from preview to prod..."
echo  "$EDITOR_PUBLIC_URL$EDITOR_BASEPATH -> $WEBSITE_PUBLIC_URL$PUBLIC_BASEPATH"
find dist/images/* -type f -name "*.json" -exec sed -i -- "s|$EDITOR_PUBLIC_URL$EDITOR_BASEPATH|$WEBSITE_PUBLIC_URL$PUBLIC_BASEPATH|g" {} +
echo "images copied"
rsync -rl --delete  dist/* $WEBSITE_PRODUCTION_PATH
echo "prod ready!"
date