import { trimEnd } from "lodash";

import node from "@astrojs/node";
import react from "@astrojs/react";
import { AstroUserConfig } from "astro/config";
import { loadEnv } from "vite";

const localEnvFile = loadEnv("", process.cwd(), "");

//give precedence to process environement if exists otherwise try .env file content
const WEBSITE_PREVIEW_URL =
  process.env.WEBSITE_PREVIEW_URL || localEnvFile.WEBSITE_PREVIEW_URL;
const WEBSITE_PUBLIC_URL =
  process.env.WEBSITE_PUBLIC_URL || localEnvFile.WEBSITE_PUBLIC_URL;
const EDITOR_BASEPATH =
  process.env.EDITOR_BASEPATH || localEnvFile.EDITOR_BASEPATH;
const PREVIEW_BASEPATH =
  process.env.PREVIEW_BASEPATH || localEnvFile.PREVIEW_BASEPATH;
const PUBLIC_BASEPATH =
  process.env.PUBLIC_BASEPATH || localEnvFile.PUBLIC_BASEPATH;
const EDITOR_PUBLIC_URL = new URL(
  process.env.EDITOR_PUBLIC_URL || localEnvFile.EDITOR_PUBLIC_URL,
);

const finalUrl = new URL(
  (process.env.PUBLIC_BUILD_TARGET === "preview"
    ? WEBSITE_PREVIEW_URL
    : WEBSITE_PUBLIC_URL) || "http://localhost:4321",
);
const base = trimEnd(
  process.env.PUBLIC_BUILD_TARGET === "preview"
    ? `${EDITOR_BASEPATH}${PREVIEW_BASEPATH}`
    : PUBLIC_BASEPATH,
  "/",
);

export const SCSS_CONSTANTS = {
  padding: 16,
  caseUnit: 20,
};

const config: AstroUserConfig = {
  site: finalUrl.origin,
  base,
  integrations: [react()],
  image: {
    domains: [EDITOR_PUBLIC_URL.hostname],
  },

  vite: {
    ssr: {
      noExternal:
        process.env.NODE_ENV !== "production"
          ? ["@apollo/client", "path-to-regexp"]
          : ["@apollo/client", "lodash", "path-to-regexp"],
    },
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: `
            $basePath: '${base}'; 
            $padding: ${SCSS_CONSTANTS.padding}px;
            $caseUnit: ${SCSS_CONSTANTS.caseUnit}px; 
            `,
        },
      },
    },
  },
  output: process.env.PUBLIC_BUILD_TARGET === "preview" ? "server" : "static",
  adapter:
    process.env.PUBLIC_BUILD_TARGET === "preview"
      ? node({
          mode: "standalone",
        })
      : undefined,
};

export default config;
