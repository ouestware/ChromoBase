
import { defineConfig } from 'astro/config';

import config from "./config";


// https://astro.build/config
export default defineConfig(config);