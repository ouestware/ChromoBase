# database
export POSTGRES_DOMAI=localhost
export POSTGRES_PORT=5432
export POSTGRES_USER=chromobase
export POSTGRES_PASSWORD="############"
export POSTGRES_DATABASE=chromobase
# KEYSTONE
export SESSION_SECRET="################################"

export WEBSITE_PREVIEW_URL="http://localhost:4321"
export PREVIEW_BASEPATH="/preview"
export WEBSITE_PUBLIC_URL="https://chromobase.huma-num.fr"
export PUBLIC_BASEPATH=""
export EDITOR_PUBLIC_URL="https://chromobase.huma-num.fr"
export EDITOR_BASEPATH="/editor"
# variables exposed client side
export NEXT_PUBLIC_WIKIDATA_API_URL="https://chromobase.huma-num.fr/"
export NEXT_PUBLIC_PREVIEW_BASEPATH="/preview"
export NEXT_PUBLIC_WEBSITE_PREVIEW_URL="http://localhost:4321"
#GRAQL USER who need a reader role in keystone
export GQL_USER="astro"
export GQL_PASSWORD="##############"

export WEBSITE_PRODUCTION_PATH="/opt/chromobase/production_website"