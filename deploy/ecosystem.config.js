module.exports = {
    apps: [
      {
        name: 'editor',
        cwd: '/opt/chromobase/code/src/editor',
        script: 'yarn',
        args: 'start',
        env: {
          NODE_ENV: 'production',
        },
      },
      {
        name: 'preview',
        cwd: '/opt/chromobase/code/src/website/distPreview/',
        script: 'node',
        args: './server/entry.mjs',
        env: {
          NODE_ENV: 'production',
        },
      },
    ],
  };