# Deploy Chromobase

This documentation covers deploying on a debian server.

## PostGresql

### install

- install from debian repo

```
sudo apt-get install postgresql-15
```

- start and enable at boot

```
sudo systemctl start postgresql
sudo systemctl enable postgresql
```

- check everything is fine

```
systemctl status postgresql
```

### configure

Create a password for postgres user and create chromobase db and user

TODO: fix this mess...

```
sudo -u postgres psql
postgres=# ALTER USER postgres PASSWORD '#####';
postgres=# create database chromobase;
postgres=# ALTER DATABASE chromobase OWNER TO chromobase;
postgres=# create user chromobase with encrypted password '######';
postgres=# GRANT CONNECT ON DATABASE database_name TO username;
postgres=# GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO chromobase;
postgres=# GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA pulic TO chromobase
postgres=# grant all privileges on database chromobase to chromobase;
```

## Chromobase code

### node and yarn

```bash
sudo apt install npm git
sudo npm install -g n
# install node 18
sudo n 18
# enable corepack to activate yarn
corepack enable
```

### pm2

```bash
sudo npm install pm2 -g
pm2 init
```

### chromobase user

```bash
sudo useradd -m -d /opt/chromobase -s /bin/bash chromobase
```

### get the code

```bash
sudo su - chromobase
git clone https://gitlab.com/ouestware/chromobase.git code
```

### add fonts

Fonts are not distributed with the code as it's forbidden by the licence.
Fonts woff files must be placed in `/opt/chromobase/code/src/website/public/fonts`.

### add and adapt env variables

```bash
cp /opt/chromobase/code/deploy/load_env_production.sh  /opt/chromobase/
cat "source /opt/chromobase/load_env_production.sh" >> .bashrc
vi /opt/chromobase/load_env_production.sh
```

Change `GQL_PASSWORD` `SESSION_SECRET` and `POSTGRES_PASSWORD`

### install and build

```bash
sudo su chromobase
cd /opt/chromobase/code/
bash ./deploy/install.sh
```

### init database

- execute migration to create database structure

```bash
yarn keystone prisma migrate deploy
```

- remove temporarily the protection on `initFirstItem`

```bash
diff --git a/src/editor/auth.ts b/src/editor/auth.ts
index 72651ad..7d47558 100644
--- a/src/editor/auth.ts
+++ b/src/editor/auth.ts
@@ -43,7 +43,7 @@ const { withAuth } = createAuth({
   // WARNING: remove initFirstItem functionality in production
   //   see https://keystonejs.com/docs/config/auth#init-first-item for more
   initFirstItem:
-    process.env.NODE_ENV !== "production"
+    true || process.env.NODE_ENV !== "production"
       ? {
           // if there are no items in the database, by configuring this field
```

- connect to `https://chromobase.huma-num.fr/editor` and create an admin user with role = admin first. Then as an admin create the astro user with role = reader and the password you've wrote in load_env_production.sh.

- desactivate `initFirstItem`:

```bash
git checkout auth.ts
```

### orchestrate the node processes with pm2

````bash
# as chromobase user
# init pm2
pm2 init
# copy pm2 configuration
cp /opt/chromobase/code/deploy/ecosystem.config.js /opt/chromobase/

- start the node processes

```bash
# as chromobase user
cd /opt/chromobase
pm2 start
````

- watch log

```bash
# as chromobase user
cd /opt/chromobase
pm2 log
```

- Enable PM2 at server start by following the instruction given by the following command

```bash
pm2 startup systemd
```

- Save the current process list (@see `pm2 list`) that will start with the server

````bash
pm2 save```

## nginx

### Install snap

````

sudo apt-get install snapd

```

- Install certbot

```

sudo snap install --classic certbot
sudo ln -s /snap/bin/certbot /usr/bin/certbot

```

### install

- install from debian repo

```

sudo apt-get install nginx

```

- start and enable at boot

```

sudo systemctl start nginx
sudo systemctl enable nginx

```

- check everything is fine
  try to open your domain in a web browser, a `Welcome to nginx!` page should be rendered

### configure

- remove default configuration

```

sudo rm -f /etc/nginx/sites-available/default
sudo rm -f /etc/nginx/sites-enabled/default

````

- add chromobase configuration

```bash
sudo cp /opt/chromobase/code/deploy/nginx.conf /etc/nginx/sites-available/chromobase
````

- Create the symlink in `site-enabled`

```bash
sudo ln -s /etc/nginx/sites-enabled/chromobase /etc/nginx/sites-available/chromobase
```

- Generate SSL certificate with certbot

```bash
sudo certbot certonly --nginx
```

-reload nginx

```bash
nginx -s reload
```

## backup script

### database data

Make sure to execute the script `backup_database.sh` regularly.
To avoid filesystem overload while keeping some anteriority one can rotate this backup file thanks to for instance the `rotate.conf`.

### editor images

Make sure to backup images uploaded in `/opt/chromobase/code/src/editor/public/images/`.

## update stack code

Connect as chromobase user

```bash
sudo su chromobase
```

stop editor and preview services

```bash
cd /opt/chromobase
pm2 stop preview editor
```

get new code

```bash
cd /opt/chromobase/code
git pull
```

Install potential new deps and build editor and website code

```bash
yarn
yarn build
```

If needed apply database migration

```
yarn keystone prisma migrate status
yarn keystone prisma migrate deploy
```

restart editor and preview

```
cd /opt/chromobase
pm2 start all
```

## restore

### database

data only

```bash
sudo -u postgres pg_restore ./chromobase_pg_data_backup.dump -d chromobase -U postgres --data -t Author -t Colour -t EventType -t Event  -t GlossaryType -t Glossary -t Media -t ObjectType -t OrganisationType -t Narrative -t NarrativeCategory -t NarrativeTranslation -t Object  -t Occupation -t Organisation  -t Person -t Reference -t User -t _Author_narratives -t _Colour_narratives -t _Event_narratives -t _Glossary_medias -t _Glossary_narratives -t _Narrative_extraReferences -t _Narrative_objects -t _Narrative_organisations -t _Narrative_people -t _Narrative_references -t _Object_colours -t _Object_medias -t _Person_occupations -t _Reference_medias
```
