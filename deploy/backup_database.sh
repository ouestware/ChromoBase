#!/bin/bash

# load env variables
source /opt/chromobase/load_env_production.sh

# backup database  
sudo -u postgres pg_dump -d $POSTGRES_DATABASE --format c -f /var/lib/postgresql/chromobase_pg_backup.dump
mv /var/lib/postgresql/chromobase_pg_backup.dump /opt/backup/database

# rotate
/usr/sbin/logrotate -f /opt/backup/database/rotate.conf
