source  ./`basename $0 .sh`.cfg

###############################################################################
# CONSTANTS & UTILS
###############################################################################
DATETIME=`date +%Y-%m-%d-%H.%M`
DATE=`date +%Y-%m-%d`
TMPDIR=$(mktemp -d)

source ~/.bashrc

echo " ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo " ~~~ Executing installation script ~~~"
echo " ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo
echo "Starting installation of $COMMIT_HASH at $DATETIME"

cd ${PROJECT_FOLDER}
echo "Installing project deps"
yarn install

echo "Building project"
yarn run build

echo "Server hot reload"
pm2 reload all --update-env